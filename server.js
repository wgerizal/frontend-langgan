const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, process.env.FOLDER_MEMBER || 'public/member/build/')));
app.use(express.static(path.join(__dirname, process.env.FOLDER_ADMIN || 'public/admin/build/')));


app.get('/admin', function (req, res) {
  res.sendFile(path.join(__dirname, process.env.FOLDER_ADMIN || 'public/admin/build/', 'index.html'));
});

app.get('/admin/*', function (req, res) {
  res.sendFile(path.join(__dirname, process.env.FOLDER_ADMIN || 'public/admin/build/', 'index.html'));
});

app.get('/auth/*', function (req, res) {
  res.sendFile(path.join(__dirname, process.env.FOLDER_ADMIN || 'public/admin/build/', 'index.html'));
});


app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, process.env.FOLDER_MEMBER || 'public/member/build/', 'index.html'));
});

app.get('*.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
});

const port = process.env.PORT || 8080;
app.listen(port, function(){
 console.log('App is listening on port ' + port)
});
