const USER_SESSION = '12839016b8asd87ASJASD90129JMzJXASda989jna???o=122**61';
const USER_EMAIL = '2345678aoskdjhgtxzyfcgv1v526781irjfvtasrdbga';
const USER_PASS = '12673uihc76at23h12>1923HAbHBAGBSDJAHNunNAUH';
const USER_TOKEN_DASHBOARD = 'token_dashboard';

let store = {
  get() {
    let userDataStr = localStorage.getItem(USER_SESSION);
    
    const token = JSON.parse(userDataStr)
    return token;
  },

  getUserEmail() {
    let userDataStr = localStorage.getItem(USER_EMAIL);
    const email = JSON.parse(userDataStr) 
    return email;
  },

  getUserPass() {
    let userDataStr = localStorage.getItem(USER_PASS);
    const pass = JSON.parse(userDataStr)
    return pass;
  },

  getTokenDashboard() {
    let userDataStr = localStorage.getItem(USER_TOKEN_DASHBOARD);
    const token_dashboard = JSON.parse(userDataStr)
    return token_dashboard;
  },


  set(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_SESSION, JSON.stringify(userData));
    }
  },

  setTokenDashboard(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_TOKEN_DASHBOARD, JSON.stringify(userData));
    }
  },

  setUserEmail(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_EMAIL, JSON.stringify(userData));
    }
  },

  setUserPass(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_PASS, JSON.stringify(userData));
    }
  },

  remove() {
    localStorage.removeItem(USER_SESSION);
  },
  removeEmail() {
    localStorage.removeItem(USER_EMAIL);
  },
  removeUserCredentials() {
    localStorage.removeItem(USER_EMAIL);
    localStorage.removeItem(USER_PASS);
  },

  isLoggedIn() {
    let userData = this.get();
    if (userData) {
      return true
    }

    this.remove();
    return false;
  }
};

export default store;
