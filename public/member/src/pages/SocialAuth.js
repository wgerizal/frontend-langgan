/* eslint-disable */
import React from 'react'
import qs from 'qs'

import store from '../helpers/user_session'

export default class SocialAuth extends React.Component {
  constructor(props) {
    super(props)

  }
  componentDidMount() {
    const { token, email, token_dashboard } = qs.parse(this.props.location.search.slice(1))

    store.set(token)
    store.setUserEmail(email)
    store.setTokenDashboard(token_dashboard)
    this.props.history.push('/member-area')

  }
  render() {


    return (
      <></>
    )
  }
}
