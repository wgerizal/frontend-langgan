/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import LoginIMG from '../../styles/img/login-img.png';
import facebookImg from '../../styles/img/facebook.png';
import googleImg from '../../styles/img/google.png';
import init from '../../dependencies/initializers'
import $ from 'jquery'

import styled from 'styled-components'

import {
    globalUiLoginModal,
    //   globalUiAgreementModal,
    globalUiRegisterModal,
} from '../../actions/globalUi'

import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userResetValidation,
} from '../../actions/users'

class ModalLogin extends Component {

    constructor(props) {
        super(props)
        this.loginModal = React.createRef();
        bindAll(this, [
            'handleClick',
            'handleClose',
            'handleLogin',
            'handleInputChange',
            'OpenModalRegister',
        ])

        this.state = {
            email: '',
            emailState: true,
            pass: '',
            checkbox: true,
        }


    }

    componentWillMount() {
        const {
            userCheckHomePageCredentials
        } = this.props
        userCheckHomePageCredentials()
        document.addEventListener('mousedown', this.handleClick, false)
    }
    componentWillUnmount() {
        document.addEventListener('mousedown', this.handleClick, false)
    }

    OpenModalRegister(e) {
        $("#id02").addClass("show")

        const {
            // globalUiAgreementModal,
            globalUiRegisterModal,
            globalUiLoginModal
        } = this.props
        globalUiLoginModal(false)

        globalUiRegisterModal(true)

    }

    handleInputChange(e) {
        const {
            userResetValidation
        } = this.props
        userResetValidation({})
        if (e.target.name == 'checkbox') {
            return this.setState({ [e.target.name]: !this.state.checkbox })
        }
        if (e.target.name === 'email') {
            const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailRex.test(e.target.value)) {
                this.setState({ emailState: true })
            } else {
                this.setState({ emailState: false })
            }
        }

        this.setState({ [e.target.name]: e.target.value })

    }

    handleLogin(e) {
        e.preventDefault()
        const {
            userAskLoggedIn
        } = this.props


        userAskLoggedIn(this.state)
    }

    handleClick(e) {
        const {
            loginUi,
            globalUiLoginModal
        } = this.props

        const notLogin = loginUi.get('login_modal')
        if (this.loginModal.contains(e.target)) {
            return;
        }
        if (notLogin == true) {
            globalUiLoginModal(!notLogin);
        }

    }

    handleClose(e) {
        const {
            loginUi,
            globalUiLoginModal,
        } = this.props
        const notLogin = loginUi.get('login_modal')
        globalUiLoginModal(!notLogin)
    }

    render() {
        const {
            loginUi,
            handleClose,
            loginError,
        } = this.props
        const {
            email,
            pass,
            checkbox,
        } = this.state

        const notLogin = loginUi.get('login_modal')
        return (
            <div id="id01" className={`modal ${notLogin == true ? "show" : ''}`} >
                <div style={{ display: 'flex' }}>
                    <img className="img-login animate" src={LoginIMG} />
                    <div className="modal-content-login animate" ref={(el) => { this.loginModal = el }}>
                        <div className="imgcontainer">
                            <span onClick={this.handleClose} className="close" title="Close Modal">&times;</span>
                Halo !
            </div>
                        <form onSubmit={this.handleLogin}>
                            <div className="container container-form-modal">
                                <input onChange={this.handleInputChange} value={email} className="custom-input-landing-page" type="email" placeholder="Email" name="email" required />
                                {loginError.get('email') ?
                                    <span className="span-error">{loginError.get('email')}</span>

                                    :
                                    null
                                }

                                <input onChange={this.handleInputChange} value={pass} className="custom-input-landing-page" type="password" placeholder="Password" name="pass" required />
                                {loginError.get('password') ?
                                    <span className="span-error">{loginError.get('password')}</span>

                                    :
                                    null
                                }

                                <div>
                                    <label>
                                        <input type="checkbox" checked={checkbox} onChange={this.handleInputChange} name="checkbox" /> Ingatkan saya
                                    </label>
                                    <Link to={`/lupa-psw`}><span className="psw">Lupa password ?</span></Link>
                                </div>

                                <div className="div-login">
                                    <button onClick={this.handleLogin} style={{ backgroundColor: "#15BFAE" }} type="submit" >Masuk
                  {/* <i className="fa fa-spinner fa-spin m-l-10" /> */}
                                    </button></div>
                            </div>
                        </form>

                        <div className="container container-form-modal background-white">
                            <div className="div-atau">
                                <span>
                                    Atau
                      </span>
                            </div>
                            <div style={{ marginTop: '20px' }}>
                                <SocialButton fb href={`${init.api_host}/api/user/login/f`}><img className="img-sosmed" src={facebookImg} />Masuk Menggunakan Facebook</SocialButton>
                            </div>
                            <div>
                                <SocialButton gg href={`${init.api_host}/api/user/login/g`}><img className="img-google" src={googleImg} />Masuk Menggunakan Google</SocialButton>
                            </div>
                        </div>
                        <div className="container container-form-modal">
                            <label>
                                Belum punya akun? <a onClick={this.OpenModalRegister}>Daftar disini.</a>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const SocialButton = styled.a`
    display:flex;
    justify-content:center;
    align-items:center;
    color:#fff !important;
    margin-top:5px;
    margin-bottom:5px;
    width:100%;
    height:43px;
    background-color:${p => p.fb ? '#3B5998' : (p.gg ? '#4885ED' : 'grey')};
    font-size: 14px;
`


const mapStateToProps = (state, ownProps) => {
    return {
        loginUi: state.loginUi,
        loginError: state.loginError,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
        // globalUiAgreementModal:(modal)=>dispatch(globalUiAgreementModal(modal)),
        globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
        userAskLoggedIn: (data) => dispatch(userAskLoggedIn(data)),
        userCheckHomePageCredentials: () => dispatch(userCheckHomePageCredentials()),
        userResetValidation: (params) => dispatch(userResetValidation(params)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalLogin))
