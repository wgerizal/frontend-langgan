/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import LoginIMG from '../../styles/img/login-img.png';

import {
    globalUiLoginModal,
    globalUiRegisterModal,
    globalUiAgreementModal,
    globalUiActivationModal,
} from '../../actions/globalUi'

import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userRegistrationIsConfirm,
} from '../../actions/users'



class ModalAgree extends Component {

    constructor(props) {
        super(props)
        this.agreeModal = React.createRef();
        this.state = ({
            is_agree: false
        })

        bindAll(this, [
            'setupPage',
            'handleScroll',
            'handleBack',
            'handleSubmit',
        ])


    }

    componentDidMount() {
        this.setupPage()
    }

    setupPage() {
        var agreement = this.agreeModal
        var visibleHeight = agreement.clientHeight;
        var scrollableHeight = agreement.scrollHeight;


        agreement.onscroll = this.handleScroll;

    }

    handleScroll() {
        var agreement = this.agreeModal
        var visibleHeight = agreement.clientHeight;
        var scrollableHeight = agreement.scrollHeight;
        var position = agreement.scrollTop;

        if (position + visibleHeight == scrollableHeight) {
            // document.getElementById("agreebox").disabled=false;
            this.setState({ is_agree: true })

        } else {
            this.setState({ is_agree: true })
        }
    }

    handleBack(e) {
        const {
            globalUiAgreementModal,
            globalUiLoginModal
        } = this.props
        globalUiAgreementModal(false)
        globalUiLoginModal(true)



    }

    handleSubmit(e) {
        const {
            userRegistrationIsConfirm,
            globalUiAgreementModal,
            globalUiActivationModal
        } = this.props
        userRegistrationIsConfirm()
        globalUiAgreementModal(false)
        globalUiActivationModal(true)

    }

    render() {
        const {
            loginUi
        } = this.props
        const {
            is_agree,
        } = this.state

        const is_open = loginUi.get('agreement_modal')

        return (
            <div id="id04" className={`modal center ${is_open == true ? 'show' : ''}`} >
                <div className="modal-agreement animate">
                    <div className="div-label"><label>SYARAT DAN KETENTUAN</label></div>
                    <div id='agreetext' ref={(el) => { this.agreeModal = el }} className="div-p">
                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Definisi</strong></h3>
                        <p><span style={{ fontWeight: 400 }}>PT Berlangganan Indonesia Global adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa pembuatan web. Selanjutnya disebut LANGGAN.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Situs LANGGAN adalah </span><a href="http://www.langgan.id" style={{ color: '#15BFAE', textTransform: 'lowercase' }}><span style={{ fontWeight: 400 }}>www.LANGGAN.id</span></a><span style={{ fontWeight: 400 }}>.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Syarat &amp; ketentuan adalah perjanjian antara Pelanggan dan LANGGAN yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab Pelanggan dan LANGGAN, serta tata cara Pelangganan sistem layanan LANGGAN.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan adalah pihak yang menggunakan layanan LANGGAN.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pembeli adalah Pihak yang membeli produk yang dijual oleh Pelanggan melalui website yang dibuat oleh LANGGAN</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan dengan ini menyatakan bahwa Pelanggan adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.</span></p>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN memungut biaya pendaftaran kepada Pelanggan.</span></p>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat &amp; ketentuan dan/atau hukum yang berlaku, penutupan toko, suspensi akun, dan/atau penghapusan akun Pelanggan.</span></p>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN tidak akan meminta username, password maupun kode SMS verifikasi atau kode OTP milik akun Pelanggan untuk alasan apapun, oleh karena itu LANGGAN menghimbau Pelanggan agar tidak memberikan data tersebut maupun data penting lainnya kepada pihak yang mengatasnamakan LANGGAN atau pihak lain yang tidak dapat dijamin keamanannya.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan dengan ini menyatakan bahwa LANGGAN tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun Pelanggan yang diakibatkan oleh kelalaian Pelanggan, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi (OTP), password atau email kepada pihak lain, maupun kelalaian Pelanggan lainnya yang mengakibatkan kerugian ataupun kendala pada akun Pelanggan.</span></p>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs yang dibuat oleh LANGGAN atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pembeli.</span></p>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN tidak bertanggung jawab atas setiap konten yang dimuat atau diunggah oleh Pelanggan</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan dilarang mempergunakan foto/gambar Barang yang memiliki watermark yang menandakan hak kepemilikan orang lain.</span></p>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Pelanggan adalah tanggung jawab Pelanggan secara pribadi.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Jenis Barang yang&nbsp;Dilarang</strong></h3>
                        <p><span style={{ fontWeight: 400 }}>Berikut ini adalah daftar jenis Barang yang dilarang untuk diperdagangkan oleh Pelanggan pada Situs yang dibuat oleh LANGGAN:</span></p>
                        <ol style={{ marginLeft: '2.5%', marginBottom: '2%', textAlign: 'left' }}>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Segala jenis obat-obatan maupun zat-zat lain yang dilarang ataupun dibatasi peredarannya menurut ketentuan hukum yang berlaku, termasuk namun tidak terbatas pada ketentuan Undang-Undang Narkotika, Undang-Undang Psikotropika, dan Undang-Undang Kesehatan. Termasuk pula dalam ketentuan ini adalah obat keras, obat-obatan yang memerlukan resep dokter, obat bius dan sejenisnya, atau obat yang tidak memiliki izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Kosmetik dan makanan minuman yang membahayakan keselamatan Pelanggannya, ataupun yang tidak mempunyai izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Bahan yang diklasifikasikan sebagai Bahan Berbahaya menurut Peraturan Menteri Perdagangan yang berlaku.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}><span style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}>Jenis Produk tertentu yang wajib memiliki:</span></span></span>
                                <ul style={{ listStyleType: 'disc', marginLeft: '1.5%', textAlign: 'left' }}>
                                    <li style={{ fontSize: '16px' }}>SNI;</li>
                                    <li style={{ fontSize: '16px' }}>Petunjuk Pelangganan dalam Bahasa Indonesia; atau</li>
                                    <li style={{ fontSize: '16px' }}>Label dalam Bahasa Indonesia.</li>
                                    <li style={{ fontSize: '16px' }}>Sementara yang diperjualbelikan tidak mencantumkan hal-hal tersebut.</li>
                                </ul>
                            </li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang-barang lain yang kepemilikannya ataupun peredarannya melanggar ketentuan hukum yang berlaku di Indonesia.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang yang merupakan hasil pelanggaran Hak Cipta, termasuk namun tidak terbatas dalam media berbentuk buku, CD/DVD/VCD, informasi dan/atau dokumen elektronik, serta media lain yang bertentangan dengan Undang-Undang Hak Cipta.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang dewasa yang bersifat seksual berupa obat perangsang, alat bantu seks yang mengandung konten pornografi, serta obat kuat dan obat-obatan dewasa, baik yang tidak memiliki izin edar BPOM maupun yang peredarannya dibatasi oleh ketentuan hukum yang berlaku.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Minuman beralkohol.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Segala bentuk tulisan yang dapat berpengaruh negatif terhadap pemakaian situs ini.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pakaian dalam bekas.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Senjata api, senjata tajam, senapan angin, dan segala macam senjata.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Dokumen pemerintahan dan perjalanan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Seragam pemerintahan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Bagian/Organ manusia.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Mailing list dan informasi pribadi.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang-Barang yang melecehkan pihak/ras tertentu atau dapat merendahkan martabat orang lain.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Atribut kepolisian.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang hasil tindak pencurian.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pembuka kunci dan segala aksesori penunjang tindakan perampokan/pencurian.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang yang dapat dan atau mudah meledak, menyala atau terbakar sendiri.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang cetakan/rekaman yang isinya dapat mengganggu keamanan &amp; ketertiban serta stabilitas nasional.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Uang tunai termasuk valuta asing kecuali Pelanggan memiliki dan dapat mencantumkan izin sebagai Penyelenggara Kegiatan Usaha Penukaran Valuta Asing Bukan Bank berdasarkan Peraturan Bank Indonesia No.18/20/PBI/2016 dan/atau peraturan lainnya yang terkait dengan penukaran valuta asing.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pengacak sinyal, penghilang sinyal, dan/atau alat-alat lain yang dapat mengganggu sinyal atau jaringan telekomunikasi</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Perlengkapan dan peralatan judi.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Jimat-jimat, benda-benda yang diklaim berkekuatan gaib dan memberi ilmu kesaktian.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang dengan hak Distribusi Eksklusif yang hanya dapat diperdagangkan dengan sistem Pelangganan langsung oleh Pelanggan resmi dan/atau Barang dengan sistem Pelangganan Multi Level Marketing.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Produk non fisik yang tidak dapat dikirimkan melalui jasa kurir, termasuk namun tidak terbatas pada produk pulsa/voucher (telepon, listrik, game, dan/atau credit digital), tiket pesawat dan/atau tiket kereta.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Tiket pertunjukan, termasuk namun tidak terbatas pada tiket konser, baik fisik maupun non fisik.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Dokumen-dokumen resmi seperti Sertifikat Toefl, Ijazah, Surat Dokter, Kwitansi, dan lain sebagainya</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Segala jenis Barang lain yang bertentangan dengan peraturan pengiriman Barang Indonesia.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Barang-Barang lain yang melanggar ketentuan hukum yang berlaku di Indonesia.</span></li>
                        </ol>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Pengiriman Barang</strong></h3>
                        <p><span style={{ fontWeight: 400 }}>Pengiriman Barang dalam sistem LANGGAN wajib menggunakan jasa perusahaan ekspedisi yang telah mendapatkan verifikasi rekanan LANGGAN yang dipilih oleh Pembeli.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Setiap ketentuan berkenaan dengan proses pengiriman Barang adalah wewenang sepenuhnya penyedia jasa layanan pengiriman Barang.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan wajib memenuhi ketentuan yang ditetapkan oleh jasa layanan pengiriman barang tersebut dan bertanggung jawab atas setiap Barang yang dikirimkan.</span></p>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan memahami dan menyetujui bahwa setiap permasalahan yang terjadi pada saat proses pengiriman Barang oleh penyedia jasa layanan pengiriman Barang adalah merupakan tanggung jawab penyedia jasa layanan pengiriman.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Kebijakan Pengembalian</strong></h3>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>LANGGAN akan menjamin seluruh layanan yang diberikan kepada Pelanggan akan berjalan dengan baik. Maka dari itu langgan tidak akan memberikan pengembalian dana kepada Pelanggan.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Keamanan</strong></h3>
                        <p><span style={{ fontWeight: 400 }}>LANGGAN selalu berupaya untuk menjaga Layanan LANGGAN aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi terus-menerus atau akses ke Layanan kami dapat selalu sempurna. Informasi dan data dalam situs LANGGAN memiliki kemungkinan tidak terjadi secara real time.</span></p>
                        <p><span style={{ fontWeight: 400 }}>Pelanggan setuju bahwa Anda memanfaatkan Layanan LANGGAN atas risiko Pelanggan sendiri, dan Layanan LANGGAN diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".</span></p>
                        <p><span style={{ fontWeight: 400 }}>Sejauh diizinkan oleh hukum yang berlaku, LANGGAN (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan Anda setuju untuk tidak menuntut LANGGAN bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari :</span></p>
                        <ol style={{ marginLeft: '2.5%', marginBottom: '2%', textAlign: 'left' }}>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pelangganan atau ketidakmampuan Pelanggan dalam menggunakan Layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Harga, Pengiriman atau petunjuk lain yang tersedia dalam layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Keterlambatan atau gangguan dalam Layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Kelalaian dan kerugian yang ditimbulkan oleh masing-masing Pelanggan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Kualitas Barang.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pengiriman Barang.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pelanggaran Hak atas Kekayaan Intelektual.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Perselisihan antar Pelanggan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pencemaran nama baik pihak lain.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Setiap penyalahgunaan Barang yang sudah dibeli pihak Pelanggan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke Rekening Resmi LANGGAN, yang dengan cara apapun mengatas-namakan LANGGAN ataupun kelalaian penulisan rekening dan/atau informasi lainnya dan/atau kelalaian pihak bank.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Pengiriman untuk perbaikan Barang yang bergaransi resmi dari pihak produsen. Pembeli dapat membawa Barang langsung kepada pusat layanan servis terdekat dengan kartu garansi dan faktur pembelian.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Virus atau perangkat lunak berbahaya lainnya (bot, script, automation tool selain fitur Power Merchant, hacking tool) yang diperoleh dengan mengakses, atau menghubungkan ke layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Gangguan, bug, kesalahan atau ketidakakuratan apapun dalam Layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Kerusakan pada perangkat keras Anda dari Pelangganan setiap Layanan LANGGAN.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Isi, tindakan, atau tidak adanya tindakan dari pihak ketiga, termasuk terkait dengan Produk yang ada dalam situs LANGGAN yang diduga palsu.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Tindak penegakan yang diambil sehubungan dengan akun Pelanggan.</span></li>
                            <li style={{ fontSize: '16px' }}><span style={{ fontWeight: 400, fontSize: '16px' }}>Adanya tindakan peretasan yang dilakukan oleh pihak ketiga kepada akun Pelanggan.</span></li>
                        </ol>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Pelepasan</strong></h3>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Jika Anda memiliki perselisihan dengan satu atau lebih Pelanggan, Anda melepaskan LANGGAN (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya, yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut, termasuk namun tidak terbatas pada kerugian yang timbul dari pembelian Barang yang telah dilarang pada Poin Jenis Barang. Dengan demikian maka Pelanggan dengan sengaja melepaskan segala perlindungan hukum (yang terdapat dalam undang-undang atau peraturan hukum yang lain) yang akan membatasi cakupan ketentuan pelepasan ini.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Ganti Rugi</strong></h3>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan akan melepaskan LANGGAN dari tuntutan ganti rugi dan menjaga LANGGAN (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian ini, Pelangganan Layanan LANGGAN yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak ketiga.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Pilihan Hukum</strong></h3>
                        <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.</span></p>

                        <h3 style={{ marginBottom: '1.3%', textAlign: 'left' }}><strong>Pembaharuan</strong></h3>
                        <p><span style={{ fontWeight: 400 }}>Syarat &amp; ketentuan mungkin diubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. LANGGAN menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat &amp; ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan LANGGAN, maka Pelanggan dianggap menyetujui perubahan-perubahan dalam Syarat &amp; Ketentuan.</span></p>
                        <p>&nbsp;</p>
                    </div>

                    <div className="div-button flex-container flex-between">
                        <div className="responsive-button"><button className="btn-white" onClick={this.handleBack} type="submit" style={{ boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)', color: '#15BFAE' }}>Kembali</button></div>
                        <div className="responsive-button"><button onClick={this.handleSubmit} style={{ width: "150px", backgroundColor: '#15BFAE', boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }} className={`btn-green ${is_agree == true ? '' : 'disabled'}`} disabled={!is_agree} type="submit">Saya Setuju</button></div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        loginUi: state.loginUi,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
        globalUiAgreementModal: (modal) => dispatch(globalUiAgreementModal(modal)),
        globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
        globalUiActivationModal: (modal) => dispatch(globalUiActivationModal(modal)),
        userRegistrationIsConfirm: () => dispatch(userRegistrationIsConfirm()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalAgree))
