/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import LoginIMG from '../../styles/img/login-img.png';
import OvalProfile from '../../styles/img/OvalProfile.png';
import init from '../../dependencies/initializers'
import { List, Map, fromJS } from 'immutable'
import Icon from '@material-ui/core/Icon';
import styled from 'styled-components'

import {
  userCheckHomePageCredentials,
  userResetValidation,
  submitResetPassword,
} from '../../actions/users'
import $ from 'jquery'


class ModalChangePassword extends Component {

  constructor(props) {
    super(props)
    this.loginModal = React.createRef();
    bindAll(this, [
      'handleInputChange',
      'handleSubmitForm',
      'resetError',
      'resetState',
      'is_empty',
    ])

    this.state = {
      err_old_password: true,
      err_password: true,
      err_password_confirmation: true,

      //state untuk pengecekan validasi form
      statusPassOld: false,
      statusPass: false,
      statusPassConf: false,
    }


  }
  resetError() {
    this.setState({
      err_old_password: true,
      err_password: true,
      err_password_confirmation: true,
    })
  }

  componentDidMount() {
    $("body").click(function (event) {
      if (event.target.id === "id06") {
        $("#id06").removeClass("show");
      }
    });
    // console.log(this.props)
  }

  resetState() {
    const {
      handleToggle,
    } = this.props
    handleToggle()
    this.setState({
      statusPassOld: false,
      statusPassConf: false,
    }, () => {
      $("body").click(function (event) {
        if (event.target.id === "id06") {
          $("#id06").removeClass("show");
        }
      });
    })
  }
 
  handleSubmitForm(e) {
    const {
      submitResetPassword,
      handleToggle,
    } = this.props

    const {
      //state value
      old_password_val,
      password_val,
      password_confirmation_val,
      statusPassOld,
      statusPass,
      statusPassConf,
    } = this.state;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if (old_password_val === '') {
      this.setState({
        statusPassOld: true
      })
    } else if (password_val === '') {
      this.setState({
        statusPass: true
      })
    } else if (password_confirmation_val === '') {
      this.setState({
        statusPassConf: true
      })
    }  else {
      //jangan rubah state init nanti error kalo mau rubah action nya jg
      submitResetPassword(this.state).then(() => {
        handleToggle()
        this.resetError()
        this.setState({
          statusPassOld: false,
          statusPass: false,
          statusPassConf: false,
        })
      })
    }
  }

  handleInputChange(e) {    
    this.setState({ [e.target.name]: e.target.value })

    switch (e.target.name) {
      case 'old_password_confirmation_val':
        this.setState({ err_old_password: false })
        break;
      case 'password_val':
        this.setState({ err_password: false })
        break;
      case 'password_confirmation_val':
        this.setState({ err_password_confirmation: false })
        break;
      default:
        break;
    }

  }

  is_empty(string) {
    return (string === null || string === '')
  }


  render() {
    const {
      loginUi,
      handleClose,
      userInformation,
      show_changePassword,
      // updateProfileError,
    } = this.props
    const {
      old_password_val,
      password_val,
      password_confirmation_val,
      err_old_password,
      err_password,
      err_password_confirmation,

      //status error form
      statusPassOld,
      statusPass,
      statusPassConf,
    } = this.state

    // const errorUpdate = updateProfileError.get('error')

    let errorOldPassword = '';
    let errorPassword = '';
    let errorPasswordConfirmation = '';

    if (typeof errorUpdate !== 'undefined') {
      if (typeof errorUpdate.address !== 'undefined') {
        if (err_address === true) {
          errorAddress = errorUpdate.address[0];
        }
      }
    }    
    return (
      <div id="id06" className={`modal profile center ${show_changePassword == true ? "show" : ''}`} >
        <div className="modal-content-profile animate" ref={(el) => { this.loginModal = el }}>
            <button id={"id06"} onClick={() => this.resetState()} className="btn-close-modal btn">✖</button>
          <div className="div-profile-preview flex-container flex-start">
            <div className="div-name center">
              <label>Ubah Password</label>
            </div>
          </div>
          <div className="div-form-profile" style={{ marginTop: 0 }}>
            <form>
              <div className="form-group">
                <label>Password Lama</label>
                <input type="password" name='old_password_val' onChange={this.handleInputChange} placeholder="Password Lama" className="form-control" style={{ borderColor: statusPassOld ? 'red' : '#DFDFDF' }} />
                {statusPassOld ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Password salah</span> : null}
              </div>
              <div className="form-group">
                <label>Password Baru</label>
                <input type="password" name='password_val'  onChange={this.handleInputChange} placeholder="Password Baru" className="form-control"/>
                {statusPass ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>The password confirmation does not match.</span> : null}
              </div>
              <div className="form-group">
                <label>Ulangi Password Baru</label>
                <input type="password" name='password_confirmation_val' onChange={this.handleInputChange} placeholder="Ulang Password" className="form-control" style={{ borderColor: statusPassConf ? 'red' : '#DFDFDF' }} />
                {statusPassConf ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}></span> : null}
              </div>
              <div className="form-group flex-between" style={{marginTop: "50%", marginBottom: "10%", display:'flex'}}>
                <div style={{ width: '35%', paddingTop: '18px' }}>
                  <button id={"id06"} onClick={() => this.resetState()} type="button" className="btn-cancel btn btn-md btn-block" style={{ backgroundColor: "inherit", color: "#e74c3c", border: "1px solid #e74c3c", boxShadow: 'none' }}>
                    Batal
                  </button>
                </div>
                <div style={{ width: '35%', paddingTop: '18px' }}>
                  <button disabled={this.is_empty(old_password_val) || this.is_empty(password_val) || this.is_empty(password_confirmation_val)} onClick={this.handleSubmitForm} type="button" className="btn-submit btn btn-md btn-block" style={{ backgroundColor: "#15bfae", boxShadow: 'none' }}>
                    Simpan
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    userInformation: state.userInformation,
    user: state.user,
    updateProfileError: state.updateProfileError,
    changePassError: state.changePassError,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userCheckHomePageCredentials: () => dispatch(userCheckHomePageCredentials()),
    userResetValidation: (params) => dispatch(userResetValidation(params)),
    submitResetPassword: (data) => dispatch(submitResetPassword(data)),
    userRegistration: (data) => dispatch(userRegistration(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalChangePassword))