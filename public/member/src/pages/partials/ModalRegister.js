/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import registerImg from '../../styles/img/register-img.png'
import facebookImg from '../../styles/img/facebook.png';
import googleImg from '../../styles/img/google.png';
import init from '../../dependencies/initializers'
import { Link } from 'react-router-dom'
import {
    globalUiLoginModal,
    globalUiRegisterModal,
} from '../../actions/globalUi'

import styled from 'styled-components'
import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userRegistration,
} from '../../actions/users'


class ModalLogin extends Component {

    constructor(props) {
        super(props)
        this.registrasi_modal = React.createRef();
        bindAll(this, [
            'handleClick',
            'handleClose',
            'handleRegister',
            'handleInputChange',
            'OpenModalLogin',
            'resetError',
        ])

        this.state = {
            email: '',
            pass: '',
            pass_confirm: '',
            err_email: true,
            err_pass: true,
            err_pass_confirm: true,

        }


    }

    resetError() {
        this.setState({ err_email: true })
        this.setState({ err_pass: true })
        this.setState({ err_pass_confirm: true })
    }

    componentWillMount() {
        const {
            userCheckHomePageCredentials
        } = this.props
        userCheckHomePageCredentials()
        document.addEventListener('mousedown', this.handleClick, false)
    }
    componentWillUnmount() {
        document.addEventListener('mousedown', this.handleClick, false)
    }

    OpenModalLogin(e) {
        const {
            globalUiRegisterModal,
            globalUiLoginModal
        } = this.props
        globalUiLoginModal(true)

        globalUiRegisterModal(false)

    }

    handleInputChange(e) {

        this.setState({ [e.target.name]: e.target.value })
        switch (e.target.name) {
            case 'email':
                this.setState({ err_email: false })
                break;
            case 'pass':
                this.setState({ err_pass: false })
                break;
            case 'pass_confirm':
                this.setState({ err_pass_confirm: false })
                break;

            default:
                break;
        }
    }

    handleRegister(e) {
        const {
            userRegistration
        } = this.props

        userRegistration(this.state)
        this.resetError()
    }

    handleClick(e) {
        const {
            loginUi,
            globalUiLoginModal
        } = this.props

        const notLogin = loginUi.get('register_modal')
        if (this.registrasi_modal.contains(e.target)) {
            return;
        }
        if (notLogin === true) {
            globalUiRegisterModal(!notLogin);
        }

    }

    handleClose(e) {
        const {
            loginUi,
            globalUiRegisterModal,
        } = this.props
        const notRegister = loginUi.get('register_modal')
        globalUiRegisterModal(!notRegister)
    }

    render() {
        const {
            loginUi,
            registerError,
        } = this.props
        const {
            email,
            pass,
            pass_confirm,
            err_email,
            err_pass,
            err_pass_confirm,
        } = this.state
        const regiter_modal = loginUi.get('register_modal')
        const errorReg = registerError.get('error')
        var errorEmail = '';
        var errorPass = '';
        var errorPassConf = '';
        if (typeof errorReg !== 'undefined') {
            if (typeof errorReg.email !== 'undefined') {
                if (err_email === true) {
                    errorEmail = errorReg.email[0];
                }
            }
            if (typeof errorReg.password !== 'undefined') {
                if (err_pass === true) {
                    errorPass = errorReg.password[0];
                }

            }
            if (typeof errorReg.password_confirmation !== 'undefined') {
                if (err_pass_confirm) {
                    errorPassConf = errorReg.password_confirmation[0];
                }

            }

        }

        return (
            <div id="id02" className={`modal center ${regiter_modal === true ? "show" : ''}`}>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <div className="modal-content-register animate" ref={(el) => { this.registrasi_modal = el }}>
                        <div className="imgcontainer">
                            <span onClick={this.handleClose} className="close" title="Close Modal">&times;</span>
                            Ayo Gabung !
                        </div>

                        <div className="container container-form-modal">
                            <input onChange={this.handleInputChange} value={email} type="email" placeholder="Email" name="email" required />
                            {errorEmail !== '' ? <span className="span-error" style={{ color: 'red' }}>{errorEmail}</span> : null}

                            <input onChange={this.handleInputChange} value={pass} type="password" placeholder="Password" name="pass" required />
                            {errorPass !== '' ? <span className="span-error" style={{ color: 'red' }}>{errorPass}</span> : null}

                            <input onChange={this.handleInputChange} value={pass_confirm} type="password" placeholder="Ulang Password" name="pass_confirm" required />
                            {errorPassConf !== '' ? <span className="span-error" style={{ color: 'red' }}>{errorPassConf}</span> : null}

                            <div className="div-login"><button style={{ backgroundColor: "#15BFAE" }} onClick={this.handleRegister} type="submit">Daftar</button></div>
                        </div>

                        <div className="container container-form-modal background-white">
                            <div className="div-atau">
                                <span>
                                    Atau
                                </span>
                            </div>
                            <div style={{ marginTop: '20px' }}>
                                <SocialButton fb href={`${init.api_host}/api/user/login/f`}><img className="img-sosmed" src={facebookImg} />Masuk Menggunakan Facebook</SocialButton>
                            </div>
                            <div>
                                <SocialButton gg href={`${init.api_host}/api/user/login/g`}><img className="img-google" src={googleImg} />Masuk Menggunakan Google</SocialButton>
                            </div>
                        </div>
                        <div className="container container-form-modal" style={{ textAlign: 'center', marginTop: '-20px' }}>
                            <div style={{ fontSize: '10px', fontWeight: 'bold' }}>Dengan mendaftar, anda setuju dengan <Link style={{ color: '#3c8dbc' }} to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link> serta <Link style={{ color: '#3c8dbc' }} to={`/privacy-policy`}> Kebijakan Privacy</Link> dari Langgan</div>
                        </div>
                        <div className="container container-form-modal">
                            <label>
                                Sudah punya akun ? <a onClick={this.OpenModalLogin}>Masuk disini.</a>
                            </label>
                        </div>
                    </div>
                    <img className="img-register animate" src={registerImg} />
                </div>
            </div>
        )
    }
}
const SocialButton = styled.a`
    display:flex;
    justify-content:center;
    align-items:center;
    color:#fff !important;
    margin-top:5px;
    margin-bottom:5px;
    width:100%;
    height:43px;
    background-color:${p => p.fb ? '#3B5998' : (p.gg ? '#4885ED' : 'grey')};
    font-size: 14px;
`



const mapStateToProps = (state, ownProps) => {
    return {
        loginUi: state.loginUi,
        registerError: state.registerError

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
        globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
        userAskLoggedIn: (data) => dispatch(userAskLoggedIn(data)),
        userCheckHomePageCredentials: () => dispatch(userCheckHomePageCredentials()),
        userRegistration: (data) => dispatch(userRegistration(data)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalLogin))
