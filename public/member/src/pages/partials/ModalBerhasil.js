/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import IMGVerifyEmail from '../../styles/img/ilus_verifyemail-09.png';

import {
    globalUiLoginModal,
    globalUiRegisterModal,
    globalUiAgreementModal,
    globalUiActivationModal
} from '../../actions/globalUi'

import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userRegistrationIsConfirm,
} from '../../actions/users'



class ModalBerhasil extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            is_agree: false
        })

        bindAll(this, [
            'handleBack',
            'handleSubmit',
        ])


    }

    handleBack(e) {
        const {
            globalUiAgreementModal,
            globalUiLoginModal
        } = this.props
        globalUiAgreementModal(false)
        globalUiLoginModal(true)



    }

    handleSubmit(e) {
        const {
            userRegistrationIsConfirm,
            globalUiLoginModal,
            globalUiActivationModal
        } = this.props
        globalUiLoginModal(true)
        globalUiActivationModal(false)
    }

    render() {
        const {
            loginUi
        } = this.props
        const {
            is_agree,
        } = this.state

        const is_open = loginUi.get('activation_modal')

        return (
            <div id="id05" className={`modal center ${is_open == true ? 'show' : ''}`} >
                <div className="modal-berhasil animate">
                    <div className="div-label"><label>Yeaay !</label></div>
                    <div className="div-img"><img src={IMGVerifyEmail} /></div>
                    <div className="div-p">
                        <p>Untuk aktivasi akun, silahkan cek email kamu lalu klik link yang sudah kami berikan, semoga beruntung ;)</p>
                    </div>
                    <div className="div-button">
                        <div><button className="btn-white-new" onClick={this.handleSubmit} type="submit" style={{ boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }}>Ok, saya mengerti</button></div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        loginUi: state.loginUi,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
        globalUiAgreementModal: (modal) => dispatch(globalUiAgreementModal(modal)),
        globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
        globalUiActivationModal: (modal) => dispatch(globalUiActivationModal(modal)),
        userRegistrationIsConfirm: () => dispatch(userRegistrationIsConfirm()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalBerhasil))
