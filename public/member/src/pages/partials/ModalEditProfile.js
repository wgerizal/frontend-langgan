/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import LoginIMG from '../../styles/img/login-img.png';
import OvalProfile from '../../styles/img/OvalProfile.png';
import init from '../../dependencies/initializers'
import { List, Map, fromJS } from 'immutable'
import Icon from '@material-ui/core/Icon';
import styled from 'styled-components'
import {
  globalUiLoginModal,
  globalUiRegisterModal,
} from '../../actions/globalUi'

import {
  getProvinces,
  getCitys,
} from '../../actions/region'

import {
  userAskLoggedIn,
  userCheckHomePageCredentials,
  userResetValidation,
  userUpdateForm,
} from '../../actions/users'
import $ from 'jquery'


class ModalEditProfile extends Component {

  constructor(props) {
    super(props)
    this.loginModal = React.createRef();
    bindAll(this, [
      'handleSelectedChange',
      'handleInputChange',
      'handleSubmitForm',
      'resetError',
      'handleInputAvatar',
      'fileInput',
      'handleChangeAvatar'
    ])

    this.state = {
      province_val: '',
      city_val: '',
      name_val: '',
      address_val: '',
      pos_code_val: '',
      email_val: '',
      phone_val: '',
      is_wa_number: false,
      err_address: true,
      err_kodepos: true,
      err_name: true,
      err_no_hp: true,
      err_email: true,
      err_province_id: true,

      //state nama kota
      namaKota:'',

      //state untuk pengecekan validasi form
      statusEmail: false,
      statusNama: false,
      statusAlamat: false,
      statusProvinsi: false,
      statusKota: false,
      statusKodepos: false,
      statusNoTelpon: false,
    }


  }
  resetError() {
    this.setState({
      err_address: true,
      err_kodepos: true,
      err_name: true,
      err_no_hp: true,
      err_email: true,
      err_province_id: true
    })
  }

  componentDidMount() {
    const {
      getProvinces,
      userInformation,
      user,
    } = this.props
    getProvinces()
    this.setState({ name_val: user.get('name') })
    this.setState({ email_val: user.get('email') })
    this.setState({ address_val: user.get('address') })
    this.setState({ province_val: user.get('id_prov') })
    this.setState({ city_val: user.get('id_kota') })
    this.setState({ phone_val: user.get('phone') })
    this.setState({ pos_code_val: user.get('postal_code') })
    this.setState({ avatar: user.get('avatar') })
    $("body").click(function (event) {
      if (event.target.id === "id03") {
        $("#id03").removeClass("show");
      }
    });

  }

  resetState() {
    const {
      handleToggle,
    } = this.props
    handleToggle()
    this.setState({
      statusEmail: false,
      statusNama: false,
      statusAlamat: false,
      statusProvinsi: false,
      statusKota: false,
      statusKodepos: false,
      statusNoTelpon: false,
    }, () => {
      $("body").click(function (event) {
        if (event.target.id === "id03") {
          $("#id03").removeClass("show");
        }
      });
    })
  }
  componentDidUpdate(prevProps) {
    const { userInformation, getCitys } = this.props
    if (prevProps.userInformation != this.props.userInformation) {
      this.setState({ name_val: userInformation.get('name') })
      this.setState({ email_val: userInformation.get('email') })
      this.setState({ address_val: userInformation.get('address') })
      this.setState({ province_val: userInformation.get('id_prov') }, () => {
        getCitys(userInformation.get('id_prov'))
          .then(() => {
            this.setState({ city_val: userInformation.get('id_kota') })
          })
      })
      // this.setState({city_val:userInformation.get('id_kota')})
      this.setState({ phone_val: userInformation.get('no_hp') })
      this.setState({ pos_code_val: userInformation.get('kode_pos') })
      this.setState({ avatar: userInformation.get('avatar') })
      // getCitys(value)
    }
    // console.log(userInformation)
  }
  handleInputAvatar() {
    this.fileInput.click()
  }
  fileInput() {

  }
  handleChangeAvatar(e) {
    // console.log(e.target.files[0])
    var file = e.target.files[0]
    this.setState({ photo: file })
    var reader = new FileReader();
    var url = reader.readAsDataURL(file);
    reader.onloadend = function (e) {
      this.setState({
        avatar: reader.result
      })
    }.bind(this);
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  handleSubmitForm(e) {
    const {
      userUpdateForm,
      handleToggle
    } = this.props

    const {
      //state value
      province_val,
      city_val,
      name_val,
      address_val,
      pos_code_val,
      email_val,
      phone_val,
    } = this.state;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if (email_val === '' || !this.validateEmail(email_val)) {
      this.setState({
        statusEmail: true
      })
    } else if (name_val === '') {
      this.setState({
        statusNama: true
      })
    } else if (address_val === '') {
      this.setState({
        statusAlamat: true
      })
    } else if (province_val === '') {
      this.setState({
        statusProvinsi: true
      })
    } else if (city_val === '') {
      this.setState({
        statusKota: true
      })
    } else if (pos_code_val === '') {
      this.setState({
        statusKodepos: true
      })
    } else if (phone_val === '') {
      this.setState({
        statusNoTelpon: true
      })
    } else {
      //jangan rubah state init nanti error kalo mau rubah action nya jg
      userUpdateForm(this.state).then(() => {
        // handleToggle()
        this.resetError()
        this.resetState()
      })
    }
  }

  handleSelectedChange(e) {
    const {
      getCitys
    } = this.props
    const name = e.target.name
    const value = e.target.value
    
    this.setState({ [name]: value }, () => {
      if (name === 'province_val') {
        this.setState({ err_province_id: false })
        getCitys(value)
      }else{
        value
      }
    })
  }

  handleInputChange(e) {
    const {
      userResetValidation
    } = this.props
    userResetValidation({})
    if (e.target.name == 'checkbox') {
      return this.setState({ [e.target.name]: !this.state.checkbox })
    }

    if (e.target.name == 'pos_code_val') {
      const kodePos = e.target.value.slice(0, 5);

      return this.setState({ [e.target.name]: kodePos })
    }
    if (e.target.name == 'phone_val') {
      const phone = e.target.value.slice(0, 13);

      return this.setState({ [e.target.name]: phone })
    }

    this.setState({ [e.target.name]: e.target.value })

    switch (e.target.name) {
      case 'name_val':
        this.setState({ err_name: false })
        break;
      case 'address_val':
        this.setState({ err_address: false })
        break;
      case 'email_val':
        this.setState({ err_email: false })
        break;
      case 'pos_code_val':
        this.setState({ err_kodepos: false })
        break;
      case 'phone_val':
        this.setState({ err_no_hp: false })
        break;
      default:
        break;
    }

  }

  is_empty(string) {
    return (string === null || string === '')
  }

  render() {
    const {
      loginUi,
      handleClose,
      loginError,
      userInformation,
      province,
      city,
      show_profile,
      updateProfileError,
    } = this.props
    const {
      province_val,
      city_val,
      name_val,
      address_val,
      pos_code_val,
      phone_val,
      email_val,
      avatar,
      err_address,
      err_email,
      err_kodepos,
      err_name,
      err_no_hp,
      err_province_id,

      //status error form
      statusEmail,
      statusNama,
      statusAlamat,
      statusProvinsi,
      statusKota,
      statusKodepos,
      statusNoTelpon,
    } = this.state

    const errorUpdate = updateProfileError.get('error')

    let errorAddress = '';
    let errorKodePos = '';
    let errorNoHp = '';
    let errorProvinceId = '';
    let errorName = '';
    let errorEmail = '';

    if (typeof errorUpdate !== 'undefined') {
      if (typeof errorUpdate.address !== 'undefined') {
        if (err_address === true) {
          errorAddress = errorUpdate.address[0];
        }
      }
      if (typeof errorUpdate.kode_pos !== 'undefined') {
        if (err_kodepos === true) {
          errorKodePos = errorUpdate.kode_pos[0];
        }

      }
      if (typeof errorUpdate.no_hp !== 'undefined') {
        if (err_no_hp === true) {
          errorNoHp = errorUpdate.no_hp[0];
        }

      }
      if (typeof errorUpdate.province_id !== 'undefined') {
        if (err_province_id === true) {
          errorProvinceId = errorUpdate.province_id[0];
        }

      }
      if (typeof errorUpdate.name !== 'undefined') {
        if (err_name) {
          errorName = errorUpdate.name[0];
        }

      }
      if (typeof errorUpdate.email !== 'undefined') {
        if (err_email) {
          errorEmail = errorUpdate.email[0];
        }

      }

    }
    const field_nama = name_val
    const field_email = email_val
    const field_address = address_val
    const field_kode_pos = pos_code_val
    const field_is_wa_number = this.state.is_wa_number
    const field_handphone = phone_val
    const field_avatar = avatar
    const first_login = userInformation.get('first_login')
    const provinceOption = []
    const cityOption = []
    provinceOption.push(<option key={'no val province'} value='' disabled > Pilih Provinsi ... </option>)

    province.map((prov, index) => {
      provinceOption.push(<option value={prov.id} key={index}>{prov.name}</option>)
    })

    cityOption.push(<option key={'no val city'} value='' disabled > Pilih Kota ... </option>)

    city.map((ct, index) => {
      cityOption.push(<option value={ct.id} key={index}>{ct.name}</option>)
    })



    return (
      <div id={"id03"} className={`modal profile center ${first_login == 0 || show_profile == true ? "show" : ''}`} >
        <div className="modal-content-profile animate" ref={(el) => { this.loginModal = el }}>
          {first_login == 0
            ? null :
            <button id={"id03"} onClick={() => this.resetState()} className="btn-close-modal btn">✖</button>}
          <div className="div-profile-preview flex-container flex-start">
            <div onClick={this.handleInputAvatar} className="div-image">
              <img src={field_avatar} />
              <div className="label-upld">Upload foto</div>
              <input
                ref={fileInput => this.fileInput = fileInput}
                onChange={(e) => this.handleChangeAvatar(e)} accept="image/*" id="file" type="file" style={{ display: 'none' }} />
            </div>
            <div className="div-name">
              <label>{userInformation.get('name')}</label>
              <p>{field_address}, {field_kode_pos}</p>
            </div>
          </div>
          <div className="div-form-profile" style={{ marginTop: 0 }}>
            <form>
              <div className="form-group">
                <label>Email</label>
                <input type="text" name='email_val' value={field_email} onChange={this.handleInputChange} className="form-control" style={{ borderColor: statusEmail ? 'red' : '#DFDFDF' }} />
                {statusEmail ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Email kamu belum sesuai</span> : null}
              </div>
              <div className="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name='name_val' value={field_nama} onChange={this.handleInputChange} className="form-control" style={{ borderColor: statusNama ? 'red' : '#DFDFDF' }} />
                {statusNama ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Nama kamu tidak boleh kosong</span> : null}
              </div>
              <div className="form-group" style={{ marginBottom: '10px' }}>
                <label>
                  Alamat Lengkap
                  <div className="question-tooltip">
                    <div className="icn"><i className="fa fa-question-circle-o" /></div>
                    <div className="text">Alamat ini yang nantinya akan digunakan untuk alamat pengiriman</div>
                  </div>
                </label>
                <textarea autocorrect="off" spellcheck="false" className="form-control" rows="3" name='address_val' value={field_address} onChange={this.handleInputChange} style={{ borderColor: statusAlamat ? 'red' : '#DFDFDF', backgroundColor: '#FFFFFF' }} />
                {statusAlamat ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Alamat kamu tidak boleh kosong</span> : null}
              </div>
              <div className="form-group flex-container flex-between" style={{ flexWrap: 'wrap' }}>
                <div style={{ width: '34%' }}>
                  <select name='province_val' className="form-control custom-select" value={province_val} onChange={this.handleSelectedChange} placeholder='Pilih Provinsi' id="sel1" style={{ borderColor: statusProvinsi ? 'red' : '#DFDFDF', backgroundColor: '#FFFFFF' }}>
                    {provinceOption}
                  </select>
                </div>
                <div style={{ width: '34%' }}>
                  <select name='city_val' value={city_val} onChange={this.handleSelectedChange} className="form-control pilihanKota" id="sel1" placeholder={'Pilih Kota'} style={{ borderColor: statusKota ? 'red' : '#DFDFDF', backgroundColor: '#FFFFFF' }}>
                    {cityOption}
                  </select>
                </div>
                <div style={{ width: '27%' }}>
                  <div className="kodePosForm">
                    <input maxLength="11" type="number" className="form-control" name='pos_code_val' value={field_kode_pos} placeholder="Kode Pos" onChange={this.handleInputChange} style={{ borderColor: statusKodepos ? 'red' : '#DFDFDF' }} />
                  </div>
                </div>
                <div style={{ width: '100%' }}>
                  {statusProvinsi ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Kamu belum memilih Provinsi</span> : null}
                  {statusKota ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Kamu belum memilih kota</span> : null}
                  {statusKodepos ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Kode Pos kamu tidak boleh kosong</span> : null}
                </div>
              </div>
              <div className="form-group flex-container flex-between">
                <div style={{ width: '62%', height: 50 }}>
                  <label>Nomor Telepon</label>
                  <input type="number" className="form-control" name='phone_val' value={field_handphone} placeholder="Nomer Telepon" onChange={this.handleInputChange} style={{ borderColor: statusNoTelpon ? 'red' : '#DFDFDF' }} />
                  {statusNoTelpon ? <span style={{ color: 'red', fontSize: 10, fontStyle: 'italic' }}>Nomer Telepon kamu tidak kosong</span> : null}
                </div>
                <div style={{ width: '35%', paddingTop: '18px' }}>
                  <button disabled={this.is_empty(province_val) || this.is_empty(city_val) || this.is_empty(name_val) || this.is_empty(address_val) || this.is_empty(pos_code_val) || this.is_empty(phone_val) || this.is_empty(email_val)} onClick={this.handleSubmitForm} type="button" className="btn-submit btn btn-md btn-block" style={{ backgroundColor: "#15bfae", boxShadow: 'none' }}>
                    Simpan
                  </button>
                </div>
                <div class="checkbox" style={{ paddingBottom: '20px' }}>
                  <label><input type="checkbox" onChange={(e) => this.setState({ is_wa_number: !field_is_wa_number })} checked={field_is_wa_number} /> Nomor diatas bisa dihubungi melalui whatsapp</label>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const SocialButton = styled.a`
    display:flex;
    justify-content:center;
    align-items:center;
    color:#fff !important;
    margin-top:5px;
    margin-bottom:5px;
    width:100%;
    height:43px;
    background-color:${p => p.fb ? '#3B5998' : (p.gg ? '#4885ED' : 'grey')};
    font-size: 14px;
`


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    loginError: state.loginError,
    userInformation: state.userInformation,
    province: state.province,
    city: state.city,
    user: state.user,
    updateProfileError: state.updateProfileError,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
    userAskLoggedIn: (data) => dispatch(userAskLoggedIn(data)),
    userCheckHomePageCredentials: () => dispatch(userCheckHomePageCredentials()),
    userResetValidation: (params) => dispatch(userResetValidation(params)),
    getProvinces: () => dispatch(getProvinces()),
    getCitys: (province_id) => dispatch(getCitys(province_id)),
    userUpdateForm: (data) => dispatch(userUpdateForm(data)),

  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalEditProfile))


{/* <center style={{ marginTop: statusKodepos || statusNoTelpon ? 80 : 20 }}>
                <label style={{ color: '#565656', fontWeight: 'bold', fontSize: 15 }}>Pengaturan Kata Sandi</label>
              </center>
              <div className="form-group">
                <label>Kata Sandi Baru</label>
                <input type="password" className="form-control" name='password' placeholder={'Ganti Kata Sandi'} onChange={this.handleInputChange} /> */}
{/* <label>Kata Sandi Baru</label>
                                <input type="password" className="form-control" name='passwordBaru' onChange={this.handleInputChange} />
                                <label>Konfirmasi Kata Sandi</label>
                                <input type="password" className="form-control" name='passwordKonfirmasi' onChange={this.handleInputChange} /> */}
{/* </div> */ }