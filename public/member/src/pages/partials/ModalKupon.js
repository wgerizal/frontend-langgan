/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import LoginIMG from '../../styles/img/login-img.png';
import OvalProfile from '../../styles/img/OvalProfile.png';
import init from '../../dependencies/initializers'
import { List, Map, fromJS } from 'immutable'
import styled from 'styled-components'

import {
    globalUiLoginModal,
    globalUiRegisterModal,
} from '../../actions/globalUi'

import {
    getProvinces,
    getCitys,
} from '../../actions/region'

import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userResetValidation,
    userUpdateForm,
} from '../../actions/users'

class ModalKupon extends Component {

    constructor(props) {
        super(props)
        bindAll(this, [
            'handleGunakan',
            'handleLewati',
        ])

        this.state = {
            show_kupon: 0
        }


    }

    componentDidMount() {

    }


    render() {

        return (
            <div id="id03" className={`modal profile center ${this.state.show_kupon == 1 ? "show" : ''}`} >
                <div className="modal-content-profile animate" ref={(el) => { this.loginModal = el }}>
                    <div className="div-form-profile">
                        <form>
                            <div className="form-group">
                                <label>Silahkan masukan kode voucher untuk mendapatkan harga yang lebih murah</label>
                                <input type="text" name='name_val' onChange={this.handleInputChange} className="form-control" />
                            </div>
                        </form>
                    </div>
                    <div className="div-login">
                        <div className="div-button flex-container flex-between">
                            <div><button className="btn-white" onClick={this.handleGunakan} type="submit">Gunakan</button></div>
                            <div><button onClick={this.handleLewati} style={{ width: "150px" }} className={`btn-green ${is_agree == true ? '' : 'disabled'}`} disabled={!is_agree} type="submit">Lewati</button></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



const mapStateToProps = (state, ownProps) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),

    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalKupon))
