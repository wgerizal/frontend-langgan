/* eslint-disable */
import React, { Component } from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import CreateToko from '../../styles/img/ilus_tokodibangun-06.png';
import { push } from 'react-router-redux';
import {
    globalUiLoginModal,
    globalUiRegisterModal,
    globalUiAgreementModal,
    globalUiActivationModal
} from '../../actions/globalUi'

import {
    userAskLoggedIn,
    userCheckHomePageCredentials,
    userRegistrationIsConfirm,
} from '../../actions/users'

import {
    tokoFetchData,
} from '../../actions/toko'


class ModalReminderToko extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            is_agree: false
        })

        bindAll(this, [
            'handleBack',
            'handleSubmit',
        ])


    }

    //     componentDidMount(){
    //     const{
    //       tokoFilterData,
    //       tokoFetchData,
    //       user
    //     }=this.props
    //     tokoFetchData(tokoFilterData)
    //     // io('https://notif.langgan.id').on(`channel:member-56`, (message) => {
    //     //   console.log(message)
    //     // })
    //   }

    handleBack(e) {
        const {
            globalUiAgreementModal,
            globalUiLoginModal
        } = this.props
        globalUiAgreementModal(false)
        globalUiLoginModal(true)



    }

    handleSubmit(e) {
        const { redirect } = this.props
        redirect('/member-paket')
    }


    render() {
        const message_list = [{
            title: "Oops !",
            content: "Kamu belum buat toko sebelumnya, ayo buat tokomu dengan sesuka hati, niat, kemauan dan mood :D",
        }, {
            title: "Oops, toko tidak berhasil dibuat",
            content: "Kamu tidak melakukan pembayaran dalam 1x24 jam, namun jangan khawatir. kamu masih bisa membuat toko dari awal.",
        }, {
            title: "Oops, toko tidak berhasil dibuat",
            content: "Domain sudah ada yang menggunakan sebelum melakukan pembayaran. Silahkan buat toko ulang",
        }]
        const {
            loginUi,
            userInformation,
            tokos,
            jumlah_toko
        } = this.props
        const {
            is_agree,
        } = this.state

        const first_login = userInformation.get('first_login')
        const is_open = loginUi.get('activation_modal')
        let show = false;
        let type_message = 0;
        if (jumlah_toko === 0 && first_login == 1) {
            show = true;
            type_message = 1;
        }
        return (
            <div id="id05" className={`modal center ${show == true ? "show" : ''}`}>
                <div className="modal-berhasil animate" style={{ height: '450px' }}>
                    <div className="div-label"><label>{message_list[type_message].title}</label></div>
                    <div className="div-img"><img src={CreateToko} /></div>
                    <div className="div-p ">
                        <p style={{ textAlign: 'center' }}>{message_list[type_message].content}</p>
                    </div>
                    <div className="div-button">
                        <div><button className="btn btn-green" onClick={this.handleSubmit} type="submit" style={{ backgroundColor: "#15bfae" }}>Buat Toko</button></div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        loginUi: state.loginUi,
        userInformation: state.userInformation,
        tokoFilterData: state.tokoFilterData,
        tokos: state.tokos,
        jumlah_toko: state.jumlah_toko,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        redirect: (params) => dispatch(push(params)),
        tokoFetchData: (filter) => dispatch(tokoFetchData(filter)),
        globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
        globalUiAgreementModal: (modal) => dispatch(globalUiAgreementModal(modal)),
        globalUiRegisterModal: (modal) => dispatch(globalUiRegisterModal(modal)),
        globalUiActivationModal: (modal) => dispatch(globalUiActivationModal(modal)),
        userRegistrationIsConfirm: () => dispatch(userRegistrationIsConfirm()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalReminderToko))
