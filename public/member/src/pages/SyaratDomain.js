/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'
import Navbar from './components/Navbar'
import Footer from './components/Footer'

import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  userLogout,
  submitForgotPass,
} from '../actions/users'
// import './styles/App.css';

class SyaratDomain extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleChange',
      'handleSubmitForgotPass',
    ])

    this.state = {
      modal: false,
      email: '',
    }

  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      globalUiLoginModal,
      loginUi,
    } = this.props

    globalUiLoginModal(false)

    //Custom forms
    $(function () {
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    if (isLogged == false) {
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }



  }

  handleChange(e) {
    this.setState({ email: e.target.value })
  }

  handleSubmitForgotPass(e) {
    const {
      submitForgotPass,
    } = this.props
    let email = this.state.email
    submitForgotPass(email)
  }

  render() {
    const {
      user
    } = this.props


    const isLogged = user.get('logged_in')

    return (
      <div className="tema">
        <Navbar />
        <section className="text-left" style={{ paddingTop: "100px", background: "#fff" }}>
          {/* FIRST BLOCK */}
          <div id="syarat-domain-block" className="text-left">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="s-12 m-12 l-12">
                    <h2 style={{ marginBottom: '2%', textAlign: 'center' }}><strong>Domain</strong></h2>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Kenapa harus punya domain?</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Domain adalah alamat kita di dunia maya. Domain selalu terdiri dari minimal 2 silabel tanpa spasi dengan tanda titik di tengah contoh : langgan.id, google.com, jd.id, disdukcapil.bandung.go.id , dsb.</span></p>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Domain menjadi krusial dalam berbisnis karena pembeli akan lebih percaya ke penjual-penjual yang memiliki website dan domainnya sendiri. Selain itu, domain akan memudahkan pembeli untuk mencari penjual yang sesuai melalui mesin pencari seperti Google, Bing &amp; Baidu. Semakin tinggi posisi domain di mesin pencari, semakin besar kemungkinan pembeli akan masuk ke website dan membeli produk kita.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Unsur - Unsur domain</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Sebelum kita memilih nama domain yang cocok dengan bisnis kita, ada baiknya kita baca dulu penjelasan singkat dibawah : Domain terdiri dari 3 unsur yaitu :</span></p>
                    <ol style={{ marginLeft: '2%', marginBottom: '2%' }}>
                      <li style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}>Top Level Domain (TLD), yaitu silabus di belakang titik seperti .com , .id , .co.id.</span></li>
                      <li style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}>Second Level Domain (2LD), yaitu silabus di depan titik seperti langgan.id , google.com , jd.id.</span></li>
                      <li style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}>Third Level Domain (3LD), yaitu silabus sebelum TLD &amp; 2LD, seperti webmail.langgan.id domain ini memang belum lumrah digunakan. Biasanya digunakan jika ada informasi atau transaksi yang rumit dalam sebuah website sehingga membutuhkan penambahan halaman baru.</span></li>
                    </ol>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Jenis - Jenis Top Level Domain</strong></h3>
                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.com</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Domain ini paling sering digunakan dan cocok buat jualan. Awalnya dipakai oleh perusahaan tapi sekarang banyak dipakai juga untuk semua website yang bersifat komersial.</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.net</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Nama domain .net adalah ranah internet tingkat teratas (TLD) yang digunakan dalam Sistem Penamaan Domain Internet. Nama .net diambil dari kata network (jaringan).</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.id</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Jenis domain ini cocok banget buat yang punya produk / jasa unik dari Indonesia. Diantara jutaan website yang ada, identitas asal negara jadi penting karena mempengaruhi keputusan seseorang untuk membeli produk / jasa. Seperti slogan yang sering kita dengar &ldquo;Cintailah produk-produk Indonesia&rdquo;</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.co.id</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Domain untuk badan usaha atau kegiatan komersial ini yang unik karena kita butuh beberapa persyaratan supaya bisa memakainya.</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.web.id</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Domain .web.id digunakan untuk Website personal. Persyaratan aktivasinya relatif cukup mudah. Pengguna cukup menunjukkan tanda bukti berupa KTP.</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.biz.id</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%' }}><span style={{ fontWeight: 400 }}>Domain .biz sejarahnya diciptakan untuk memenuhi permintaan pasar bisnis yang melewatkan kesempatan untuk menggunakan nama domain .com. Sebab sesuai dengan pengucapannya sendiri yang membentuk kata Business.&nbsp;</span></p>

                    <ul style={{ marginLeft: '2%' }}>
                      <li style={{ fontWeight: 400 }}><strong>.my.id</strong></li>
                    </ul>
                    <p style={{ marginLeft: '2%', marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Domain .my.id, karena bisa digunakan siapa saja untuk mengekspresikan diri dengan tulisan, blog, bisnis online, bahkan email pribadi. Selain itu Nama Domain my.id mudah diingat dan bisa merepresentasikan diri sebagai my identity atau my international domain.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Memilih domain yang tepat</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Pilihlah domain yang mudah diingat dan dengan ejaan yang mudah dibaca. Pastikan Second Level Domain (2LD) yang dipilih sesuai dengan sesuai dengan apa yang ditampilkan website kita.</span></p>

                  </article>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* FOOTER */}
        <Footer />
      </div>
    );

  }

}


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    submitForgotPass: (email) => dispatch(submitForgotPass(email))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SyaratDomain))
