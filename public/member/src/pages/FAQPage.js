/* eslint-disable */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import Script from 'react-load-script';
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'

import WALogo from '../styles/img/wa.png';

import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  userLogout,
  submitForgotPass,
  userSubscribe,
} from '../actions/users'

// import './styles/App.css';

class FAQPage extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleSubscribe',
      'handleSubmitSubscribe',
    ])

    this.state = {
      modal: false,
      email: '',
      email_subscribe: ''
    }

  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      globalUiLoginModal,
      loginUi,
    } = this.props

    globalUiLoginModal(false)
    //Responsee tabs
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Custom forms
    $(function () {
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    });
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    if (isLogged == false) {
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }
  }

  handleSubscribe(e) {
    this.setState({ email_subscribe: e.target.value })
  }
  handleSubmitSubscribe() {

    const {
      userSubscribe
    } = this.props

    userSubscribe(this.state.email_subscribe)

    this.setState({
      email_subscribe: ''
    })

  }

  //fungsi whatsapp button
  handleScriptCreate() {
    this.setState({ scriptLoaded: false })
  }
  
  handleScriptError() {
    this.setState({ scriptError: true })
  }
  
  handleScriptLoad() {
    this.setState({ scriptLoaded: true })
  }


  render() {
    const {
      user
    } = this.props


    const isLogged = user.get('logged_in')

    return (
      <div className="tema">
        {/* <ModalLogin modal={modal} handleClose={this.handleLogin}/> */}
        {/* TOP NAV WITH LOGO */}
        <header>
          <nav className="nav-fixed">
            <div className="line">
              <div className="s-12 m-3 l-2 div-langgan-logo">
                <Link to={`/`}><img className="s-5 l-12 img-langgan-logo" src={LangganLogo} /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  <li><Link onClick={this.closeNav} className="menu-tab" to={`/#second-block`}>Fitur</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab" to={`/tema-page`}>Tema</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab" to={`/#pilihpaket-block`}>Harga</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab" to={`/#fourth-block`}>Hubungi Kami</Link></li>
                  <li><a href="http://blog.langgan.id/" target="_blank" className="menu-tab">Blog</a></li>
                  <li className="li-line-bound"><a className="line-bound">|</a></li>
                  {isLogged == false ?
                    <li><a onClick={this.handleLogin} className="menu-tab" style={{ color: '#15BFAE' }}>Masuk / Daftar</a></li>
                    :
                    <Fragment>
                      <li><Link to={`/member-area`} className="menu-tab" style={{ color: '#15BFAE' }}>Member Area</Link></li>
                      <li><a onClick={this.handleLogin} className="menu-tab" style={{ color: '#15BFAE' }}>Keluar</a></li>
                    </Fragment>
                  }

                </ul>
              </div>
            </div>
          </nav>
        </header>
        <div className="">
          <Script
            url="https://widget.tochat.be/bundle.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}
            attributes={{key:"1d3e8f55-968b-46eb-9aea-53301a6512db"}}
          />
          {/* <div className="text-right"><a target="_blank" href="https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..." className="btn btn-hubungi-wa" type="submit"><i class="fa fa-whatsapp"></i></a></div> */}
        </div>
        <section className="text-left">
          {/* FIRST BLOCK */}
          <div id="faq-block" className="text-left">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="s-12 m-12 l-12 left">
                    <h1 className="center">FAQ</h1>
                    <h2>Mulai menggunakan Langgan :</h2>
                    <label>Apa itu Langgan dan bagaimana Langgan Bekerja?</label>
                    <p>Langgan adalah platform perdagangan online lengkap yang memungkinkan Anda memulai, menumbuhkan, dan mengelola bisnis secara Online.</p>
                    <p>Buat dan atur toko online, Kelola produk, inventaris, pembayaran, dan pengiriman. Langgan sepenuhnya berbasis cloud, yang berarti Anda tidak perlu khawatir tentang peningkatan atau pemeliharaan perangkat lunak atau server web. Hal ini memberi Anda fleksibilitas untuk mengakses dan menjalankan bisnis Anda dari mana saja dengan koneksi internet.</p>
                    <label>Apakah saya harus memiliki kemampuan Design dan Coding untuk menggunakan Langgan?</label>
                    <p>Tidak, Anda tidak perlu menjadi desainer atau developer untuk menggunakan Langgan. Sesuaikan tampilan dan nuansa toko Anda dengan pembuat dan tema toko online.</p>
                    <p>Anda akan memiliki seluruh fitur yang ditawarkan oleh Langgan. Jika Anda memerlukan bantuan tambahan, Anda dapat menghubungi tim dukungan kami.</p>
                    <label>Berapa biaya yang dibutuhkan untuk menggunakan Langgan?</label>
                    <p>Langgan menyediakan 3 macam skema pembayaran sesuai dengan kebutuhan anda, dimulai dari Rp. 300.000 (diluar Ppn) untuk masa berlangganan 1 bulan sampai Rp.2.500.000 (diluar Ppn) untuk masa berlangganan 1 tahun.</p>
                    <p>Gunakan berbagai macam cara pembayaran untuk menggunakan aplikasi Langgan.</p>
                    <label>Bisakah saya menggunakan domain saya sendiri di Langgan?</label>
                    <p>Ya, anda dapat menggunakan domain anda sendiri. Langgan juga memberikan gratis domain ( .com, .id, .web.id, .net, .my.id .co.id, .biz.id) selama anda menggunakan layanan dari Langgan.</p>
                    <p>Apabila anda sudah memiliki domain sebelumnya anda dapat menghubungi Technical Support untuk mengintegrasikan domain anda ke layanan Langgan.</p>
                    <label>Apa saja dokumen yang dibutuhkan untuk mendapatkan domain di Langgan?</label>
                    <p>Untuk domain .com, .id, .net, .my.id  dan .biz.id anda tidak diwajibkan untuk memberikan dokumen apapun ke Langgan.</p>
                    <p>Syarat untuk domain .web.id anda harus mengunggah KTP Republik Indonesia</p>
                    <p>Syarat untuk domain .co.id anda harus mengunggah beberapa dokumen:</p>
                    <ul>
                      <li>KTP Republik Indonesia.</li>
                      <li>SIUP / TDP / AKTA / Surat Ijin Setara.</li>
                    </ul>
                    <h2>Berjualan di Langgan :</h2>
                    <label>Apa yang saya butuhkan untuk mulai berjualan di Langgan?</label>
                    <p>Untuk mulai berjualan di Langgan, anda membutuhkan akun dan menyelesaikan proses pembayaran untuk skema pembelian yang anda pilih.</p>
                    <p>Unggah produk-produk yang akan anda jual dan anda sudah bisa langsung berjualan menggunakan Langgan.</p>
                    <label>Dimana saja saya dapat berjualan menggunakan Langgan?</label>
                    <p>Langgan dapat membantu anda untuk berjualan kemana saja.</p>
                    <p>Mengoptimalkan seluruh aset social media dan marketplace dapat membantu anda untuk meningkatkan penjualan anda.</p>
                    <label>Apa yang terjadi ketika saya menerima pesanan?</label>
                    <p>Anda akan mendapatkan pemberitahuan melalui Bisnis Kit (Dashboard) Langgan dan melalui aplikasi mobile yang dapat di unduh melalui Apps Store dan Play Store.</p>
                    <h2>Pembayaran di Langgan :</h2>
                    <label>Apa itu payment gateway pihak ketiga?</label>
                    <p>Sistem pembayaran pihak ketiga adalah sebuah jasa aplikasi yang dapat membantu anda menerima dan mengkonfirmasi pembayaran dari pembeli anda secara otomatis.</p>
                    <p>Saat ini Langgan bekerjasama dengan midtrans.com.</p>
                    <label>Apa itu Merchant ID?</label>
                    <p>Merchant ID adalah identitas akun anda yang terdaftar di midtrans.com. </p>
                    <p>Melalui akun yang terdaftar di midtrans.com anda pembayaran yang anda terima akan di tampung dan dapat anda cairkan kapanpun (waktu pencairan dana merupakan kebijakan dari midtrans.com)</p>
                    <label>Bagaimana Jika saya tidak ingin menggunakan payment gateway?</label>
                    <p>Kami telah menyediakan juga pilihan manual transfer jika anda tidak ingin menggunakan payment gateway yang telah disediakan, silahkan isikan informasi mengenai rekening bank anda sebagai informasi kepada pembeli anda untuk melakukan pembayaran</p>
                    <label>Apakah ada dokumen legal yang perlu saya kirimkan jika ingin menggunakan payment gateway?</label>
                    <p>Iya. Anda perlu mengirimkan beberapa dokumen legal untuk menerima pembayaran melalui Midtrans. Sebagian besar dari aplikasi payment channel membutuhkan dokumen legal berikut ini:</p>
                    <ol>
                      <li>Fotokopi atau hasil scan dari ID (KTP / KITAS / Paspor);</li>
                      <li>Fotokopi atau hasil scan dari NPWP - Nomor Pokok Wajib Pajak;</li>
                      <li>Form Permohonan Merchant yang akan dikirimkan oleh Midtrans; dan</li>
                      <li>Perjanjian Kerjasama yang akan dikirimkan oleh Midtrans.</li>
                    </ol>
                    <p>Untuk lebih jelasnya silahkan klik <a href="https://support.midtrans.com/hc/id/articles/115011159927-Apakah-ada-dokumen-legal-yang-perlu-saya-kirimkan-" target="_blank">disini</a>.</p>
                    <label>Bagaimana cara saya mulai menggunakan Payment Gateway untuk menerima pembayaran?</label>
                    <p>Karena saat ini kami bekerja sama dengan midtrans, berikut cara menggunakan payment gateway midtrans.</p>
                    <p>Hal pertama yang dapat Anda lakukan adalah membuat akun di Merchant Administration Portal, yang akan membantu Anda dalam melihat dan mengelola transaksi. Setelah Anda terdaftar, Anda akan berada di mode development/sandbox, dimana Anda akan menemukan beberapa dummy transaksi yang bisa Anda coba. Anda belum dapat melakukan transaksi sebenarnya.</p>
                    <p>Setelah akun Anda sudah dibuat:</p>
                    <ol>
                      <li>Tim Solutions Midtrans akan menghubungi Anda untuk berdiskusi mengenai kebutuhan bisnis Anda dan memberikan informasi persyaratan dokumen legal;</li>
                      <li>Sementara, Anda atau tim developer Anda dapat melakukan integrasi antara sistem Midtrans dengan sistem Anda (e-commerce website, aplikasi mobile, dsb.) seperti yang terdapat di dokumentasi teknis. Setelah selesai, silahkan coba melakukan transaksi dummy;</li>
                      <li>Tim Midtrans akan memberikan informasi mengenai aplikasi Anda jika sudah diterima oleh Midtrans dan/atau Bank, dan Anda akan menerima credentials pada mode Production (contohnnya Merchant ID atau Merchant Key);</li>
                      <li>Anda dapat menggunakan credentials Production ke sistem Anda dan memindahkan MAP akun dari Sandbox ke Production;</li>
                      <li>Silahkan lakukan tes transaksi sebenarnya, dan Anda siap menerima pembayaran pelanggan Anda melalui Midtrans.</li>
                    </ol>
                    <p>Untuk lebih jelasnya silahkan klik <a href="https://support.midtrans.com/hc/id/articles/204173464-Bagaimana-cara-saya-mulai-menggunakan-Midtrans-untuk-menerima-pembayaran-" target="_blank">disini</a>.</p>
                    <label>Bisnis apa saja yang dapat menggunakan payment gateway?</label>
                    <p>Berbagai bisnis online (penjual individu, UKM, eCommerce multinational, dsb) dapat bekerja sama dengan Midtrans, namun Midtrans tidak dapat bekerjasama dengan bisnis yang menjual produk/jasa di bawah ini:</p>
                    <ol>
                      <li>Obat-obatan dan/atau zat terlarang;</li>
                      <li>Material yang berkaitan dengan pornografi;</li>
                      <li>Jasa perjudian online;</li>
                      <li>Produk palsu;</li>
                      <li>Produk atau barang lainnya yang tidak diperbolehkan oleh pemerintah untuk diperjualbelikan.</li>
                    </ol>
                    <h2>Pengiriman barang di Langgan :</h2>
                    <label>Apakah saya harus mengirim barang saya sendiri?</label>
                    <p>Hal ini tergantung dengan jasa layanan kurir dan perjanjian yang anda sepakati dengan pihak ekspedisi.</p>
                    <label>Apakah ada biaya tambahan terkait fitur shipping?</label>
                    <p>Langgan tidak memungut biaya dari fitur shipping ini, anda hanya tinggal mengaktifkan shipping yang anda suka tanpa ada beban biaya alias gratis.</p>
                    <label>Bagaimana saya dapat melacak orderan pembeli?</label>
                    <p>Beberapa jasa ekspedisi di Langgan memiliki fitur lacak paket, silahkan ke menu pengiriman dan pilih menu lacak pengiriman.</p>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* FOOTER */}
        <footer className="footer-desktop">
          <div className="line vertical-align-center">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
              <div className="s-12 l-4 social-btn-div text-center p-t-20">
                <div className="m-b-20">
                  <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                    <i className="fa fa-twitter"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                    <i className="fa fa-linkedin"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                    <i className="fa fa-youtube"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="s-12 l-4 text-right subscribe">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
        </footer>

        <footer className="footer-mobile">
          <div className="line vertical-align-center">
            <div className="s-12 l-12">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-4 social-btn-div text-center p-t-20">
              <div className="m-b-20">
                <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                  <i className="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                  <i className="fa fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                  <i className="fa fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                  <i className="fa fa-linkedin"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                  <i className="fa fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
          <div className="line vertical-align-center m-t-10">
            <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
              <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 text-left subscribe-mobile">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-4 l-2 text-left">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-4 l-2 text-left">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
          </div>
        </footer>
      </div>
    );

  }

}


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    submitForgotPass: (email) => dispatch(submitForgotPass(email)),
    userSubscribe: (email) => dispatch(userSubscribe(email)),

  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FAQPage))
