/* eslint-disable */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import Script from 'react-load-script';
import Pagination from './components/Pagination'
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'

import WALogo from '../styles/img/wa.png';

import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  getTema,
  temaAddFilterDataPage,
  temaAddFilterDataPerPage,
  temaAddFilterDataFilter,
} from '../actions/tema'

import {
  userLogout,
  userSubscribe,
} from '../actions/users'
// import './styles/App.css';

class TemaPage extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleChangeInput',
      'handleSubmitSearch',
      'handlePreview',
      'handleSubscribe',
      'handleSubmitSubscribe',
    ])

    this.state = {
      modal: false,
      tema_val: '',
      tema_preview: false,
      tema_link_preview: false,
      email_subscribe: '',
    }

  }

  handleChangeInput(e) {
    const {
      getTema
    } = this.props
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmitSearch(e) {
    const {
      getTema,
      temaAddFilterDataFilter,
      temaFilterData,
    } = this.props
    temaAddFilterDataFilter(this.state.tema_val)
      .then(() => {
        getTema(temaFilterData)
      })

  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      getTema
    } = this.props
    //Responsee tabs
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Custom forms
    $(function () {
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    });
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    //ini teh buat filter == search maka rubah lah jadi reducer sekarang

    getTema(this.props.temaFilterData)
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    this.closeNav()
    if (isLogged == false) {
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }



  }
  handlePreview(link = '') {
    this.setState({ tema_preview: !this.state.tema_preview })
    this.setState({ tema_link_preview: link })
  }
  closeNav() {
    $("body").removeClass("show-menu");
  }

  handleSubscribe(e) {
    this.setState({ email_subscribe: e.target.value })
  }
  handleSubmitSubscribe() {

    const {
      userSubscribe
    } = this.props

    userSubscribe(this.state.email_subscribe)

    this.setState({
      email_subscribe: ''
    })

  }

  //fungsi whatsapp button
  handleScriptCreate() {
    this.setState({ scriptLoaded: false })
  }
  
  handleScriptError() {
    this.setState({ scriptError: true })
  }
  
  handleScriptLoad() {
    this.setState({ scriptLoaded: true })
  }

  render() {
    const {
      user,
      tema,
      temaFilterData,
      temaPagination,
      getTema,
      temaAddFilterDataPage,
      temaAddFilterDataPerPage,
    } = this.props
    // console.log(temaPagination)
    const temaContentReducer = []

    tema.map((tems, index) =>
      temaContentReducer.push(
        <div className="wrapper-image col-lg-3 col-sm-6 mar-bot-40">
          <img key={tems.id} description={tems.description} style={{ width: '300px', height: '140px' }} className="img-tema-modal" src={tems.thumbnail} />
          <div className="label-tema">{tems.name}</div>
          <div className="img-tema-overlay">
            <a href="#" onClick={() => this.handlePreview(tems.link_preview)} className="icon-magnify">
              <i className="fa fa-search"></i>
            </a>
          </div>
        </div>)
    )
    const isLogged = user.get('logged_in')

    let temaPageContent;

    temaPageContent = temaContentReducer

    const navBarStyle = {
      marginLeft: 21,
    };

    return (
      <div className="tema">
        {/* <ModalLogin modal={modal} handleClose={this.handleLogin}/> */}
        {/* TOP NAV WITH LOGO */}
        <header>
          <nav className="nav-fixed">
            <div className="line">
              <div className="s-12 m-3 l-2 div-langgan-logo">
                <Link to={`/`}><img className="s-5 l-12 img-langgan-logo" src={LangganLogo} onClick={this.scrollToTop} alt="Logo Langgan Indonesia" title="Logo Langgan Indonesia" /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  <li><Link onClick={this.closeNav}  className="menu-tab navBarMargin" to={`/#second-block`}>Benefit</Link></li>
                  <li><Link onClick={this.closeNav}  className="menu-tab navBarMargin" to={`/tema-page`}>Tema</Link></li>
                  <li><Link onClick={this.closeNav}  className="menu-tab navBarMargin" to={`/#pilihpaket-block`}>Harga</Link></li>
                  <li><Link onClick={this.closeNav}  className="menu-tab navBarMargin" to={`/#fourth-block`}>Hubungi Kami</Link></li>
                  <li>
                    <a href="http://blog.langgan.id/" target="_blank"  className="menu-tab navBarMargin navBarBlogMargin">Blog</a>
                  </li>
                  {
                    isLogged == false
                      ?
                      <li className="vertical-align-center" style={{ height: 50 }}>
                        <div
                          onClick={this.handleLogin}
                          className="menu-tab buttonLoginStyle"
                        // style={{color: '#0B877E', border:'2px solid #0B877E', padding:5, paddingLeft:15, paddingRight:15, borderRadius:20}}

                        >
                          Masuk / Daftar
                              </div>
                      </li>
                      :
                      <Fragment>
                        <li><Link to={`/member-area`} className="menu-tab" style={{ color: '#0B877E', fontWeight: 'bold' }}>Member Area</Link></li>
                        <li><a onClick={this.handleLogin} className="menu-tab" style={{ color: '#0B877E', marginLeft: 21, fontWeight: 'bold' }}>Keluar</a></li>
                      </Fragment>
                  }
                </ul>
              </div>
            </div>
          </nav>
        </header>

        <div className="">
          <Script
            url="https://widget.tochat.be/bundle.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}
            attributes={{key:"1d3e8f55-968b-46eb-9aea-53301a6512db"}}
          />
          {/* <div className="text-right"><a target="_blank" href="https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..." className="btn btn-hubungi-wa" type="submit"><i class="fa fa-whatsapp"></i></a></div> */}
        </div>
        <section className="text-center">
          {/* FIRST BLOCK */}
          <div id="tema-header-block" className="text-center">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="s-12 m-12 l-7 center">
                    <h2 className="margin-bottom">Tema</h2>
                    <p className="text-center" style={{ fontSize: '20px' }}>Beragam Variasi Pilihan Yang Responsif Untuk Semua Perangkat
                        </p>
                    {/* <div><input type='text' name='tema_val' value={this.state.tema_val} onChange={this.handleChangeInput} placeholder='Ketik nama tema disini...'/><button className="btn-cari" onClick={this.handleSubmitSearch} type="submit">Cari</button></div> */}
                  </article>
                </div>
              </div>
            </div>
          </div>
          {/* SECOND BLOCK */}
          <div id="tema-block" className="text-left">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin ">
                  {temaPageContent}
                </div>
                {/* <Pagination
                    pagination={temaPagination}
                    addPageNumber={temaAddFilterDataPage}
                    addPerPageSize={temaAddFilterDataPerPage}
                    fetchData={getTema}
                    filterData={temaFilterData}
                    perPageSize={''} /> */}
              </div>
            </div>
          </div>

          <div id="id03" className={`modal ${this.state.tema_preview == true ? "show" : ''}`} >
            <div className="modal-content-preview animate" style={{ overflow: 'auto' }}>
              <div className="div-tema-content">
                <button className="button-close pull-right" onClick={this.handlePreview}></button>
                <article className="s-12 m-12 l-7 center">
                </article>
              </div>
              <div id="tema-preview" className="text-left">
                <div className="line-preview">
                  <iframe src={this.state.tema_link_preview} style={{ width: '100%' }} />
                </div>
              </div>
            </div>
          </div>

        </section>
        {/* FOOTER */}
        <footer className="footer-desktop">
          <div className="line vertical-align-center">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
              <div className="s-12 l-4 social-btn-div text-center p-t-20">
                <div className="m-b-20">
                  <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                    <i className="fa fa-twitter"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                    <i className="fa fa-linkedin"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                    <i className="fa fa-youtube"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="s-12 l-4 text-right subscribe">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
              <div className="m-b-10" style={{ marginTop: 10 }}>
                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                  <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', float: 'right', objectFit: 'contain' }} />
                </a> */}

                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                        <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', float:'right', objectFit:'contain', marginRight:10}}/>
                      </a> */}
              </div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
        </footer>

        <footer className="footer-mobile">
          <div className="line vertical-align-center">
            <div className="s-12 l-12">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-4 social-btn-div text-center p-t-20">
              <div className="m-b-20">
                <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                  <i className="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                  <i className="fa fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                  <i className="fa fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                  <i className="fa fa-linkedin"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                  <i className="fa fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>

          <div className="line vertical-align-center m-t-10">
            <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
              <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 text-left subscribe-mobile">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>

          <div className="line vertical-align-center">
            <div className="s-12 l-12 subscribe-mobile">
              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', objectFit: 'contain', float: 'right', marginRight: '20%' }} />
              </a> */}

              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                      <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', bjectFit:'contain', marginRight:10, float:'right'}}/>
                    </a> */}
            </div>
          </div>

          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-4 l-2 text-left">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-4 l-2 text-left">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
          </div>
        </footer>
      </div>
    );

  }

}


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
    tema: state.tema,
    temaFilterData: state.temaFilterData,
    temaPagination: state.temaPagination,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    getTema: (tema) => dispatch(getTema(tema)),
    temaAddFilterDataPage: (page) => dispatch(temaAddFilterDataPage(page)),
    temaAddFilterDataPerPage: (per_page) => dispatch(temaAddFilterDataPerPage(per_page)),
    temaAddFilterDataFilter: (filter) => dispatch(temaAddFilterDataFilter(filter)),
    userSubscribe: (email) => dispatch(userSubscribe(email)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TemaPage))
