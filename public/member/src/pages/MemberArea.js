/* eslint-disable */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'

import Logo from '../styles/img/logo.png'
import store from '../helpers/user_session'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import io from 'socket.io-client'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { push } from 'react-router-redux';
import { withRouter } from 'react-router'
import 'nprogress/nprogress.css'
import ModalReminderToko from './partials/ModalReminderToko'
import Ilus_tokodibangunLogo from './../styles/img/ilus_tokodibangun-06.png'
import BeatLoader from "react-spinners/BeatLoader";

//import action redux
import {
  tokoFetchData,
  shopOffline
} from '../actions/toko'
import {
  userLogout,
  userSubscribe,
} from '../actions/users'


import {
  paymentToko
} from '../actions/payment'


import styled from 'styled-components'

const Card = styled.div`
 
  display:flex;
  box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);
  background-color:#fff;
  padding: 5%;

  //mobile 
  width: 90% ;
    h1 {
      font-size: 1.5rem ;
    }
    p#about {
      font-size: 1.2rem;
    }
    .buttonStyle button{
      // margin-left: 29%;
      // padding: 2px 5px;
      // height: 30px;
    }
    margin-top:15% ;
  
  //ipad 
  @media only screen and (min-width: 768px){
    h1 {
      font-size: 2.2rem;
    }
    p#about {
      font-size: 1.8rem;
    }
    margin-top: 17%;
  }
  //desktop
  @media only screen and (min-width: 1024px){

    width: 50%;
    h1{
      font-size: 2.7rem;
    }
    p#about {
      font-size: 2rem;
    }
    margin-top: 0;
  }
`

const ContentTop = styled.div`
  @media only screen and (max-width: 768px){
    margin-top: 25%;
  }
  @media only screen and (max-width: 1024px){
    margin-top: 10%;
  }
`

class MemberArea extends Component {

  constructor(props) {
    super(props)


    bindAll(this, [
      'handleOffline',
      'handleUpdate',
      'handleSubscribe',
      'handleSubmitSubscribe',

    ])
    this.state = {
      modal: false,
      email: ''
    }
  }
  handleSubscribe(e) {
    this.setState({ email_subscribe: e.target.value })
  }
  handleSubmitSubscribe() {

    const {
      userSubscribe
    } = this.props
    userSubscribe(this.state.email_subscribe)
    this.setState({
      email_subscribe: ''
    })
  }

  lakukan_pembayaran(data) {
    let arrayData = {}
    arrayData['id_user'] = this.props.userInformation.get('id')
    arrayData['id_paket'] = data.get("id_paket");
    arrayData['domain'] = data.get("domain");
    arrayData['snap_token'] = data.get("snap_token");
    arrayData['nominal'] = data.get("nominal");
    arrayData['nama_toko'] = data.get("name");
    arrayData['id_tema'] = data.get("id_tema");
    this.props.paymentToko({ arrayData, dataKupon: 0, statusPembayaran:'Bayar'})
  }

  ubah_pembayaran(data) {
    let id_paket = null;
    if(data.get("nama_paket") === "Starter"){
      id_paket = 1;
    } else if(data.get("nama_paket") === "Premium"){
      id_paket = 2;
    } else {
      id_paket = 3;
    }

    let arrayData = {}
    arrayData['id_user'] = this.props.userInformation.get('id')
    arrayData['id_paket'] = id_paket;
    arrayData['id_billing'] = data.get("id_billing");
    arrayData['domain'] = data.get("domain");
    arrayData['snap_token'] = data.get("snap_token");
    arrayData['nominal'] = data.get("nominal");
    arrayData['nama_toko'] = data.get("nama_toko");
    arrayData['id_tema'] = data.get("id_tema");
    arrayData['code_kupon'] = data.get("kupon");
    this.props.paymentToko({ arrayData, dataKupon: 0, statusPembayaran:'Ubah' })
  }

  componentDidMount() {
    const {
      tokoFilterData,
      tokoFetchData,
      user
    } = this.props

    tokoFetchData(tokoFilterData)
    // io('https://notif.langgan.id').on(`channel:member-56`, (message) => {
    //   console.log(message)
    // })
    // console.log(this.props.tokos)
  }
  
  handleOffline(id) {
    const {
      shopOffline,
      tokoFilterData
    } = this.props
    shopOffline(id, tokoFilterData)
  }

  handleUpdate(id) {
    const { redirectUpdatePaket } = this.props
    let url = 'update-paket/' + id;

    
    redirectUpdatePaket(url)
  }

  renderGetData(value) {
    console.log(value)
  }

  renderCardView(isMobile){
    const { tokos } = this.props
    return(
      <>
      {tokos.map((v, i) => (
        <div key={i} className="">
          {
            (v.get('status_flag') === "Menunggu Toko Aktif" || v.get('status_flag') === "Menunggu Domain Aktif") && v.get('status_flag') !== "Toko Aktif" ? 
                <div className="alert alert-primary" role="alert" style={ isMobile ? {marginTop: "5%"} : {position: v.get('status_flag') === "Menunggu Toko Aktif" ? "":"absolute", marginTop: v.get('status_flag') === "Menunggu Toko Aktif" ? "":"-100%", left: v.get('status_flag') === "Menunggu Toko Aktif" ? "":"-5%"} }>
                  {v.get('status_flag') === "Menunggu Toko Aktif" ? 
                    "Domain kamu masih dalam proses pendaftaran, mohon tunggu maksimal 1x24 jam sampai dengan di aktifkan"
                    :
                    "Domain kamu masih dalam proses pendaftaran, mohon tunggu maksimal 1x24 jam sampai dengan di aktifkan. sambil menunggu, kami membuatkan subdomain terlebih dahulu supaya kamu bisa mengatur toko lebih awal."
                  }
                </div>
            :
            ""
          }
          {
            v.get('status_pembayaran') === 3 && v.get('status_flag') === "Menunggu Toko Aktif" &&
            <div className="alert alert-secondary" role="alert">
              <big>Sistem kami sedang membuatkan toko kamu. Mohon menunggu maksimal 1jam dari sekarang, apabila toko berhasil dibuat pesan ini akan hilang.</big>
              <br/><br/>
              <small>*Lakukan refresh secara berkala pada halaman ini untuk mengetahui status toko</small>
            </div>
          }
          {
            v.get('status_pembayaran') === 3 && v.get('status_flag') === "Request Perubahan Domain" &&
            <div className="alert alert-secondary" role="alert">
              <big>Domain kamu sudah digunakan namun tidak usah khawatir, kamu bisa request domain baru kepada kami untuk dibuatkan, silahkan hubungi kami untuk request domain baru dengan cara klik  
              <a style={{ color: '#337ab7', fontSize: '1.8rem', marginLeft: '1%' }} className={"text-left center-when-mobile"} href="https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..." target="_blank">
               disini.
              </a>
              </big>
            </div>
          }
        </div>
      ))}
      </>
    )
  }

  render() {
    const { tokos } = this.props

    const linkWa = "https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..."

    return (
      <div className="home member-area">
        <Navbar handleProfile={this.handleProfile} />
        <ModalReminderToko />
        <section className="text-center force-bottom-bg">
          {
            this.props.loaderFetchDataToko
              ?
                <MediaQuery minDeviceWidth={800}>
                  {(matches) =>
                    matches
                      ? 
                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'20%'}}>
                          <BeatLoader
                            size={20}
                            color={"#1c877f"}
                            loading={true}
                          />
                          <p id="about">
                            Tunggu Beberapa Saat...   
                          </p>
                        </article>
                      :
                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'50%'}}>
                          <BeatLoader
                            size={20}
                            color={"#1c877f"}
                            loading={true}
                          />
                          <p id="about">
                            Tunggu Beberapa Saat...   
                          </p>
                        </article>
                  }
                </MediaQuery>
              :
                <div className="container-fluid">
                  {/* <!-- FIRST BLOCK --> */}
                  <div className="text-center" >
                    <div className="line">
                      <div className="margin-bottom">
                        <div className="" >
                        {
                          tokos.size == 0 
                            ?
                              <article style={{display:'flex', justifyContent:'center', flexDirection:'row', paddingTop:'10%'}}>
                                <Card style={{marginBottom:'-18%', display:'flex', justifyContent:'center'}}>
                                  <div className="notif-area s-12 m-12 l-12 " >
                                    <h1 id="title">Oops! </h1>
                                    <img src={Ilus_tokodibangunLogo} style={{ maxWidth:'50%', display:'inline'}}></img>
                                    <p id="about">
                                      Kamu belum buat toko sebelumnya, ayo buat tokomu dengan sesuka hati, niat, kemauan dan mood :D                
                                    </p>
                                    <Link to={`/member-paket`} className="menu-tab">
                                      <div className="s-5 m-3 l-3 center buttonStyle">
                                        <div style={{ paddingTop: '5%'}}>
                                        <button className={'btn btn-primary btn-block btn-md'} type="submit" style={{ backgroundColor: '#15BFAE' }}>
                                          Buat Toko
                                        </button>
                                        </div>
                                      </div>
                                    </Link>
                                  </div>
                                </Card>
                              </article>
                            :
                              null
                        }

                        {
                          
                          tokos.map((v, i) => (  
                            <div key={i} className="">
                              {
                                v.get('status_pembayaran') === 2
                                  ?
                                    v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true
                                      ?
                                        <MediaQuery minDeviceWidth={800}>
                                          {(matches) =>
                                            matches
                                              ? 
                                                <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                  <h1 className="margin-bottoms">Toko Saya</h1>
                                                  <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                </article> 
                                              :
                                                <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                  <h1 className="margin-bottoms">Toko Saya</h1>
                                                  <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                </article> 
                                          }
                                        </MediaQuery>
                                      :
                                        <MediaQuery minDeviceWidth={800}>
                                          {(matches) =>
                                            matches
                                              ? 
                                                <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                  <h1 className="margin-bottoms">Toko Saya</h1>
                                                  <p className="text-center f-20">Silahkan lakukan pembayaran sebelum 1x24jam. Atau kamu bisa mengubah metode pembayaran dengan channel lain</p>
                                                </article>
                                              :
                                                <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                  <h1 className="margin-bottoms">Toko Saya</h1>
                                                  <p className="text-center f-20">Silahkan lakukan pembayaran sebelum 1x24jam. Atau kamu bisa mengubah metode pembayaran dengan channel lain</p>
                                                </article>   
                                          }
                                        </MediaQuery>
                                  :
                                    v.get('status_pembayaran') === 3
                                      ?
                                        v.get('status_flag') === "Domain sudah dipakai"
                                          ?
                                            <article style={{display:'flex', justifyContent:'center', flexDirection:'row', paddingTop:'10%'}}>
                                              <Card style={{marginBottom:'-18%'}}>
                                                <div className="notif-area s-12 m-12 l-12 " >
                                                  <h1 id="title">Selamat toko anda sudah berhasil dibuat</h1>
                                                  <img src={Ilus_tokodibangunLogo} style={{ maxWidth:'50%', display:'inline'}}/>
                                                  <p id="about">
                                                    Tetapi Domain sudah ada yang menggunakan setelah anda melakukan pembayaran. Silahkan <a href={linkWa} target="_blank" style={{color:'#3c8dbc'}}>hubungi kami</a> untuk mengganti domain anda.
                                                  </p>
                                                </div>
                                              </Card> 
                                            </article>
                                          :
                                            v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true  
                                              ?
                                                <MediaQuery minDeviceWidth={800}>
                                                  {(matches) =>
                                                    matches
                                                      ? 
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                        </article> 
                                                      :
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                        </article> 
                                                  }
                                                </MediaQuery>
                                              :
                                                <MediaQuery minDeviceWidth={800}>
                                                  {(matches) =>
                                                    matches
                                                      ? 
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Berikut adalah informasi toko kamu. {v.get('status_flag') === "Toko Aktif" ? 'Tekan tombol "Menuju Bisnis Kit" untuk mulai mengatur toko kamu.' : "Silahkan baca informasi di kanan bawah terlebih dahulu."}</p>
                                                        </article>      
                                                      :
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Berikut adalah informasi toko kamu. {v.get('status_flag') === "Toko Aktif" ? 'Tekan tombol "Menuju Bisnis Kit" untuk mulai mengatur toko kamu.' : "Silahkan baca informasi di kanan bawah terlebih dahulu."}</p>
                                                        </article>      
                                                  }
                                                </MediaQuery>
                                      :
                                        v.get('status_pembayaran') === 5 && v.get('status_flag') === "Toko Aktif" ||  v.get('status_pembayaran') === 4 && v.get('status_flag') === "Toko Aktif"
                                          ?
                                            <MediaQuery minDeviceWidth={800}>
                                              {(matches) =>
                                                matches
                                                  ? 
                                                    <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                      <h1 className="margin-bottoms">Toko Saya</h1>
                                                      <p className="text-center f-20">Berikut adalah informasi toko kamu. {v.get('status_flag') === "Toko Aktif" ? 'Tekan tombol "Menuju Bisnis Kit" untuk mulai mengatur toko kamu.' : "Silahkan baca informasi di kanan bawah terlebih dahulu."}</p>
                                                    </article> 
                                                  :
                                                    <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                      <h1 className="margin-bottoms">Toko Saya</h1>
                                                      <p className="text-center f-20">Berikut adalah informasi toko kamu. {v.get('status_flag') === "Toko Aktif" ? 'Tekan tombol "Menuju Bisnis Kit" untuk mulai mengatur toko kamu.' : "Silahkan baca informasi di kanan bawah terlebih dahulu."}</p>
                                                    </article> 
                                              }
                                            </MediaQuery>
                                          :
                                            v.get('status_pembayaran') === 4 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||
                                            v.get('status_pembayaran') === 5 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true 
                                              ?
                                                <MediaQuery minDeviceWidth={800}>
                                                  {(matches) =>
                                                    matches
                                                      ? 
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'10%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                        </article> 
                                                      :
                                                        <article style={{display:'flex', justifyContent:'center', flexDirection:'column', paddingTop:'30%'}}>
                                                          <h1 className="margin-bottoms">Toko Saya</h1>
                                                          <p className="text-center f-20">Masa berlangganan kamu sudah habis, Silakan perpanjang kembali berlanggananmu untuk bisa mengakses tokomu kembali</p>
                                                        </article> 
                                                  }
                                                </MediaQuery>
                                              :
                                                <article style={{display:'flex', justifyContent:'center', flexDirection:'row', paddingTop:'10%'}}>
                                                  <Card style={{marginBottom:'-18%'}}>
                                                    <div className="notif-area s-12 m-12 l-12 " >
                                                      <h1 id="title">Oops, toko tidak berhasil dibuat</h1>
                                                      <img src={Ilus_tokodibangunLogo} style={{ maxWidth:'50%', display:'inline'}}/>
                                                      <p id="about">
                                                        {
                                                          v.get('status_flag') === "Domain sudah dipakai" 
                                                            ?
                                                              "Domain sudah ada yang menggunakan sebelum melakukan pembayaran. Silahkan buat ulang toko."
                                                            :
                                                              v.get('status_flag') === "Transaski gagal" 
                                                                ?
                                                                  "Transaksi kamu gagal, namu jangan khawatir. kamu masih bisa membuat toko dari awal."
                                                                :
                                                                  "Kamu tidak melakukan pembayaran dalam 1x24 jam, namun jangan khawatir. Kamu masih bisa membuat toko dari awal."
                                                        }
                                                      </p>
                                                      <Link to={`/member-paket`} className="menu-tab">
                                                        <div className="s-5 m-3 l-3 center buttonStyle">
                                                          <div style={{ paddingTop: '5%'}}>
                                                          <button className={'btn btn-primary btn-block btn-md'} type="submit" style={{ backgroundColor: '#15BFAE' }}>
                                                            Buat Toko
                                                          </button>
                                                          </div>
                                                        </div>
                                                      </Link>
                                                    </div>
                                                  </Card> 
                                                </article>
                              }
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* <!-- SECOND BLOCK --> */}
                  <div className="text-left" style={{paddingTop:'18%', paddingBottom:'5%'}}>
                    <div className="line" style={{ padding: '0px' }}>
                      <div className="floating-alert">
                      {screen.width >= 1024  ? this.renderCardView(false) : null}
                      </div>
                      {
                        tokos.map((v, i) => (
                          <div key={i} className="">
                            {
                              v.get('status_pembayaran') === 2 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||  
                              v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||  
                              v.get('status_pembayaran') === 4 && v.get('status_flag') !== "Toko Aktif" || 
                              v.get('status_pembayaran') === 5 && v.get('status_flag') !== "Toko Aktif" || 
                              v.get('status_pembayaran') === 3 && v.get('status_flag') === "Domain sudah dipakai" || 
                              v.get('status_pembayaran') === 0
                                ?
                                  v.get('status_pembayaran') === 2 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||  
                                  v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||  
                                  v.get('status_pembayaran') === 4 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true ||
                                  v.get('status_pembayaran') === 5 && v.get('status_flag') === "Toko expired" && v.get('berlangganan') === true 
                                    ?
                                      <>
                                        <div className="margin-bottom">
                                          <div className="s-12 m-12 l-2 left vertical-align-center" style={{ height: '100px' }}>
                                            <img style={{ height: '100%', objectFit: 'cover' }} className="margin-auto-when-mobile" src={Logo} alt="" />
                                          </div>
        
                                          <div className="s-12 m-12 l-2 left">
                                            <h2 className="bold text-center-when-mobile" style={{ marginBottom: 0 }}>{v.get('nama_toko')}</h2>
                                            <a style={{ color: '#337ab7' }} className={"text-left center-when-mobile"} href={`http://${v.get('domain')}`} target="_blank">
                                              <p style={{ fontSize: '1.8rem' }}>{v.get('domain')}</p>
                                            </a>
                                            <h3 className="text-left center-when-mobile">
                                              {v.get('nama_paket') === 'Starter' && <>Skema 1 Bulan</>}
                                              {v.get('nama_paket') === 'Premium' && <>Skema 6 Bulan</>}
                                              {v.get('nama_paket') === 'Enterprise' && <>Skema 12 Bulan</>}
                                            </h3>
                                          </div>
        
                                          <div className="s-12 m-12 l-2 left">
                                            <h3 className="text-left center-when-mobile">Tema Kamu :</h3>
                                            <h3 className="text-left center-when-mobile">{v.get('nama_tema')}</h3>
                                          </div>
                                          
                                          <div className="s-12 m-12 l-3 left">
                                            <h3 className="text-left center-when-mobile">Berlaku s/d {v.get('expired_at')}</h3>
                                            <h3 className="text-left center-when-mobile">Status Toko Tidak Aktif</h3>
                                            <h3 className="text-left center-when-mobile">Masa Berlangganan Kamu Habis</h3>
                                          </div>
        
                                          <div className="s-12 m-12 l-2 left">
                                            <div>
                                            {screen.width < 1024  ? this.renderCardView(true): null}
                                            </div>
        
                                            <div style={{ marginBottom: '10px' }}>
                                              {
                                                v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko expired" || 
                                                v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko expired" || 
                                                v.get('status_pembayaran') === 4 && v.get('status_flag') === "Toko expired" || 
                                                v.get('status_pembayaran') === 5 && v.get('status_flag') === "Toko expired"  
                                                  ?
                                                    <button className={'btn-jadikan btn-offline'} onClick={(e) => { this.handleUpdate(v.get('id')) }} type="submit" style={{ fontSize: 14, color: '#15BFAE', borderColor: '#15BFAE' }}>
                                                      Perpanjang Berlangganan
                                                    </button>
                                                  :
                                                    null
                                              }
        
                                              {
                                                v.get('status_pembayaran') <= 2 && v.get('status_pembayaran') != null &&
                                                <button className={'btn btn-primary btn-block btn-md'} onClick={(e) => this.lakukan_pembayaran(v)} type="submit" style={{ backgroundColor: '#15BFAE', fontSize:v.get('status_flag') === "Toko expired" ?  12 : 14  }}>
                                                  Lakukan Pembayaran {v.get('status_flag') === "Toko expired" ?  "\n (Upgrade)" : ""}
                                                </button>
                                              }
                                            </div>
                                            <div style={{ marginBottom: '10px' }}>
                                            {
                                              v.get('status_pembayaran') <= 2
                                                ?
                                                  <button className={'btn btn-primary-outline btn-block btn-md'} onClick={(e) => this.ubah_pembayaran(v)} type="button" style={{fontSize:v.get('status_flag') === "Toko expired" ?  12 : 14 }}>
                                                    Ubah pembayaran {v.get('status_flag') === "Toko expired" ?  "\n (Upgrade)" : ""}
                                                  </button>
                                                :
                                                  null
                                            }
                                            </div>
                                            <div>
                                              <a 
                                                className={`btn btn-primary disabled btn-block btn-md ${(v.get('status_pembayaran') === 3 && (v.get('status_flag') === "Toko expired" || v.get('status_flag') === "Menunggu Domain Aktif")) || (v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko Aktif") ? "" : ""}`} 
                                                style={{ paddingTop: '5px', backgroundColor: '#15BFAE' }} 
                                                target={'_blank'} 
                                                href={'#'} 
                                              >
                                                Menuju Bisnis Kit
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </>
                                    :
                                      null
                                :
                              <>
                                <div className="margin-bottom">
                                  <div className="s-12 m-12 l-2 left vertical-align-center" style={{ height: '100px' }}>
                                    <img style={{ height: '100%', objectFit: 'cover' }} className="margin-auto-when-mobile" src={Logo} alt="" />
                                  </div>

                                  <div className="s-12 m-12 l-2 left">
                                    <h2 className="bold text-center-when-mobile" style={{ marginBottom: 0 }}>{v.get('nama_toko')}</h2>
                                    <a style={{ color: '#337ab7' }} className={"text-left center-when-mobile"} href={`http://${v.get('domain')}`} target="_blank">
                                      <p style={{ fontSize: '1.8rem' }}>{v.get('domain')}</p>
                                    </a>
                                    <h3 className="text-left center-when-mobile">
                                      {v.get('nama_paket') === 'Starter' && <>Skema 1 Bulan</>}
                                      {v.get('nama_paket') === 'Premium' && <>Skema 6 Bulan</>}
                                      {v.get('nama_paket') === 'Enterprise' && <>Skema 12 Bulan</>}
                                    </h3>
                                  </div>

                                  <div className="s-12 m-12 l-2 left">
                                    <h3 className="text-left center-when-mobile">Tema Kamu :</h3>
                                    <h3 className="text-left center-when-mobile">{v.get('nama_tema')}</h3>
                                  </div>
                                  
                                  <div className="s-12 m-12 l-3 left">
                                    <h3 className="text-left center-when-mobile">Berlaku s/d {v.get('expired_at')}</h3>
                                    <h3 className="text-left center-when-mobile">Status {v.get('status_flag')}</h3>
                                  </div>

                                  <div className="s-12 m-12 l-2 left">
                                    <div>
                                    {screen.width < 1024  ? this.renderCardView(true): null}
                                    </div>

                                    <div style={{ marginBottom: '10px' }}>
                                      {
                                        v.get('status_pembayaran') === 3 && v.get('status_flag') === "Menunggu Domain Aktif" || 
                                        v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko Aktif" || 
                                        v.get('status_pembayaran') === 4 && v.get('status_flag') === "Toko Aktif" || 
                                        v.get('status_pembayaran') === 5 && v.get('status_flag') === "Toko Aktif"  
                                          ?
                                            <button className={'btn-jadikan btn-offline'} onClick={(e) => { this.handleUpdate(v.get('id')) }} type="submit" style={{ fontSize: 14, color: '#15BFAE', borderColor: '#15BFAE' }}>
                                              Upgrade Berlangganan
                                            </button>
                                          :
                                            null
                                      }

                                      {
                                        v.get('status_pembayaran') <= 2 && v.get('status_pembayaran') != null &&
                                        <button className={'btn btn-primary btn-block btn-md'} onClick={(e) => this.lakukan_pembayaran(v)} type="submit" style={{ backgroundColor: '#15BFAE', fontSize:v.get('status_flag') === "Toko Aktif" ?  12 : 14 }}>
                                          Lakukan Pembayaran {v.get('status_flag') === "Toko Aktif" ?  "\n (Upgrade)" : ""}
                                        </button>
                                      }
                                    </div>
                                    <div style={{ marginBottom: '10px' }}>
                                    {
                                      v.get('status_pembayaran') <= 2
                                        ?
                                          <button className={'btn btn-primary-outline btn-block btn-md'} onClick={(e) => this.ubah_pembayaran(v)} type="button" style={{fontSize:v.get('status_flag') === "Toko Aktif" ?  12 : 14 }}>
                                            Ubah pembayaran {v.get('status_flag') === "Toko Aktif" ?  "\n (Upgrade)" : ""}
                                          </button>
                                        :
                                          null
                                    }
                                    </div>
                                    <div>{
                                      v.get('status_flag') === "Menunggu Domain Aktif" || v.get('status_flag') === "Toko Aktif"
                                        ?
                                          <a 
                                            className={`btn btn-primary btn-block btn-md ${(v.get('status_pembayaran') === 3 && (v.get('status_flag') === "Toko Aktif" || v.get('status_flag') === "Menunggu Domain Aktif")) || (v.get('status_pembayaran') === 3 && v.get('status_flag') === "Toko Aktif") ? "" : ""}`} 
                                            style={{ paddingTop: '5px', backgroundColor: '#15BFAE' }} 
                                            target={'_blank'} 
                                            href={`http://` + (v.get('status_flag') === "Menunggu Domain Aktif" ? v.get('subdomain_dashboard_langgan') : "bisniskit."+v.get('domain')) + `/login?token=` + store.getTokenDashboard()} 
                                          >
                                            Menuju Bisnis Kit
                                          </a>
                                        :
                                          null
                                      }
                                      </div>
                                    </div>
                                  </div>
                              </>
                              }
                          </div>
                        )
                      )
                    }
                    </div>
                  </div>
                </div>
          }
        </section >

        {/* FOOTER */}
        < footer className="footer-desktop" style={{
          background: '#2b3332',
          padding: '20px',
          color: 'white',
          height: '32%',
          width: '100%',
        }
        }>
          <div className="line vertical-align-center">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-12 l-4 social-btn-div text-center p-t-20">
                <div className="m-b-20">
                  <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                    <i className="fa fa-twitter"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                    <i className="fa fa-linkedin"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                    <i className="fa fa-youtube"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="s-12 l-4 text-right subscribe">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
              <div className="m-b-10" style={{ marginTop: 10 }}>
                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                  <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', float: 'right', objectFit: 'contain' }} />
                </a> */}
              </div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
        </footer >
        <footer className="footer-mobile">
          <div className="line vertical-align-center">
            <div className="s-12 l-12">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-4 social-btn-div text-center p-t-20">
              <div className="m-b-20">
                <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                  <i className="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                  <i className="fa fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                  <i className="fa fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                  <i className="fa fa-linkedin"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                  <i className="fa fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
          <div className="line vertical-align-center m-t-10">
            <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
              <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 text-left subscribe-mobile">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 subscribe-mobile">
              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', objectFit: 'contain', float: 'right', marginRight: '20%' }} />
              </a> */}
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-4 l-2 text-left">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-4 l-2 text-left">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
          </div>
        </footer>
      </div >
    )
  }
}


const mapStateToProps = (state, ownProps) => {
  return {
    tokoFilterData: state.tokoFilterData,
    userInformation: state.userInformation,
    tokos: state.tokos,
    user: state.user,
    loaderFetchDataToko: state.loaderFetchDataToko,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    tokoFetchData: (filter) => dispatch(tokoFetchData(filter)),
    userSubscribe: (email) => dispatch(userSubscribe(email)),
    shopOffline: (id, filter) => dispatch(shopOffline(id, filter)),
    redirectUpdatePaket: (url) => dispatch(push(url)),
    paymentToko: (data) => dispatch(paymentToko(data))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MemberArea))


{/* <div><button className={v.get('status_pembayaran') != '3' ? 'btn-offline btn-jadikan disabled' : 'btn-jadikan btn-offline'} disabled={v.get('status_pembayaran') != '3' ? 'ture' : 'false'} onClick={(e)=>{this.handleOffline(v.get('id'))}} type="submit">{v.get('status_flag') == 'Live' ? 'Jadikan Offline' : 'Jadikan Online'}</button></div> */ }


{/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                      <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', bjectFit:'contain', marginRight:10, float:'right'}}/>
                    </a> */}

{/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                        <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', float:'right', objectFit:'contain', marginRight:10}}/>
                      </a> */}