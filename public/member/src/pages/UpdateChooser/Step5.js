/* eslint-disable */
import React, { Component } from 'react'

import Carousel from '../components/Carousel'

function formatRupiah(angka, prefix) {
	// console.log(angka)
	var number_string = angka.toString().replace(/[^,\d]/g, ''),
		split = number_string.split(','),
		sisa = split[0].length % 3,
		rupiah = split[0].substr(0, sisa),
		ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if (ribuan) {
		let separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

export default class Step5 extends Component {
	render() {
		const {
			price,
			currentTheme,
			currentToko,
			currentDomain,
			currentPaket,
			currentKupon,
		} = this.props
		var check_domain = currentDomain.split('.')
		var is_premium_domain = false
		var convertedprice = price.replace('.', '')
		var final_price = parseInt(convertedprice.replace('.', ''))
		console.log(final_price)
		if (check_domain[1] == 'id') {
			if (check_domain[0].length == 4) {
				final_price = parseInt(price.replace('.', '')) + 2250000
				is_premium_domain = true
			}
			if (check_domain[0].length == 3) {
				final_price = parseInt(price.replace('.', '')) + 15000000
				is_premium_domain = true
			}
			if (check_domain[0].length == 2) {
				final_price = parseInt(price.replace('.', '')) + 500000000
				is_premium_domain = true
			}

		} else {
			if (check_domain[0].length == 2) {
				final_price = parseInt(price.replace('.', '')) + 17000000
				is_premium_domain = true
			}
		}
		// const total_biaya = final_price * (10 / 100) + final_price - currentKupon
		var ppn = (final_price - currentKupon) * 10 / 100


		const total_biaya = ppn + final_price - currentKupon
		console.log(ppn)
		console.log(final_price)
		return (
			<>
				<div className="langgan-flex" style={{ display: "flex", padding: "30px 20px", justifyContent: "space-around", }}>
					<div className="langgan-col" style={{ width: "60%" }}>
						<div style={{ 'border-right': "0px", 'text-align': "center", fontWeight: "600" }}>{currentToko} - {currentDomain}</div>
						<div className="langgan-flex" style={{ display: "flex", justifyContent: "space-around", marginTop: "20px" }}>
							<div className="langgan-col" style={{ width: "45%" }}>
								<div style={{ background: `${currentPaket.color}`, height: "250px", 'padding': "15px", 'border-radius': "5px", boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }} >
									<p style={{ 'font-size': "16px", color: "#fff", 'text-align': "center" }}>
										{
											currentPaket.name === 'Starter'
												?
												'Skema 1 Bulan'
												:
												currentPaket.name === 'Premium'
													?
													'Skema 6 Bulan'
													:
													'Skema 12 Bulan'
										}
									</p>
									<hr />
									<div className="choosen text-left" dangerouslySetInnerHTML={{ __html: currentPaket.descLil }} /></div>
							</div>
							<div className="langgan-col" style={{ width: "45%" }}>
								<div style={{ backgroundImage: `url(${currentTheme.thumbnail})`, height: "250px", 'padding-top': "10px", 'padding-left': '5px', boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)', backgroundSize: "cover" }} >
								</div>
							</div>
						</div>
					</div>
					<div className="langgan-col" style={{ width: "30%" }}>
						<div style={{ 'text-align': "center", fontSize: "16px", 'border-right': "0px", borderBottom: "2px solid #eee", margin: "20px 0" }}>Summary</div>
						<div style={{ display: "flex", fontSize: "16px", justifyContent: "space-between" }}>
							<span style={{ width: "50%", textAlign: "left" }}>Paket {currentPaket.name}</span>
							<span style={{ width: "50%", textAlign: "right" }}>
								{formatRupiah(final_price, 'Rp. ')} {is_premium_domain == true ? '(Premium Domain)' : ''}
							</span>
						</div>
						<div style={{ display: "flex", fontSize: "16px", justifyContent: "space-between", borderBottom: "2px solid #eee", paddingBottom: "25px" }}>
							<span style={{ width: "50%", textAlign: "left" }}>Voucher</span>
							<span style={{ width: "50%", textAlign: "right", color: `${currentPaket.color}` }}>
								{"- " + formatRupiah(currentKupon, 'Rp. ')}
							</span>

						</div>
						<div style={{ display: "flex", fontSize: "16px", justifyContent: "space-between" }}>
							<span style={{ width: "50%", textAlign: "left" }}>Subtotal</span>
							<span style={{ width: "50%", textAlign: "right" }}>
								{formatRupiah((final_price - currentKupon), 'Rp. ')}
							</span>
						</div>
						<div style={{ display: "flex", fontSize: "16px", justifyContent: "space-between" }}>
							<span style={{ width: "50%", textAlign: "left" }}>PPN (10%)</span>
							<span style={{ width: "50%", textAlign: "right" }}>
								{"+ " + formatRupiah((final_price - currentKupon) * (10 / 100), 'Rp. ')}
							</span>
						</div>

						<div style={{ display: "flex", fontSize: "16px", justifyContent: "space-between" }}>
							<span style={{ width: "50%", textAlign: "left" }}>Total</span>
							<span style={{ width: "50%", textAlign: "right", fontWeight: "600" }}>
								{formatRupiah(total_biaya, 'Rp. ')}
							</span>
						</div>
					</div>
				</div>
			</>
		)
	}
}
