/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'

import PaketCard from '../components/PaketCard'

const paket = [
  {
    id: 1,
    name: 'Starter',
    color: '#508A88',
    desc: `<ul class="list-unstyled ul-with-space">
      <li>Gratis Domain</li>
      <li>1 Bulan Berlangganan</li>
      <li>Pasang Domainmu Sendiri</li>
      <li>Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    descLil: `<ul class="list-unstyled ul-with-space">
      <li style="font-size:12px">Gratis Domain</li>
      <li style="font-size:12px">1 Bulan Berlangganan</li>
      <li style="font-size:12px">Pasang Domainmu Sendiri</li>
      <li style="font-size:12px">Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    pricing: "300.000"
  }, {
    id: 2,
    name: 'Premium',
    color: '#518a50',
    desc: `<ul class="list-unstyled ul-with-space">
      <li>6 Bulan Berlangganan</li>
      <li>Gratis Domain</li>
      <li>Pasang Domainmu Sendiri</li>
      <li>Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    descLil: `<ul class="list-unstyled ul-with-space">
      <li style="font-size:12px">6 Bulan Berlangganan</li>
      <li style="font-size:12px">Gratis Domain</li>
      <li style="font-size:12px">Pasang Domainmu Sendiri</li>
      <li style="font-size:12px">Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    pricing: "1.500.000"
  }, {
    id: 3,
    name: 'Enterprise',
    color: '#50528a',
    desc: ` <ul class="list-unstyled ul-with-space">
      <li>1 Tahun Berlangganan</li>
      <li>Gratis Domain</li>
      <li>Pasang Domainmu Sendiri</li>
      <li>Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    descLil: ` <ul class="list-unstyled ul-with-space">
      <li style="font-size:12px">1 Tahun Berlangganan</li>
      <li style="font-size:12px">Gratis Domain</li>
      <li style="font-size:12px">Pasang Domainmu Sendiri</li>
      <li style="font-size:12px">Seluruh Fitur Toko Online Langgan</li>
    </ul>`,
    pricing: "2.500.000"
  },
]


export default class Step1 extends Component {
  render() {
    return (
      <div>
        <PaketWrapper>
          {paket.map((v, i) => (
            <React.Fragment key={i.toString()}>
              <PaketCard
                active={this.props.currentPaket === null || this.props.currentPaket.id !== v.id ? false : true}
                paket={v.name}
                desc={v.desc}
                price={v.pricing}
                color={v.color}
                onClick={() => this.props.selectPaket(v)} />
            </React.Fragment>
          ))}
        </PaketWrapper>
      </div>
    )
  }
}

const PaketWrapper = styled.div`
  @media only screen and (min-width: 768px){
    display:flex;
    flex-direction:row;
  }

  width:100%;
  padding:10px;
`