/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'
import qs from 'qs'

import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  getDetailInvoice,
} from '../actions/payment'

class Invoice extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
    ])

    this.state = {
      invoice: {
        orderDetail: {}
      }
    }

  }

  componentDidMount() {
    var url_string = window.location.href

    var url = new URL(url_string);
    var order_id = url.searchParams.get("order_id");
    var status_code = url.searchParams.get("status_code");
    var transaction_status = url.searchParams.get("transaction_status");
    var __this = this
    const { getDetailInvoice } = this.props
    const arr = this.props.kode_invoice.split("-")
    const data = arr[arr.length - 1]
    getDetailInvoice(data).then((e) => {
      this.setState({ invoice: e })
    })
  }





  render() {

    var bilangan = this.state.invoice.nominal;
    var number_string = String(bilangan);
    var sisa = number_string.length % 3;
    var rupiah = number_string.substr(0, sisa);
    var ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
      var separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    return (
      <div className="force-bottom-bg">
        <div style={{ paddingBottom: "1%" }} className="pr-16 pt-24 fs-18 mb-16 bold">
          <div className="line" style ={{paddingLeft: 0, marginLeft: '16%'}}>
            <div className="margin-bottom">
              <div className="margin">
                <div className="s-12 m-12 l-3 bold left">
                  <img style={{ width: '300px' }} src={LangganLogo} />
                </div>
                {/* <div className="s-12 m-12 l-3 right">
                  <h1 className="margin-bottom bold text-center-when-mobile">Faktur</h1>
                </div> */}
              </div>
            </div>
          </div>
        </div>
        <div className="text-left">
          <div className="line" style={{maxWidth: '70%'}}>
            <div className="margin-bottom">
              <div className="margin">
                <h3 className="title-fraktur bold">
                  Selesaikan pembayaran Anda sebelum {this.state.invoice.expired}
                </h3>
                <div className="row">
                  <div className="col-lg-3 col-sm-6">
                    <h2 className="bold" style={{fontSize: '16px'}}>Tanggal Checkout :</h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                  <h2  style={{fontSize: '16px'}}> {this.state.invoice.orderDate}</h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 className="bold" style={{fontSize:'16px' }}>Total Pembayaran : </h3>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 style={{fontSize: '16px'}}>{this.state.invoice.orderDetail.subtotal}</h3>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-3 col-sm-6">
                    <h2 className="bold" style={{fontSize: '16px'}}>Status : </h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                  <h2  style={{fontSize: '16px'}}> {this.state.invoice.status}</h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 className="bold" style={{ fontSize:'16px' }}>Skema Berlangganan : </h3>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 style={{fontSize: '16px'}}> {this.state.invoice.orderDetail.schema}</h3>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-3 col-sm-6">
                    <h2 className="bold" style={{fontSize: '16px'}}>Metode Pembayaran : </h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                  <h2  style={{fontSize: '16px'}}> {this.state.invoice.paymentMethod}</h2>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 className="bold" style={{ fontSize:'16px' }}>Kode Pembayaran : </h3>
                  </div>
                  <div className="col-lg-3 col-sm-6">
                    <h3 style={{fontSize: '16px'}}> {this.state.invoice.code}</h3>
                  </div>
                </div>

                {/* <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Tanggal Checkout</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.orderDate}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold right">
                  <div className="col-sm-5"><h2 className="bold">Kode Invoice</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.code}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Tanggal Pesanan</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.orderDate}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Nama Pembeli</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.buyer}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Metode Pembayaran</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.paymentMethod}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Status</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.status}</h2></div>
                </div>
                <div className="s-12 m-12 l-10 bold left">
                  <div className="col-sm-5"><h2 className="bold">Berlaku s/d</h2></div>
                  <div className="col-sm-7"><h2> : {this.state.invoice.expired}</h2></div>
                </div> */}

              </div>
            </div>
          </div>
          <div className="text-left">
            <div className="line">
              <div className="margin-bottom" style={{ marginTop: 100 }}>
                <div className="margin" style={{ marginBottom: 50 }}>
                  <div className="m-12 bold left invoice-detail-header">
                    <div className="col-sm-2"><h3>Domain</h3></div>
                    <div className="col-sm-3 text-center"><h3>Skema</h3></div>
                    <div className="col-sm-2 text-center"><h3>Jumlah</h3></div>
                    <div className="col-sm-2 text-center"><h3>PPN</h3></div>
                    <div className="col-sm-3 text-right"><h3>Subtotal</h3></div>
                    <br />
                    <hr />
                  </div>

                  <div className="m-12 bold left invoice-detail-content" >
                    <div className="col-sm-2"><h3>{this.state.invoice.orderDetail.domain}</h3></div>
                    <div className="col-sm-3 text-center"><h3>{this.state.invoice.orderDetail.schema}</h3></div>
                    <div className="col-sm-2 text-center"><h3>{this.state.invoice.orderDetail.amount}</h3></div>
                    <div className="col-sm-2 text-right"><h3>10%</h3></div>
                    <div className="col-sm-3 text-right"><h3>{this.state.invoice.orderDetail.total}</h3></div>
                    <br />
                  </div>
                  <div className="m-12 bold left">
                    <div className="col-sm-3"></div>
                    <div className="col-sm-2"></div>
                    <div className="col-sm-7">
                      <hr />
                      <div className="col-sm-6 text-right"><h3 style={{ fontWeight: 600 }}>Total Pembayaran:</h3></div>
                      <div className="col-sm-6 text-right" style={{ paddingRight: 0 }}><h3>{this.state.invoice.orderDetail.subtotal}</h3></div>
                    </div>
                  </div>


                  <br />
                  {/* <div className="detail-box-content">
                        <div className="detail-item mb-16">
                          <div className="detail-item-left">
                            <div className="fs-14 bold">Total Pembayaran</div>
                          </div>
                          <div className="fs-14 bold" >Rp. {rupiah}</div>
                        </div>
                        <div className="detail-item detail-item-other">
                          <div className="detail-item-left">
                            <div className="detail-item-name">Biaya Layanan</div>
                          </div>
                          <div className="detail-item-price">Gratis</div>
                        </div>
                      </div>
                      <div className="detail-box  mb-24">
                        <div className="detail-box-content">
                          <div className="detail-item">
                            <div className="detail-item-left">
                              <div className="fs-14 black bold">Total Bayar</div>
                              
                            </div>
                            <div className="detail__tagihan-amount bold with-currency fs-14">Rp<span className="detail__tagihan-payment-left payment-left">. {rupiah}</span></div>
                          </div>
                        </div>
                      </div>
                      <div className="fs-16 mb-16 bold">Produk Yang Dibeli</div>
                        <div className="detail-box mb-24">
                          <div className="detail-item">
                            <div className="detail-item-left">
                              <div className="detail-item-name">Nama Domain : {this.state.invoice.domain}</div>
                              s
                            </div>
                            <div className="detail-item-price">Rp. {rupiah}</div>
                          </div>
                          <div className='detail-store-name fs-14 '>Berlangganan Indonesia Global, PT</div>
                            <div className="detail-box-content pt-8">
                              
                              
                            </div>
                        </div>
                        */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    kode_invoice: ownProps.match.params.kode_invoice,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailInvoice: (data) => dispatch(getDetailInvoice(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Invoice))