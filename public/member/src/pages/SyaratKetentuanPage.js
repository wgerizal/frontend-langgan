/* eslint-disable */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import Script from 'react-load-script';
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'
import WALogo from '../styles/img/wa.png';
import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  userLogout,
  submitForgotPass,
  userSubscribe,
} from '../actions/users'
// import './styles/App.css';

class SyaratKetentuanPage extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleChange',
      'handleSubmitForgotPass',
      'handleSubscribe',
      'handleSubmitSubscribe',
    ])

    this.state = {
      modal: false,
      email: '',
      email_subscribe: '',
    }

  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      globalUiLoginModal,
      loginUi,
    } = this.props

    globalUiLoginModal(false)
    //Responsee tabs
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Custom forms
    $(function () {
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    });
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    if (isLogged == false) {
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }



  }

  handleChange(e) {
    this.setState({ email: e.target.value })
  }

  handleSubmitForgotPass(e) {
    const {
      submitForgotPass,
    } = this.props
    let email = this.state.email
    submitForgotPass(email)
  }

  handleSubscribe(e) {
    this.setState({ email_subscribe: e.target.value })
  }
  handleSubmitSubscribe() {

    const {
      userSubscribe
    } = this.props

    userSubscribe(this.state.email_subscribe)

    this.setState({
      email_subscribe: ''
    })

  }

  //fungsi whatsapp button
  handleScriptCreate() {
    this.setState({ scriptLoaded: false })
  }
  
  handleScriptError() {
    this.setState({ scriptError: true })
  }
  
  handleScriptLoad() {
    this.setState({ scriptLoaded: true })
  }

  render() {
    const {
      user
    } = this.props


    const isLogged = user.get('logged_in')

    return (
      <div className="tema">
        {/* <ModalLogin modal={modal} handleClose={this.handleLogin}/> */}
        {/* TOP NAV WITH LOGO */}
        <header>
          <nav className="nav-fixed">
            <div className="line">
              <div className="s-12 m-3 l-2 div-langgan-logo">
                <Link to={`/`}><img className="s-5 l-12 img-langgan-logo" src={LangganLogo} onClick={this.scrollToTop} alt="Logo Langgan Indonesia" title="Logo Langgan Indonesia" /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  <li><Link onClick={this.closeNav} className="menu-tab navBarMargin" to={`/#second-block`}>Benefit</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab navBarMargin" to={`/tema-page`}>Tema</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab navBarMargin" to={`/#pilihpaket-block`}>Harga</Link></li>
                  <li><Link onClick={this.closeNav} className="menu-tab navBarMargin" to={`/#fourth-block`}>Hubungi Kami</Link></li>
                  <li>
                    <a href="http://blog.langgan.id/" target="_blank" className="menu-tab navBarMargin navBarBlogMargin">Blog</a>
                  </li>
                  {
                    isLogged == false
                      ?
                      <li className="vertical-align-center" style={{ height: 50 }}>
                        <div
                          onClick={this.handleLogin}
                          className="menu-tab buttonLoginStyle"
                        // style={{color: '#0B877E', border:'2px solid #0B877E', padding:5, paddingLeft:15, paddingRight:15, borderRadius:20}}

                        >
                          Masuk / Daftar
                              </div>
                      </li>
                      :
                      <Fragment>
                        <li><Link to={`/member-area`} className="menu-tab" style={{ color: '#0B877E', fontWeight: 'bold' }}>Member Area</Link></li>
                        <li><a onClick={this.handleLogin} className="menu-tab" style={{ color: '#0B877E', marginLeft: 21, fontWeight: 'bold' }}>Keluar</a></li>
                      </Fragment>
                  }
                </ul>
              </div>
            </div>
          </nav>
        </header>

        <div className="">
          <Script
            url="https://widget.tochat.be/bundle.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}
            attributes={{key:"1d3e8f55-968b-46eb-9aea-53301a6512db"}}
          />
          {/* <div className="text-right"><a target="_blank" href="https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..." className="btn btn-hubungi-wa" type="submit"><i class="fa fa-whatsapp"></i></a></div> */}
        </div>
        <section className="text-left">
          {/* FIRST BLOCK */}
          <div id="kebijakan-privasi-block" className="text-left">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="s-12 m-12 l-12 left" style={{ paddingLeft: 45, paddingRight: 45 }}>
                    <h2 style={{ marginBottom: '2%', textAlign: 'center' }}><strong>Syarat & Ketentuan Langgan</strong></h2>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Definisi</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>PT Berlangganan Indonesia Global adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa pembuatan web. Selanjutnya disebut LANGGAN.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Situs LANGGAN adalah </span><a href="http://www.langgan.id" style={{ color: '#15BFAE', textTransform: 'lowercase' }}><span style={{ fontWeight: 400 }}>www.LANGGAN.id</span></a><span style={{ fontWeight: 400 }}>.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Syarat &amp; ketentuan adalah perjanjian antara Pelanggan dan LANGGAN yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab Pelanggan dan LANGGAN, serta tata cara Pelangganan sistem layanan LANGGAN.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan adalah pihak yang menggunakan layanan LANGGAN.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pembeli adalah Pihak yang membeli produk yang dijual oleh Pelanggan melalui website yang dibuat oleh LANGGAN</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan dengan ini menyatakan bahwa Pelanggan adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.</span></p>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN memungut biaya pendaftaran kepada Pelanggan.</span></p>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat &amp; ketentuan dan/atau hukum yang berlaku, penutupan toko, suspensi akun, dan/atau penghapusan akun Pelanggan.</span></p>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN tidak akan meminta username, password maupun kode SMS verifikasi atau kode OTP milik akun Pelanggan untuk alasan apapun, oleh karena itu LANGGAN menghimbau Pelanggan agar tidak memberikan data tersebut maupun data penting lainnya kepada pihak yang mengatasnamakan LANGGAN atau pihak lain yang tidak dapat dijamin keamanannya.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan dengan ini menyatakan bahwa LANGGAN tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun Pelanggan yang diakibatkan oleh kelalaian Pelanggan, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi (OTP), password atau email kepada pihak lain, maupun kelalaian Pelanggan lainnya yang mengakibatkan kerugian ataupun kendala pada akun Pelanggan.</span></p>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs yang dibuat oleh LANGGAN atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pembeli.</span></p>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN tidak bertanggung jawab atas setiap konten yang dimuat atau diunggah oleh Pelanggan</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan dilarang mempergunakan foto/gambar Barang yang memiliki watermark yang menandakan hak kepemilikan orang lain.</span></p>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Pelanggan adalah tanggung jawab Pelanggan secara pribadi.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Jenis Barang yang&nbsp;Dilarang</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Berikut ini adalah daftar jenis Barang yang dilarang untuk diperdagangkan oleh Pelanggan pada Situs yang dibuat oleh LANGGAN:</span></p>
                    <ol style={{ marginLeft: '2%', marginBottom: '2%' }}>
                      <li><span style={{ fontWeight: 400 }}>Segala jenis obat-obatan maupun zat-zat lain yang dilarang ataupun dibatasi peredarannya menurut ketentuan hukum yang berlaku, termasuk namun tidak terbatas pada ketentuan Undang-Undang Narkotika, Undang-Undang Psikotropika, dan Undang-Undang Kesehatan. Termasuk pula dalam ketentuan ini adalah obat keras, obat-obatan yang memerlukan resep dokter, obat bius dan sejenisnya, atau obat yang tidak memiliki izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</span></li>
                      <li><span style={{ fontWeight: 400 }}>Kosmetik dan makanan minuman yang membahayakan keselamatan Pelanggannya, ataupun yang tidak mempunyai izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</span></li>
                      <li><span style={{ fontWeight: 400 }}>Bahan yang diklasifikasikan sebagai Bahan Berbahaya menurut Peraturan Menteri Perdagangan yang berlaku.</span></li>
                      <li><span style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}><span style={{ fontWeight: 400 }}>Jenis Produk tertentu yang wajib memiliki:</span></span></span>
                        <ul style={{ listStyleType: 'disc', marginLeft: '1.5%' }}>
                          <li>SNI;</li>
                          <li>Petunjuk Pelangganan dalam Bahasa Indonesia; atau</li>
                          <li>Label dalam Bahasa Indonesia.</li>
                          <li>Sementara yang diperjualbelikan tidak mencantumkan hal-hal tersebut.</li>
                        </ul>
                      </li>
                      <li><span style={{ fontWeight: 400 }}>Barang-barang lain yang kepemilikannya ataupun peredarannya melanggar ketentuan hukum yang berlaku di Indonesia.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang yang merupakan hasil pelanggaran Hak Cipta, termasuk namun tidak terbatas dalam media berbentuk buku, CD/DVD/VCD, informasi dan/atau dokumen elektronik, serta media lain yang bertentangan dengan Undang-Undang Hak Cipta.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang dewasa yang bersifat seksual berupa obat perangsang, alat bantu seks yang mengandung konten pornografi, serta obat kuat dan obat-obatan dewasa, baik yang tidak memiliki izin edar BPOM maupun yang peredarannya dibatasi oleh ketentuan hukum yang berlaku.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Minuman beralkohol.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Segala bentuk tulisan yang dapat berpengaruh negatif terhadap pemakaian situs ini.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pakaian dalam bekas.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Senjata api, senjata tajam, senapan angin, dan segala macam senjata.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Dokumen pemerintahan dan perjalanan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Seragam pemerintahan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Bagian/Organ manusia.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Mailing list dan informasi pribadi.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang-Barang yang melecehkan pihak/ras tertentu atau dapat merendahkan martabat orang lain.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Atribut kepolisian.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang hasil tindak pencurian.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pembuka kunci dan segala aksesori penunjang tindakan perampokan/pencurian.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang yang dapat dan atau mudah meledak, menyala atau terbakar sendiri.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang cetakan/rekaman yang isinya dapat mengganggu keamanan &amp; ketertiban serta stabilitas nasional.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Uang tunai termasuk valuta asing kecuali Pelanggan memiliki dan dapat mencantumkan izin sebagai Penyelenggara Kegiatan Usaha Penukaran Valuta Asing Bukan Bank berdasarkan Peraturan Bank Indonesia No.18/20/PBI/2016 dan/atau peraturan lainnya yang terkait dengan penukaran valuta asing.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pengacak sinyal, penghilang sinyal, dan/atau alat-alat lain yang dapat mengganggu sinyal atau jaringan telekomunikasi</span></li>
                      <li><span style={{ fontWeight: 400 }}>Perlengkapan dan peralatan judi.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Jimat-jimat, benda-benda yang diklaim berkekuatan gaib dan memberi ilmu kesaktian.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang dengan hak Distribusi Eksklusif yang hanya dapat diperdagangkan dengan sistem Pelangganan langsung oleh Pelanggan resmi dan/atau Barang dengan sistem Pelangganan Multi Level Marketing.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Produk non fisik yang tidak dapat dikirimkan melalui jasa kurir, termasuk namun tidak terbatas pada produk pulsa/voucher (telepon, listrik, game, dan/atau credit digital), tiket pesawat dan/atau tiket kereta.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Tiket pertunjukan, termasuk namun tidak terbatas pada tiket konser, baik fisik maupun non fisik.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Dokumen-dokumen resmi seperti Sertifikat Toefl, Ijazah, Surat Dokter, Kwitansi, dan lain sebagainya</span></li>
                      <li><span style={{ fontWeight: 400 }}>Segala jenis Barang lain yang bertentangan dengan peraturan pengiriman Barang Indonesia.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Barang-Barang lain yang melanggar ketentuan hukum yang berlaku di Indonesia.</span></li>
                    </ol>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Pengiriman Barang</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Pengiriman Barang dalam sistem LANGGAN wajib menggunakan jasa perusahaan ekspedisi yang telah mendapatkan verifikasi rekanan LANGGAN yang dipilih oleh Pembeli.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Setiap ketentuan berkenaan dengan proses pengiriman Barang adalah wewenang sepenuhnya penyedia jasa layanan pengiriman Barang.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan wajib memenuhi ketentuan yang ditetapkan oleh jasa layanan pengiriman barang tersebut dan bertanggung jawab atas setiap Barang yang dikirimkan.</span></p>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan memahami dan menyetujui bahwa setiap permasalahan yang terjadi pada saat proses pengiriman Barang oleh penyedia jasa layanan pengiriman Barang adalah merupakan tanggung jawab penyedia jasa layanan pengiriman.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Kebijakan Pengembalian</strong></h3>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>LANGGAN akan menjamin seluruh layanan yang diberikan kepada Pelanggan akan berjalan dengan baik. Maka dari itu langgan tidak akan memberikan pengembalian dana kepada Pelanggan.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Keamanan</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>LANGGAN selalu berupaya untuk menjaga Layanan LANGGAN aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi terus-menerus atau akses ke Layanan kami dapat selalu sempurna. Informasi dan data dalam situs LANGGAN memiliki kemungkinan tidak terjadi secara real time.</span></p>
                    <p><span style={{ fontWeight: 400 }}>Pelanggan setuju bahwa Anda memanfaatkan Layanan LANGGAN atas risiko Pelanggan sendiri, dan Layanan LANGGAN diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".</span></p>
                    <p><span style={{ fontWeight: 400 }}>Sejauh diizinkan oleh hukum yang berlaku, LANGGAN (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan Anda setuju untuk tidak menuntut LANGGAN bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari :</span></p>
                    <ol style={{ marginLeft: '2%', marginBottom: '2%' }}>
                      <li><span style={{ fontWeight: 400 }}>Pelangganan atau ketidakmampuan Pelanggan dalam menggunakan Layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Harga, Pengiriman atau petunjuk lain yang tersedia dalam layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Keterlambatan atau gangguan dalam Layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Kelalaian dan kerugian yang ditimbulkan oleh masing-masing Pelanggan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Kualitas Barang.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pengiriman Barang.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pelanggaran Hak atas Kekayaan Intelektual.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Perselisihan antar Pelanggan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pencemaran nama baik pihak lain.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Setiap penyalahgunaan Barang yang sudah dibeli pihak Pelanggan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke Rekening Resmi LANGGAN, yang dengan cara apapun mengatas-namakan LANGGAN ataupun kelalaian penulisan rekening dan/atau informasi lainnya dan/atau kelalaian pihak bank.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Pengiriman untuk perbaikan Barang yang bergaransi resmi dari pihak produsen. Pembeli dapat membawa Barang langsung kepada pusat layanan servis terdekat dengan kartu garansi dan faktur pembelian.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Virus atau perangkat lunak berbahaya lainnya (bot, script, automation tool selain fitur Power Merchant, hacking tool) yang diperoleh dengan mengakses, atau menghubungkan ke layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Gangguan, bug, kesalahan atau ketidakakuratan apapun dalam Layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Kerusakan pada perangkat keras Anda dari Pelangganan setiap Layanan LANGGAN.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Isi, tindakan, atau tidak adanya tindakan dari pihak ketiga, termasuk terkait dengan Produk yang ada dalam situs LANGGAN yang diduga palsu.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Tindak penegakan yang diambil sehubungan dengan akun Pelanggan.</span></li>
                      <li><span style={{ fontWeight: 400 }}>Adanya tindakan peretasan yang dilakukan oleh pihak ketiga kepada akun Pelanggan.</span></li>
                    </ol>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Pelepasan</strong></h3>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Jika Anda memiliki perselisihan dengan satu atau lebih Pelanggan, Anda melepaskan LANGGAN (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya, yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut, termasuk namun tidak terbatas pada kerugian yang timbul dari pembelian Barang yang telah dilarang pada Poin Jenis Barang. Dengan demikian maka Pelanggan dengan sengaja melepaskan segala perlindungan hukum (yang terdapat dalam undang-undang atau peraturan hukum yang lain) yang akan membatasi cakupan ketentuan pelepasan ini.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Ganti Rugi</strong></h3>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Pelanggan akan melepaskan LANGGAN dari tuntutan ganti rugi dan menjaga LANGGAN (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian ini, Pelangganan Layanan LANGGAN yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak ketiga.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Pilihan Hukum</strong></h3>
                    <p style={{ marginBottom: '2%' }}><span style={{ fontWeight: 400 }}>Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.</span></p>

                    <h3 style={{ marginBottom: '1.3%' }}><strong>Pembaharuan</strong></h3>
                    <p><span style={{ fontWeight: 400 }}>Syarat &amp; ketentuan mungkin diubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. LANGGAN menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat &amp; ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan LANGGAN, maka Pelanggan dianggap menyetujui perubahan-perubahan dalam Syarat &amp; Ketentuan.</span></p>
                    <p>&nbsp;</p>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* FOOTER */}
        <footer className="footer-desktop">
          <div className="line vertical-align-center">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
              <div className="s-12 l-4 social-btn-div text-center p-t-20">
                <div className="m-b-20">
                  <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                    <i className="fa fa-twitter"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                    <i className="fa fa-linkedin"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                    <i className="fa fa-youtube"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="s-12 l-4 text-right subscribe">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
              <div className="m-b-10" style={{ marginTop: 10 }}>
                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                  <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', float: 'right', objectFit: 'contain' }} />
                </a> */}

                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                        <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', float:'right', objectFit:'contain', marginRight:10}}/>
                      </a> */}
              </div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
        </footer>

        <footer className="footer-mobile">
          <div className="line vertical-align-center">
            <div className="s-12 l-12">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-4 social-btn-div text-center p-t-20">
              <div className="m-b-20">
                <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                  <i className="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                  <i className="fa fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                  <i className="fa fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                  <i className="fa fa-linkedin"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                  <i className="fa fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>

          <div className="line vertical-align-center m-t-10">
            <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
              <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 text-left subscribe-mobile">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>

          <div className="line vertical-align-center">
            <div className="s-12 l-12 subscribe-mobile">
              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', objectFit: 'contain', float: 'right', marginRight: '20%' }} />
              </a> */}

              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                      <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', bjectFit:'contain', marginRight:10, float:'right'}}/>
                    </a> */}
            </div>
          </div>

          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-4 l-2 text-left">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-4 l-2 text-left">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
          </div>
        </footer>
      </div>
    );

  }

}


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    submitForgotPass: (email) => dispatch(submitForgotPass(email)),
    userSubscribe: (email) => dispatch(userSubscribe(email)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SyaratKetentuanPage))
