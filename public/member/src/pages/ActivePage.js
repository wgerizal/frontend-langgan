/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import logo from '../logo.svg';
import bindAll from 'lodash/bindAll'
import LangganLogo from '../styles/img/logo-langgan.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'



import {
  globalUiLoginModal,
} from '../actions/globalUi'

import {
  userLogout,
  submitForgotPass,
} from '../actions/users'
// import './styles/App.css';

class ActivePage extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleChange',
      'handleSubmitForgotPass',
    ])

    this.state = {
      modal: false,
      email: '',
    }

  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      globalUiLoginModal,
      loginUi,
    } = this.props

    globalUiLoginModal(false)
    //Responsee tabs
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Custom forms
    $(function () {
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    }).parent('li').addClass('active-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    if (isLogged == false) {
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }



  }

  handleChange(e) {
    this.setState({ email: e.target.value })
  }

  handleSubmitForgotPass(e) {
    const {
      submitForgotPass,
    } = this.props
    let email = this.state.email
    submitForgotPass(email)
  }

  render() {
    const {
      user
    } = this.props


    const isLogged = user.get('logged_in')

    return (
      <div className="tema">
        {/* <ModalLogin modal={modal} handleClose={this.handleLogin}/> */}
        {/* TOP NAV WITH LOGO */}
        <header>
          <nav>
            <div className="line">
              <div className="s-12 l-2 p-t-20">
                <Link to={`/`}><img className="s-5 l-12 center" src={LangganLogo} /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  <li><a>Fitur</a></li>
                  <li><a>Demo</a></li>
                  <li><Link to={`/tema-page`}>Tema</Link></li>
                  <li><a>Harga</a></li>
                  <li><a>Hubungi Kami</a></li>
                  <li><a href="https://blog.langgan.id/">Blog</a></li>
                  {isLogged == false ?
                    <li><a onClick={this.handleLogin}>Masuk / Daftar</a></li>
                    :
                    <li><a onClick={this.handleLogin}>Keluar</a></li>
                  }

                </ul>
              </div>
            </div>
          </nav>
        </header>
        <section className="text-center">
          {/* FIRST BLOCK */}
          <div id="tema-lupa-psw-block" className="text-center">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="s-12 m-12 l-7 center">
                    <div className="alert alert-success">Your account has been activated</div>
                  </article>
                  <article className="s-12 m-12 l-7 center">
                    <div onClick={this.handleLogin} className="btn btn-primary">Go To Member Area</div>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* FOOTER */}
        <footer>
          <div className="line">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-6">
                FAQ<br />
                      Komunitas<br />
                {/*Karir*/}
              </div>
              <div className="s-12 l-6">
                Demo<br />
                      Tema<br />
                      Blog
                    </div>
            </div>
            <div className="s-12 l-4">
              <div>Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Anda" className="input-email-subscribe" /></div>
            </div>
          </div>
          <hr />
          <div className="line">
            <div className="s-12 l-6">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-3 text-right">
              Syarat &amp; Ketentuan
                  </div>
            <div className="s-12 l-3 text-right">
              Kebijakan Privasi
                  </div>
          </div>
        </footer>
      </div>
    );

  }

}


const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    submitForgotPass: (email) => dispatch(submitForgotPass(email))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ActivePage))
