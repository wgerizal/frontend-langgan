/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'
import { Prompt } from 'react-router'

var shouldBlockNavigation;
export default class PaketCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false
    }
  }

  componentDidMount() {
    window.onbeforeunload = function (e) {
      e = e || window.event;
      if (e) {
        e.returnValue = 'Sure?';
      }
      return 'Sure?';
    };
  }

  componentDidUpdate = () => {
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true
    } else {
      window.onbeforeunload = undefined
    }
  }

  render() {
    const { active, color } = this.props
    const customProps = {
      active, color

    }
    return (
      <Wrapper {...customProps}>
        <Prompt
          when={shouldBlockNavigation}
          message='You have unsaved changes, are you sure you want to leave?'
        />
        <Title {...customProps}>
          {
            this.props.paket === 'Starter'
              ?
              'Skema 1 Bulan'
              :
              this.props.paket === 'Premium'
                ?
                'Skema 6 Bulan'
                :
                'Skema 12 Bulan'
          }
        </Title>
        <Body {...customProps}>
          <Description {...customProps} >
            <div dangerouslySetInnerHTML={{ __html: this.props.desc }} />
          </Description>
          <Pricing {...customProps}>
            Rp. {this.props.price},-
          </Pricing>
          <Action>
            <Button
              onClick={this.props.onClick}
              className='btn-white'
              style={{ border: "2px solid #518A50", width: '82%', boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)', }}
              {...customProps}>
              Pilih
            </Button>
          </Action>
        </Body>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  display:flex;
  flex-direction:column;
  flex:1;
  margin-left:20px;
  margin-right:20px;
  margin-bottom:20px;
  box-shadow: ${p => p.active ? "0 4px 8px 0 rgba(0,0,0,0.6)" : ""};

  @media only screen and (min-width: 768px){
    margin-bottom:0;
  }
`
const Title = styled.div`
  display:flex;
  height:40px;
  border-top-left-radius:5px;
  border-top-right-radius:5px;
  justify-content:center;
  align-items:center;
  color:#fff;
  background-color:${p => p.color ? p.color : '#508a88'};
`

const Body = styled.div`
  display:flex;
  flex-direction:column;
  border:2px solid ${p => p.color ? p.color : '#508a88'};
  border-top-color:${p => p.active ? '#fff' : p.color};
  background-color:${p => p.active ? (p.color ? p.color : '#508a88') : '#fff'};
  min-height:250px;
`

const Description = styled.div`
  flex:1;
  text-align:left;
  font-size:14px;
  padding:20px;
  color:${p => p.active ? '#fff' : 'inherit'} !important;
`

const Pricing = styled.div`
  font-size: 18px;
  color:#000;
  margin-bottom:10px;
  color:${p => p.active ? '#fff' : 'inherit'} !important;
`

const Action = styled.div`

`

const Button = styled.button`
  border-color:${p => p.color ? p.color : '#508a88'} !important;
  color: ${p => p.color ? p.color : '#508a88'} !important;
  box-shadow:${p => p.active ? "" : "4px 4px 10px rgba(0, 0, 0, 0.5)"};
`