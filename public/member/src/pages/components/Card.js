/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'

export default class Card extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <>
        <Wrapper {...this.props}>
          {this.props.children}
        </Wrapper>
      </>
    )
  }
}

const Wrapper = styled.div`
  display:flex;
  flex-direction:column;
  box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);
  background-color:#fff;
  width: 100%;

  @media only screen and (min-width: 768px){
    width: 90%;
  }

  @media only screen and (min-width: 1024px){
    width: 75%;
  }
`