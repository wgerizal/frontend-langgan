/* eslint-disable */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
export default class Footer extends Component {
   render() {
      return (
         <div>
            <footer className="footer-desktop">
               <div className="line vertical-align-center">
                  <div className="s-12 l-4">
                     <p>Langgan</p>
                     <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
                     <p>(022) 8731 6625 / hello@langgan.id</p>
                  </div>
                  <div className="s-12 l-4 text-left">
                     <div className="s-12 l-4 p-l-40 p-t-20">
                        <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                        <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                     </div>
                     <div className="s-12 l-4 p-l-40 p-t-20">
                        <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                        {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
                     </div>
                     <div className="s-12 l-4 social-btn-div text-center p-t-20">
                        <div className="m-b-20">
                           <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                              <i className="fa fa-facebook"></i>
                           </a>
                           <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                              <i className="fa fa-twitter"></i>
                           </a>
                        </div>
                        <div className="m-b-20">
                           <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                              <i className="fa fa-instagram"></i>
                           </a>
                           <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                              <i className="fa fa-linkedin"></i>
                           </a>
                        </div>
                        <div className="m-b-20">
                           <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                              <i className="fa fa-youtube"></i>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div className="s-12 l-4 text-right subscribe">
                     <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
                     <div><input type="email" placeholder="Masukan Email Anda" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
                  </div>
               </div>
               <div className="line"><hr /></div>
               <div className="line copyright">
                  <div className="s-12 l-8">
                     <p>© 2020 PT. Berlangganan Indonesia Global</p>
                  </div>
                  <div className="s-12 l-2 text-right">
                     <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
                  </div>
                  <div className="s-12 l-2 text-right">
                     <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
                  </div>
               </div>
            </footer>
            <footer className="footer-mobile">
               <div className="line vertical-align-center">
                  <div className="s-12 l-12">
                     <p>Langgan</p>
                     <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
                     <p>(022) 8731 6625 / hello@langgan.id</p>
                  </div>
               </div>
               <div className="line vertical-align-center">
                  <div className="s-12 l-4 social-btn-div text-center p-t-20">
                     <div className="m-b-20">
                        <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                           <i className="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                           <i className="fa fa-twitter"></i>
                        </a>
                        <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                           <i className="fa fa-instagram"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                           <i className="fa fa-linkedin"></i>
                        </a>
                        <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                           <i className="fa fa-youtube"></i>
                        </a>
                     </div>
                  </div>
               </div>
               <div className="line vertical-align-center m-t-10">
                  <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
                     <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                        <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                        <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                     </div>
                     <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                        <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                        {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
                     </div>
                  </div>
               </div>
               <div className="line vertical-align-center">
                  <div className="s-12 l-12 text-left subscribe-mobile">
                     <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
                     <div><input type="email" placeholder="Masukan Email Anda" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
                  </div>
               </div>
               <div className="line"><hr /></div>
               <div className="line copyright">
                  <div className="s-4 l-2 text-left">
                     <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
                  </div>
                  <div className="s-4 l-2 text-left">
                     <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
                  </div>
               </div>
               <div className="line copyright">
                  <div className="s-12 l-8">
                     <p>© 2020 PT. Berlangganan Indonesia Global</p>
                  </div>
               </div>
            </footer>
         </div>
      )
   }
}
