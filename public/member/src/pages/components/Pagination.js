/* eslint-disable */
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import range from 'lodash/range'
import bindAll from 'lodash/bindAll'
import { Map } from 'immutable'

class Pagination extends Component {
	constructor(props) {
		super(props);

		bindAll(this, [
			'pageRange',
			'handleLinkClick',
			'handlePrevLinkClick',
			'handleNextLinkClick',
			// 'handleLastClick',
			// 'handleFirstLinkClick'
		])
	}

	handleNextLinkClick(): boolean | void {
		const {
			addPageNumber,
			fetchData,

			pagination
		} = this.props

		const pageNumber: number = pagination.get('current_page') + 1
		const totalPages: number = pagination.get('total') / pagination.get('per_page')

		if (pageNumber < 1 || pagination.get('current_page') > totalPages) {
			return false
		}

		addPageNumber(pageNumber).then(() => {
			fetchData(this.props.filterData)
		})
	}

	handlePrevLinkClick(): boolean | void {
		const {
			addPageNumber,
			fetchData,

			pagination
		} = this.props

		const pageNumber: number = pagination.get('current_page') - 1
		const totalPages: number = pagination.get('total') / pagination.get('per_page')

		if (pageNumber < 1 || pageNumber > totalPages) {
			return false
		}

		addPageNumber(pageNumber).then(() => {
			fetchData(this.props.filterData)
		})
	}


	pageRange(): Array<number> {
		const { pagination } = this.props

		let startPage: number = 0
		let endPage: number = 0
		let pages: Array<number> = []

		const totalPages = Math.ceil(pagination.get('total') / pagination.get('per_page'))

		if (totalPages <= 3) {
			startPage = 1
			endPage = totalPages
		} else {

			if (pagination.get('current_page') == 1) {
				startPage = 1
				endPage = 3
			} else if (pagination.get('current_page') == pagination.get('last_page')) {
				startPage = pagination.get('last_page') - 2
				endPage = pagination.get('last_page')
			} else if (pagination.get('current_page') <= pagination.get('last_page') && pagination.get('current_page') >= pagination.get('last_page') - 2) {
				startPage = pagination.get('last_page') - 3
				endPage = pagination.get('last_page')
			} else if (pagination.get('current_page') >= 2 && pagination.get('current_page') <= 3) {
				startPage = 1
				endPage = 4
			} else {
				startPage = pagination.get('current_page') - 2
				endPage = pagination.get('current_page') + 2
			}




		}

		pages = range(startPage, endPage + 1)

		return pages
	}


	handleLinkClick(e) {
		const {
			addPageNumber,
			fetchData,
			addPerPageSize,
			perPageSize,
		} = this.props

		const pageNumber: number = e.target.dataset.page

		addPageNumber(pageNumber).then(() => {

			if (perPageSize == 0 || perPageSize == '' || perPageSize == undefined) {

				fetchData(this.props.filterData)
			} else {
				addPerPageSize(perPageSize).then(() => {
					fetchData(this.props.filterData)
				})


			}


		})
	}


	render() {
		const { pagination } = this.props
		const pages: Array<Object> = []
		const total_pages = pagination.get('total') / pagination.get('per_page')
		if (total_pages > 1) {
			if (pagination.get('current_page') == 1) {

				pages.push(
					<button key='pages-link-kembali' className="btn-kembali notactive"><i className="fa fa-angle-left m-r-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i>Kembali</button>
				)



			} else {


				pages.push(
					<button key='pages-link-kembali' onClick={this.handlePrevLinkClick} className="btn-kembali "><i className="fa fa-angle-left m-r-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i>Kembali</button>

				)


			}


			const pageRange: Array<number> = this.pageRange()

			if (pagination.get('current_page') >= 4) {

				pages.push(
					<span key='pages-link-dot' className="dot-pagination">....</span>
				)

			}

			for (var i = 0; i < pageRange.length; i++) {
				const pageNumber: number = pageRange[i]

				if (pageNumber === pagination.get('current_page')) {
					pages.push(
						<button key={'pages-link-' + pageNumber} className="btn-pagination active">{pageNumber}</button>
					)
				} else {
					pages.push(

						<button key={'pages-link-' + pageNumber} onClick={this.handleLinkClick} data-page={pageNumber} className="btn-pagination">{pageNumber}</button>
					)
				}
			}

			if (pagination.get('current_page') <= pagination.get('last_page') - 3) {

				pages.push(
					<span key='pages-link-dot' className="dot-pagination">....</span>
				)

			}


			if (pagination.get('current_page') === pagination.get('last_page')) {


				pages.push(
					<button key='pages-link-selanjutnya' className="btn-selanjutnya notactive">Selanjutnya<i className="fa fa-angle-right m-l-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i></button>
				)
			} else {
				pages.push(
					<button key='pages-link-selanjutnya' onClick={this.handleNextLinkClick} className="btn-selanjutnya">Selanjutnya<i className="fa fa-angle-right m-l-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i></button>
				)
			}

		} else {
			pages.push(
				<button key='pages-link-kembali' className="btn-kembali notactive"><i className="fa fa-angle-left m-r-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i>Kembali</button>
			)
			pages.push(
				<button key='pages-link-1' className="btn-pagination active">1</button>
			)
			pages.push(
				<button key='pages-link-selanjutnya' className="btn-selanjutnya notactive">Selanjutnya<i className="fa fa-angle-right m-l-5" style={{ fontSize: '20px', fontWeight: 'bold' }}></i></button>
			)
		}

		return (
			<div className="margin center">
				{pages}
			</div>
		)
	}
}

export default Pagination