/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'

export default class Stepper extends Component {
  // constructor(props){
  //   super(props)
  //   this.state={
  //     active:this.props.active
  //   }
  // }
  // clickAction = (index)=>{
  //   this.setState({active:index+1})
  //   this.props.clickedStep(index+1)
  // }
  render() {
    return (
      <Parent {...this.props}>
        {this.props.items.map((v, i) => (
          <React.Fragment key={i.toString()}>
            {/* <BWrapper onClick={()=>this.clickAction(i)}> */}
            <BWrapper>
              <Bullet active={this.props.active - 1 >= i ? true : false}>
                <Text active={this.props.active - 1 === i ? true : false}>{v}</Text>
                <i className="fa fa-check" style={{ color: this.props.active - 2 >= i ? "#fff" : "rgba(0,0,0,0)", fontSize: "14px", marginTop: "3px" }}></i>
              </Bullet>
            </BWrapper>
            {i !== this.props.items.length - 1 && (
              <Wrapper>
                <Connector active={this.props.active - 2 >= i ? true : false} />
              </Wrapper>
            )}
          </React.Fragment>
        ))}
      </Parent>
    )
  }
}

const Parent = styled.div`
  display:flex;
  width:100%;
  padding-left:30px;
  padding-right:30px;
  height:70px;
  font-size:10px;

  @media only screen and (min-width: 768px){
    padding-left:100px;
    padding-right:100px;
    font-size:16px;
  }

  @media only screen and (min-width: 400px){
    font-size:12px;
  }

  @media only screen and (min-width: 500px){
    font-size:14px;
  }
`

const Wrapper = styled.div`
  margin-top:-25px;
  display:flex;
  flex:1;
  align-items:center;
`

const BWrapper = styled.div`
  margin-top:-25px;
  display:flex;
  align-items:center;
  cursor: pointer;
`

const Connector = styled.div`
  height:5px;
  flex:1;
  background-color:${p => p.active ? '#15bfae' : 'rgba(0,0,0,0.2)'};
`

const Bullet = styled.div`
  position:relative;
  width:20px;
  height:20px;
  border-radius:10px;
  background-color:${p => p.active ? '#15bfae' : 'rgba(0,0,0,0.2)'};
`
const Text = styled.div`
  position:absolute;
  width:150px;
  left:-65px;
  bottom:-30px;
  color:${p => p.active ? '#000' : 'unset'}
`