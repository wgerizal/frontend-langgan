/* eslint-disable */
import React, { Component } from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png'
import store from '../../helpers/user_session'
import { Link, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { withRouter, Switch } from 'react-router'
import Script from 'react-load-script';
import { userLogout, userResetNotif, userGetNotifikasi } from '../../actions/users'
import ModalEditProfile from '../partials/ModalEditProfile'
import ModalChangePassword from '../partials/ModalChangePassword'

import $ from 'jquery'
import io from 'socket.io-client'
import WALogo from '../../styles/img/wa.png';

class Navbar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isUserOpen: '',
      show_profile: false,
      show_changePassword: false,
    }

    bindAll(this, [
      'handleLogout',
      'handleOpen',
      'handleProfile',
      'handlechangePassword',
      'handleSocketMessage',
      'connectSocket',
      'handleEnvClick',
      'handleToggleEdit',
      'handleToggleChangePassword',
    ])
  }

  handleToggleEdit() {
    this.setState({ show_profile: !this.state.show_profile })
  }

  handleToggleChangePassword() {
    this.setState({ show_changePassword: !this.state.show_changePassword })
  }

  componentDidMount() {
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
      globalUiLoginModal,
      loginUi,
    } = this.props
    // this.connectSocket();
    //Responsee tabs
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      // var count = $(element).find('li').length;
      // var content = '<span class="count-number"> ' + count + '</span>';
      // $(element).closest('li').children('.menu-tab').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      // var count = $(element).find('li').length;
      // var content = '<span class="count-number"> ' + count + '</span>';
      // $(element).closest('li').children('.menu-tab').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    });
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
  }

  componentDidUpdate(prevProps) {
    const { userInformation } = this.props
    if (prevProps.userInformation != this.props.userInformation) {
      this.connectSocket(this.props.userInformation)
    }
  }

  handleEnvClick() {
    const { userResetNotif } = this.props
    userResetNotif()

  }

  handleSocketMessage(message) {
    const { userGetNotifikasi } = this.props
    userGetNotifikasi()
  }

  connectSocket(userInformation) {
    io('https://notif.langgan.id').on(`channel:member-${userInformation.get('id')}`, (message) => {
      console.log(message)
      this.handleSocketMessage(message)
    })
  }

  handleLogout(e) {
    const { userLogout } = this.props
    userLogout()
  }

  handleOpen(e) {
    if (this.state.isUserOpen == '') {
      this.setState({ isUserOpen: 'open' })
    } else {
      this.setState({ isUserOpen: '' })
    }
  }
  handleProfile() {
    this.setState({ show_profile: !this.state.show_profile })
  }
  handlechangePassword() {
    this.setState({ show_changePassword: !this.state.show_changePassword })
  }

  //fungsi whatsapp button
  handleScriptCreate() {
    this.setState({ scriptLoaded: false })
  }
  
  handleScriptError() {
    this.setState({ scriptError: true })
  }
  
  handleScriptLoad() {
    this.setState({ scriptLoaded: true })
  }

  render() {
    const user_email = store.getUserEmail()
    const { tokos, userInformation, userNotification } = this.props
    // console.log(userInformation.toJS())
    // console.log(userNotification.get('list_notifikasi'))
    let notifications = userNotification.get('list_notifikasi')
    let renderListNotif = []
    if (notifications != undefined) {
      notifications.map((notif, index) => {
        renderListNotif.push(
          <li key={index} className="notif-decs">
            <a href={notif.url} className="link-notif" target="_blank">
              <div className="row row-notif">
                <div className="col-md-12 pad-notif">
                  <div className="read-more-notif unread" style={{ color: 'black' }}>{notif.pesan}</div>
                  {
                    notif.pesan === 'Toko anda sedang offline' || notif.pesan === 'Toko anda sedang online'
                      ?
                        <div className="read-more-notif notif-10">{notif.date}</div>
                      :
                        <div className="read-more-notif notif-10">Lihat Detail</div>
                  }
                </div>
              </div>
            </a>
          </li>
        )
      })
    }
    let renderEnvelope;
    if (userNotification.get('jumlah_notifikasi') > 0) {
      renderEnvelope = <i key={'notifikasi-envelope'} className="fa fa-envelope"><span className="badge">{userNotification.get('jumlah_notifikasi')}</span></i>

    } else {
      renderEnvelope = <i className="fa fa-envelope"><span className="badge"></span></i>
    }

    return (
      <div>
        <div className="">
          <Script
            url="https://widget.tochat.be/bundle.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}
            attributes={{key:"1d3e8f55-968b-46eb-9aea-53301a6512db"}}
          />
          {/* <div className="text-right"><a target="_blank" href="https://api.whatsapp.com/send?phone=628112484440&text=Halo%20Customer%20Care%20Langgan%0ABisa%20Tolong%20Saya..." className="btn btn-hubungi-wa" type="submit"><i className="fa fa-whatsapp"></i></a></div> */}
        </div>
        <header>
          <nav className="nav-fixed" style={{boxShadow: '0 8px 6px -6px #999'}}>
            <div className="line">
              <div className="s-12 m-3 l-2 div-langgan-logo">
                <Link to={`/member-area`}><img className="s-5 l-12 img-langgan-logo" src={LangganLogo} /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  {tokos.size != 0 ?
                    <li><Link to={`/tema-page`} className="menu-tab">Tema</Link></li>
                    :
                    ''
                  }
                  <li><NavLink to={`/syarat-domain`} className="menu-tab">Tentang Domain</NavLink></li>

                  <li><NavLink to={`/member-area`} className="menu-tab">Toko Saya</NavLink></li>
                  {
                    tokos.size == 0 
                      ?
                        <li><NavLink to={`/member-paket`} className="menu-tab">Buat Toko</NavLink></li>
                      :
                        ''
                  }
                  <li className="li-line-bound"><a className="line-bound">|</a></li>
                  <li><a className="menu-tab" href='#' onClick={this.handleEnvClick} data-target='dropdown-notif' style={{ color: "#15bfae" }}>
                    {renderEnvelope}
                  </a>
                    <ul id='dropdown-notif' className='dropdown-content-notif'>
                      {renderListNotif}
                    </ul>
                  </li>

                  <li className={`dropdown user user-menu ${this.state.isUserOpen}`}>
                    <a href="#" onClick={this.handleOpen} className="dropdown-toggle" style={{ color: "#15bfae" }} data-toggle="dropdown"><i className="fa fa-user-circle"></i> {user_email} <i className="fa fa-caret-down"></i></a>
                    <ul className="dropdown-menu">
                      <li className="user-footer">
                        <div className="">
                          <a onClick={this.handleProfile} className="btn btn-default btn-flat color-langgan">Profil</a>
                        </div>
                        <div className="">
                            <a onClick={this.handlechangePassword} className="btn btn-default btn-flat color-langgan">Ubah Kata Sandi</a>
                          </div>
                        <div className="">
                          <a onClick={this.handleLogout} className="btn btn-default btn-flat color-langgan">Keluar</a>
                        </div>

                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </header>
        <ModalEditProfile handleToggle={this.handleToggleEdit} show_profile={this.state.show_profile} />
        <ModalChangePassword handleToggle={this.handleToggleChangePassword} show_changePassword={this.state.show_changePassword} />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    tokos: state.tokos,
    userInformation: state.userInformation,
    userNotification: state.userNotification,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogout: () => dispatch(userLogout()),
    userResetNotif: () => dispatch(userResetNotif()),
    userGetNotifikasi: () => dispatch(userGetNotifikasi()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar))
