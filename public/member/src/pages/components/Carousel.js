/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'
import bindAll from 'lodash/bindAll'
import $ from 'jquery'
import ItemsCarousel from 'react-items-carousel';
import MediaQuery from 'react-responsive'

const themeList = [
  { name: 'East Anastacio' },
  { name: 'Lianaland' },
  { name: 'East Sylvan' },
  { name: 'West Nolahaven' },
  { name: 'Mini Austra' },
  { name: 'East Anastacio' },
  { name: 'Lianaland' },
  { name: 'East Sylvan' },
  { name: 'West Nolahaven' },
  { name: 'Mini Austra' },
  { name: 'East Anastacio' },
  { name: 'Lianaland' },
  { name: 'East Sylvan' },
  { name: 'West Nolahaven' },
  { name: 'Mini Austra' },
]

export default class CarouselComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      position: 0,
      preview: false,
      selected: this.props.currentTheme,
      link_preview: '',

      //state button
      buttonLeft: true,
      buttonRight: false,

      //carousel
      valueCarousel:0,
    }
    bindAll(this, [
      'handlePreview',
      'handleNavCarouselLeft',
      'handleNavCarouselRight'
    ])
    this.items = React.createRef()
  }
  changeTheme = (selected, i) => {
    this.setState({ selected })
    this.props.selectedTheme(selected)
    $(".overlay-item").css({ "display": "none" })
    $(".overlay-item").removeClass("kepilih")
    $("#itempaket" + i + " .overlay-item").css({ "display": "block" })
    $("#itempaket" + i + " .overlay-item").addClass("kepilih")
    $(".overlay-item:not(.kepilih)").removeAttr('style')
  }
  handlePreview(link = '') {
    this.setState({ preview: !this.state.preview })
    this.setState({ link_preview: link })
  }

  handleNavCarouselLeft() {
    this.setState({ buttonRight: false })
    if (this.state.position < 0) {
      this.setState({ position: this.state.position + 140, buttonLeft: false })
    } else if (this.state.position === 0) {
      this.setState({ buttonLeft: true })
    }
  }

  handleNavCarouselRight() {
    // console.log(this.props.themaList.size * 140)
    console.log(this.props.themaList.size)
    let offs = 420
    let g = false

    this.setState({ buttonLeft: false })

    if (window.innerWidth <= 1045 && window.innerWidth >= 1024) {
      offs = 280
    }
    if (window.innerWidth < 877 && window.innerWidth > 646) {
      offs = 280
    }
    if (window.innerWidth < 647 && window.innerWidth > 496) {
      offs = 0
    }
    if (window.innerWidth < 377) {
      g = true
    }
    // if (this.state.position - 140 > (this.props.themaList.size * 140 - offs) * -1 || (g && this.state.position > -1370)) {
    //   this.setState({position:this.state.position-140})
    // }
    if (this.state.position - 140 > (this.props.themaList.size * 290 - offs) * -1 || (g && this.state.position > -1370)) {
      this.setState({ position: this.state.position - 140 })
    } else {
      this.setState({ buttonRight: true })
    }
  }

  onChange = value => {
    this.setState({ valueCarousel:value });
  };

  render() {
    const { themaList } = this.props

    const chevronWidth = 40;

    return (
      <Wrapper>
          {/* <Container navigate>
            <Navigator onClick={() => this.handleNavCarouselLeft()} style={{ backgroundColor: this.state.buttonLeft ? 'grey' : '#15BFAE' }}>
              <i className="fa fa-arrow-left" style={{ color: "#fff", marginTop: "6px" }}></i>
            </Navigator>
          </Container> */}

          <Container  item>
            <div ref={this.items} style={{ position: "absolute", height: '100%', maxWidth:'80%'}}>
              <MediaQuery minDeviceWidth={800}>
                {(matches) =>
                  matches
                  ?
                    <ItemsCarousel
                      requestToChangeActive={this.onChange}
                      activeItemIndex={this.state.valueCarousel}
                      numberOfCards={3}
                      slidesToScroll={1}
                      gutter={5}
                      leftChevron={
                        <button style={{marginTop:-25, backgroundColor:'#15BFAE', paddingTop:5, paddingLeft:5, paddingRight:5, paddingBottom:5, fontSize:15}}>
                          <i className="fa fa-arrow-left" style={{ color: "#fff"}}></i>
                        </button>
                      }
                      rightChevron={
                        <button style={{marginTop:-25, backgroundColor:'#15BFAE', paddingTop:5, paddingLeft:5, paddingRight:5, paddingBottom:5, fontSize:15}}>
                          <i className="fa fa-arrow-right" style={{ color: "#fff"}}></i>
                        </button>
                      }
                      outsideChevron={true}
                      chevronWidth={chevronWidth}
                      showSlither={false}
                      firstAndLastGutter={false}
                      activePosition={'center'}
                      disableSwipe={false}
                      alwaysShowChevrons={false}
                    >
                      {themaList.map((v, i) =>
                        <Item className="item-paket" style={{height:180}} id={"itempaket" + i}>
                          <div className="overlay-item">
                            <div className="">
                              <p style={{
                                backgroundColor: "#15BFAE",
                                color: "white",
                                fontWeight:'400',
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}>{v.name}</p>
                              <button onClick={() => this.handlePreview(v.link_preview)} style={{
                                backgroundColor: "#fff",
                                color: "#000",
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}>Lihat Tema</button>
                              <button onClick={() => this.changeTheme(v, i)} key={i.toString()} selected={this.state.selected === null || this.state.selected.id != v.id ? false : true} style={{
                                backgroundColor: !(this.state.selected === null || this.state.selected.id != v.id) ? "#15bfae" : "#fff",
                                color: !(this.state.selected === null || this.state.selected.id != v.id) ? "#fff" : "#000",
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}><i class={!(this.state.selected === null || this.state.selected.id != v.id) ? "fa fa-check" : ""}></i> {!(this.state.selected === null || this.state.selected.id != v.id) ? "Dipilih" : "Pilih Tema"}</button>
                            </div>
                          </div>
                          <div className="wrap-hover">
                            <img description={v.description} className="img-tema" src={v.thumbnail} style={{ width: '300px', height: '100%' }} />
                          </div>
                          <Title>{v.name}</Title>
                        </Item>)}
                    </ItemsCarousel>
                  :
                    <ItemsCarousel
                      requestToChangeActive={this.onChange}
                      activeItemIndex={this.state.valueCarousel}
                      numberOfCards={1}
                      slidesToScroll={1}
                      gutter={5}
                      leftChevron={
                        <button style={{marginTop:-25, backgroundColor:'#15BFAE', paddingTop:5, paddingLeft:5, paddingRight:5, paddingBottom:5, fontSize:15}}>
                          <i className="fa fa-arrow-left" style={{ color: "#fff"}}></i>
                        </button>
                      }
                      rightChevron={
                        <button style={{marginTop:-25, backgroundColor:'#15BFAE', paddingTop:5, paddingLeft:5, paddingRight:5, paddingBottom:5, fontSize:15}}>
                          <i className="fa fa-arrow-right" style={{ color: "#fff"}}></i>
                        </button>
                      }
                      outsideChevron={true}
                      chevronWidth={chevronWidth}
                      showSlither={false}
                      firstAndLastGutter={false}
                      activePosition={'center'}
                      disableSwipe={false}
                      alwaysShowChevrons={false}
                    >
                      {themaList.map((v, i) =>
                        <Item className="item-paket" style={{height:180}} id={"itempaket" + i}>
                          <div className="overlay-item">
                            <div className="">
                              <p style={{
                                backgroundColor: "#15BFAE",
                                color: "white",
                                fontWeight:'400',
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}>{v.name}</p>
                              <button onClick={() => this.handlePreview(v.link_preview)} style={{
                                backgroundColor: "#fff",
                                color: "#000",
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}>Lihat Tema</button>
                              <button onClick={() => this.changeTheme(v, i)} key={i.toString()} selected={this.state.selected === null || this.state.selected.id != v.id ? false : true} style={{
                                backgroundColor: !(this.state.selected === null || this.state.selected.id != v.id) ? "#15bfae" : "#fff",
                                color: !(this.state.selected === null || this.state.selected.id != v.id) ? "#fff" : "#000",
                                margin: "10px auto",
                                width: "100%",
                                fontSize: "12px",
                                padding: "1px 5px",
                                width: "70%",
                                display: "block"
                              }}><i class={!(this.state.selected === null || this.state.selected.id != v.id) ? "fa fa-check" : ""}></i> {!(this.state.selected === null || this.state.selected.id != v.id) ? "Dipilih" : "Pilih Tema"}</button>
                            </div>
                          </div>
                          <div className="wrap-hover">
                            <img description={v.description} className="img-tema" src={v.thumbnail} style={{ width: '300px', height: '100%' }} />
                          </div>
                          <Title>{v.name}</Title>
                        </Item>)}
                    </ItemsCarousel>
                  }
                </MediaQuery>
              </div>

            <div id="id03" className={`modal ${this.state.preview == true ? "show" : ''}`} >
              <div className="modal-content-preview animate" style={{ overflow: 'auto' }}>
                <div className="div-tema-content">
                  <button className="button-close pull-right" onClick={this.handlePreview}></button>
                  <article className="s-12 m-12 l-7 center">
                  </article>
                </div>
                <div id="tema-preview" className="text-left">
                  <div className="line-preview">
                    <iframe src={this.state.link_preview} style={{ width: '100%' }} />
                  </div>
                </div>
              </div>
            </div>
          </Container>
          
          {/* <Container navigate>
            <Navigator onClick={() => this.handleNavCarouselRight()} style={{ backgroundColor: this.state.buttonRight ? 'grey' : '#15BFAE' }}>
              <i className="fa fa-arrow-right" style={{ color: "#fff", marginTop: "6px" }}></i>
            </Navigator>
          </Container> */}

      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  width:100%;
  height:200px;
  display:flex;
  margin-top:100px;
`

const Container = styled.div`
  overflow:hidden;
  ${p => p.item && `
    flex:1;
    position:relative;
    width:300px;
    height:140px;
    display:flex;
    justify-content:center;
    align-items:center;
  `}
  ${p => p.navigate && `
    align-self:center;
  `}
`

const Item = styled.div`
  display: inline-block;
  background-color:#fff;
  width:300px;
  height:140px;
  margin-left:5px;
  margin-right:5px;
  cursor: pointer;
  border: 5px solid ${p => p.selected ? '#518a50' : '#fff'};
  position:relative;
  
`

const Title = styled.div`
  position:absolute;
  bottom:-5px;
  background-color: rgba(21, 191, 174, 0.3);
  
  width:100%;
  height:30px;
  display:flex;
  justify-content:center;
  align-items:center;
  color:#000;
  font-size:13px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
`

const Navigator = styled.div`
  background-color:#15bfae;
  width:30px;
  height:30px;
  border-radius:15px;
  margin-left:10px;
  margin-right:10px;
  cursor: pointer;
`