/* eslint-disable */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import bindAll from 'lodash/bindAll'
import $ from 'jquery'
import { push } from 'react-router-redux';
import { Link as LinkScroll, animateScroll as scroll } from "react-scroll";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Script from 'react-load-script';
import TagManager from 'react-gtm-module';

//library pop up 
import 'rodal/lib/rodal.css';

//library facebook pixel
import ReactPixel from 'react-facebook-pixel';

//import actions
import {
  globalUiLoginModal,
  indexAddSelectedPaket,
} from '../actions/globalUi';

import {
  userLogout,
  userSubscribe,
} from '../actions/users';


//import images
import LangganLogo from '../styles/img/logo-langgan.png';

const paket = [
  {
    id: 1,
    name: 'Starter',
    color: '#508A88',
    desc: ` <ul>
            <li>Gratis Domain</li>
            <li>1 Bulan Berlangganan</li>
            <li>Pasang Domainmu Sendiri</li>
            <li>Seluruh Fitur Toko Online Langgan</li>
          </ul>`,
    pricing: "300.000"
  }, {
    id: 2,
    name: 'Premium',
    color: '#518a50',
    desc: `<ul>
            <li>6 Bulan Berlangganan</li>
            <li>Gratis Domain</li>
            <li>Pasang Domainmu Sendiri</li>
            <li>Seluruh Fitur Toko Online Langgan</li>
          </ul>`,
    pricing: "1.500.000"
  }, {
    id: 3,
    name: 'Enterprise',
    color: '#50528a',
    desc: ` <ul>
            <li>1 Tahun Berlangganan</li>
            <li>Gratis Domain</li>
            <li>Pasang Domainmu Sendiri</li>
            <li>Seluruh Fitur Toko Online Langgan</li>
          </ul>`,
    pricing: "2.500.000"
  },
]


class LangganHome extends Component {
  constructor(props) {
    super(props)

    bindAll(this, [
      'handleLogin',
      'handleProfile',
      'handleSubscribe',
      'handleSubmitSubscribe',
      'handlePaketClick',
    ])

    this.state = {
      modal: false,
      onTop: true,
      email_subscribe: '',
      visibleModal:false,
    }

    this.scrollToTop = this.scrollToTop.bind(this);
  }

  handlePaketClick(e, params) {
    const { redirectMemberPaket, indexAddSelectedPaket } = this.props
    indexAddSelectedPaket(paket[params])
      .then(() => {
        redirectMemberPaket()
      })

  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  componentDidMount() {
    const parent = this;

    const options = {
      autoConfig: true, // set pixel's autoConfig
      debug: false, // enable logs
    };
    ReactPixel.init('490052041836692', options);
    ReactPixel.track('track', 'PageView');
    ReactPixel.track('track', 'ViewContent');

    window.onscroll = function () {
      if (window.pageYOffset < 100) {
        parent.setState({ onTop: true });
      } else {
        parent.setState({ onTop: false });
      }
    };
    const {
      current_tabs,
      tip,
      tip_top,
      tip_bottom,
    } = this.props
    var window_url = window.location.href
    var convert_to_array = window_url.split('/')
    if (convert_to_array.length == 3) {
      $('html,body').animate({ scrollTop: $(convert_to_array[3]).offset().top - 78 }, 'slow');
    }
    $(window).scroll(function () {
      if (typeof $('#second-block').offset() != 'undefined') {
        var light_pos = $('#second-block').offset().top;
        var light_height = $('#second-block').height();
        var menu_pos = $('.menu-tab').offset().top;
        var scroll = $(window).scrollTop();
        if (scroll > 520 && scroll < 1000) {
          $('.menu-tab[data-target="#second-block"]').addClass('menu_green');
          $('.menu-tab[data-target="#pilihpaket-block"]').removeClass('menu_green');
          $('.menu-tab[data-target="#fourth-block"]').removeClass('menu_green');
        }
        else if (scroll > 1450 && scroll < 2100) {
          $('.menu-tab[data-target="#second-block"]').removeClass('menu_green');
          $('.menu-tab[data-target="#pilihpaket-block"]').addClass('menu_green');
          $('.menu-tab[data-target="#fourth-block"]').removeClass('menu_green');
        }
        else if (scroll > 2160 && scroll < 2700) {
          $('.menu-tab[data-target="#second-block"]').removeClass('menu_green');
          $('.menu-tab[data-target="#pilihpaket-block"]').removeClass('menu_green');
          $('.menu-tab[data-target="#fourth-block"]').addClass('menu_green');
        }
      }

    })
    $('.tabs').each(function (intex, element) {
      current_tabs = $(this);
      $('.tab-label').each(function (i) {
        var tab_url = $(this).attr('data-url');
        if ($(this).attr('data-url')) {
          $(this).closest('.tab-item').attr("id", tab_url);
          $(this).attr("href", "#" + tab_url);
        } else {
          $(this).closest('.tab-item').attr("id", "tab-" + (i + 1));
          $(this).attr("href", "#tab-" + (i + 1));
        }
      });
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      function loadTab() {
        $(this).parent().children().removeClass("active-btn");
        $(this).addClass("active-btn");
        var tab = $(this).attr("href");
        $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
        $(this).parent().parent().find(tab).fadeIn();
        $('html,body').animate({ scrollTop: $(tab).offset().top - 160 }, 'slow');
        if ($(this).attr('data-url')) {
        } else {
          return false;
        }
      }
      $(this).find(".tab-nav a").click(loadTab);
      $(this).find('.tab-label').each(function () {
        if ($(this).attr('data-url')) {
          var tab_url = window.location.hash;
          if ($(this).parent().find('a[href="' + tab_url + '"]').length) {
            loadTab.call($(this).parent().find('a[href="' + tab_url + '"]')[0]);
          }
        }
      });
      var url = window.location.hash;
      if ($(url).length) {
        $('html,body').animate({ scrollTop: $(url).offset().top - 160 }, 'slow');
      }
    });
    //Slide nav
    $('<div class="slide-nav-button"><div class="nav-icon"><div></div></div></div>').insertBefore(".slide-nav");
    $(".slide-nav-button").click(function () {
      $("body").toggleClass("active-slide-nav");
    });
    //Responsee eside nav
    $('.aside-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.aside-nav > ul > li:has(ul)').addClass('aside-submenu');
    $('.aside-nav > ul ul > li:has(ul)').addClass('aside-sub-submenu');
    $('.aside-nav > ul > li.aside-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul li.aside-submenu:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul li.aside-submenu:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    $('.aside-nav > ul ul > li.aside-sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.aside-nav ul ul li:not(:hover) > ul').removeClass('show-aside-ul', 'fast');
      $('.aside-nav ul ul li:hover > ul').toggleClass('show-aside-ul', 'fast');
    });
    //Mobile aside navigation
    $('.aside-nav-text').each(function (index, element) {
      $(element).click(function () {
        $('.aside-nav > ul').toggleClass('show-menu', 'fast');
      });
    });
    //Responsee nav
    // Add nav-text before top-nav
    $('.top-nav').before('<p class="nav-text"><span></span></p>');
    $('.top-nav > ul > li ul').each(function (index, element) {
      var count = $(element).find('li').length;
      var content = '<span class="count-number"> ' + count + '</span>';
      $(element).closest('li').children('a').append(content);
    });
    $('.top-nav > ul li:has(ul)').addClass('submenu');
    $('.top-nav > ul ul li:has(ul)').addClass('sub-submenu').removeClass('submenu');
    $('.top-nav > ul li.submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav > ul li.submenu > ul').removeClass('show-ul', 'fast');
      $('.top-nav > ul li.submenu:hover > ul').toggleClass('show-ul', 'fast');
    });
    $('.top-nav > ul ul > li.sub-submenu > a').attr('aria-haspopup', 'true').click(function () {
      //Close other open sub menus
      $('.top-nav ul ul li > ul').removeClass('show-ul', 'fast');
      $('.top-nav ul ul li:hover > ul').toggleClass('show-ul', 'fast');
    });
    //Mobile navigation
    $('.nav-text').click(function () {
      $("body").toggleClass('show-menu');
    });
    //Custom forms
    $(function () {
      var urlHash = window.location.href.split("#")[1];
      if (urlHash) {
        $('html,body').animate({
          scrollTop: $('#' + urlHash).offset().top - 77
        }, 500);
      }
      var input = document.createElement("input");
      if (('placeholder' in input) == false) {
        $('[placeholder]').focus(function () {
          var i = $(this);
          if (i.val() == i.attr('placeholder')) {
            i.val('').removeClass('placeholder');
            if (i.hasClass('password')) {
              i.removeClass('password');
              this.type = 'password';
            }
          }
        }).blur(function () {
          var i = $(this);
          if (i.val() == '' || i.val() == i.attr('placeholder')) {
            if (this.type == 'password') {
              i.addClass('password');
              this.type = 'text';
            }
            i.addClass('placeholder').val(i.attr('placeholder'));
          }
        }).blur().parents('form').submit(function () {
          $(this).find('[placeholder]').each(function () {
            var i = $(this);
            if (i.val() == i.attr('placeholder')) i.val('');
          })
        });
      }
    });
    //Tooltip
    $(".tooltip-container").each(function () {
      $(this).hover(function () {
        var pos = $(this).position();
        var container = $(this);
        var pos = container.offset();
        tip = $(this).find('.tooltip-content');
        tip_top = $(this).find('.tooltip-content.tooltip-top');
        tip_bottom = $(this).find('.tooltip-content.tooltip-bottom');

        var height = tip.height();
        tip.fadeIn("fast"); //Show tooltip
        tip_top.css({
          top: pos.top - height,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
        tip_bottom.css({
          top: pos.top,
          left: pos.left + (container.width() / 2) - (tip.outerWidth(true) / 2)
        })
      }, function () {
        tip.fadeOut("fast"); //Hide tooltip
      });
    });
    //Active item
    var url = window.location.href;
    $('a').filter(function () {
      return this.href == url;
    }).parent('li').addClass('active-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    var url = window.location.href;
    $('.aside-nav a').filter(function () {
      return this.href == url;
    }).parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('active-aside-item');
    $('.menu-tab').click(function (event) {
      if ($(this).data('target')) {
        $('html,body').animate({ scrollTop: $($(this).data('target')).offset().top - 77 }, 'slow');
      }
    });

    //code untuk goolge tag manager / GTM
    const tagManagerArgs = {
      gtmId: 'AW-707809799',
      events: {
        'gtm.start': new Date().getTime(),
        'conversion':{'send_to': 'AW-707809799/oXAnCJeJzNABEIekwdEC'}
      }
    }
  
    TagManager.initialize(tagManagerArgs)
  }

  handleLogin() {
    const {
      globalUiLoginModal,
      loginUi,
      user,
      userLogout,
    } = this.props
    const isLogged = user.get('logged_in')
    this.closeNav()
    if (isLogged == false) {
      // document.querySelector('.js-focus-visible').classList.remove('show-menu')
      $("#id02").removeClass("show")
      globalUiLoginModal(!loginUi.get('login_modal'))
    } else {
      userLogout()
    }
  }
  
  handleSubscribe(e) {
    this.setState({ email_subscribe: e.target.value })
  }
  handleSubmitSubscribe() {

    const {
      userSubscribe
    } = this.props

    userSubscribe(this.state.email_subscribe)

    this.setState({
      email_subscribe: ''
    })

  }
  handleProfile() {
    document.getElementById('id03').style.display = 'block';
  }

  closeNav() {
    $("body").removeClass("show-menu");
  }

  //fungsi ketika logo di klik
  scrollToTop() {
    scroll.scrollToTop();
  };

  //fungsi whatsapp button
  handleScriptCreate() {
    this.setState({ scriptLoaded: false })
  }
  
  handleScriptError() {
    this.setState({ scriptError: true })
  }
  
  handleScriptLoad() {
    this.setState({ scriptLoaded: true })
  }

  render() {
    const {
      user
    } = this.props

    const isLogged = user.get('logged_in')

    return (
      <div className="home">
        {/* TOP NAV WITH LOGO */}
        <header>
          <nav className={`nav-fixed ${this.state.onTop ? "on-top" : ""}`}>
            <div className="line">
              <div className="s-12 m-3 l-2 div-langgan-logo">
                <Link to={`/`}><img className="s-5 l-12 img-langgan-logo" src={LangganLogo} onClick={this.scrollToTop} alt="Logo Langgan Indonesia" title="Logo Langgan Indonesia" /></Link>
              </div>
              <div className="top-nav s-12 l-10 right">
                <ul className="right">
                  <li>
                    <LinkScroll
                      activeClass="active"
                      onClick={() => document.body.classList.remove("show-menu")}
                      to="second-block"
                      spy={true}
                      smooth={true}
                      offset={-70}
                      duration={1000}
                      className={'menu-tab navBarMargin'}
                    >
                      Benefit
                        </LinkScroll>
                  </li>
                  <li>
                    <LinkScroll
                      onClick={() => document.body.classList.remove("show-menu")}
                      activeClass="active"
                      to="third-block"
                      spy={true}
                      smooth={true}
                      offset={-70}
                      duration={1000}
                      className={'menu-tab navBarMargin'}
                    >
                      Tema
                        </LinkScroll>
                  </li>
                  <li>
                    <LinkScroll
                      onClick={() => document.body.classList.remove("show-menu")}
                      activeClass="active"
                      to="pilihpaket-block"
                      spy={true}
                      smooth={true}
                      offset={-70}
                      duration={1000}
                      className={'menu-tab navBarMargin'}
                    >
                      Harga
                        </LinkScroll>
                  </li>
                  <li>
                    <LinkScroll
                      onClick={() => document.body.classList.remove("show-menu")}
                      activeClass="active"
                      to="fourth-block"
                      spy={true}
                      smooth={true}
                      offset={-70}
                      duration={1000}
                      className={'menu-tab navBarMargin'}
                    >
                      Hubungi Kami
                    </LinkScroll>
                  </li>
                  <li>
                    <Link
                      onClick={() => document.body.classList.remove("show-menu")}
                      activeClass="active"
                      to="fitur-page"
                      className={'menu-tab navBarMargin'}
                    >
                      Fitur
                    </Link>
                  </li>
                  <li>
                    <Link
                      onClick={() => document.body.classList.remove("show-menu")}
                      activeClass="active"
                      to="tampilan-page"
                      className={'menu-tab navBarMargin'}
                    >
                      Tampilan
                    </Link>
                  </li>
                  <li>
                    <a href="http://blog.langgan.id/" target="_blank" className="menu-tab navBarMargin navBarBlogMargin">Blog</a>
                  </li>
                  {
                    isLogged == false
                      ?
                      <li className="vertical-align-center" style={{ height: 50 }}>
                        <div
                          onClick={this.handleLogin}
                          className="menu-tab buttonLoginStyle"
                        >
                          Masuk / Daftar
                        </div>
                      </li>
                      :
                      <Fragment>
                        <li><Link to={`/member-area`} className="menu-tab" style={{ color: '#0B877E', fontWeight: 'bold' }}>Member Area</Link></li>
                        <li><a onClick={this.handleLogin} className="menu-tab" style={{ color: '#0B877E', marginLeft: 21, fontWeight: 'bold' }}>Keluar</a></li>
                      </Fragment>
                  }
                </ul>
              </div>
            </div>
          </nav>
        </header>

        <section className="text-center bg-white">
          {/* FIRST BLOCK */}
          <div id="first-block" className="text-left">
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="left">
                    <h2 className="margin-bottom" style={{ color: '#0B877E', width: '100%'}}>SAATNYA UMKM INDONESIA <br /> PUNYA WEBSITE TOKO ONLINE SENDIRI</h2>
                    <p className="text-justify" style={{width: '100%'}}>Langgan adalah penyedia jasa layanan Website Toko Online yang siap membantu UMKM Indonesia berkembang dengan fitur yang mendukung Bisnis Sobat Langgan</p>
                    <p
                      style={{ fontSize: 15, textAlign: 'center', color: '#0B877E', fontWeight: 'bold', cursor: 'pointer' }}
                      onClick={(e) => this.handlePaketClick()}
                      className="buttonMulaiSekarang"
                    >
                      Mulai Sekarang
                        </p>
                  </article>
                </div>
                <div className="" style={{position:'relative', zIndex:0}}>
                  <Script
                    url="https://widget.tochat.be/bundle.js"
                    onCreate={this.handleScriptCreate.bind(this)}
                    onError={this.handleScriptError.bind(this)}
                    onLoad={this.handleScriptLoad.bind(this)}
                    attributes={{
                      key:"1d3e8f55-968b-46eb-9aea-53301a6512db",
                    }}
                    
                  />
                </div>
              </div>
            </div>
          </div>


          {/* NEXT FIRST BLOCK */}
          <div id="section-next-first-block" className="text-center">
            <h2 className="margin-bottom">Kenapa Harus Punya Toko Online?</h2>
            <div className="center" style={{ flexDirection: 'row', display:'block', margin:'0 auto' }}>
              <div className="div1 bob-on-hover">
                <img className="center" src={require('../styles/img/landingPage/Ceklis.png')} alt="Langgan Indonesia - Toko Online Terpercaya" title="Toko Online Terpercaya" />
                <p>
                  Meningkatkan<br />
                      Kepercayaan<br />
                      Customer
                    </p>
              </div>

              <div className="div2 bob-on-hover">
                <img className="center" src={require('../styles/img/landingPage/shout.png')} alt="Langgan Indonesia - Promosi Toko Online" title="Promosi Toko Online" />
                <p>
                  Menjadi<br />
                      Media Promosi<br />
                      yang Efektif
                    </p>
              </div>

              <div className="div3 bob-on-hover">
                <img className="center" src={require('../styles/img/landingPage/number-one.png')} alt="Langgan Indonesia - Selangkah Lebih Maju" title="Selangkah Lebih Maju" />
                <p>
                  Selangkah<br />
                      Lebih Maju<br />
                      dari Pesaing
                    </p>
              </div>

              <div className="div4 bob-on-hover">
                <img className="center" src={require('../styles/img/landingPage/travel.png')} alt="Langgan Indonesia - Cara Perluas Pasar" title="Cara Perluas Pasar" />
                <p>
                  Dapat Memperluas<br />
                  Pasar Sampai<br />
                  Ke Seluruh Dunia
                </p>
              </div>
            </div>
          </div>

          {/* SECOND BLOCK */}
          <div id="second-block" className="text-left">
            <h2 style={{ color: '#0B877E' }}>Anti Ribet</h2>
            {/* {window.matchMedia('screen and (max-width: 1024px) and (min-width: 500px)').matches && <SecondBlockTablet />} */}
            {!window.matchMedia('screen and (max-width: 1024px) and (min-width: 500px)').matches && <>
              <div className="line">
                <div className="gambarIlustrasiSec2 vertical-align-center">
                  <img className="center" src={require('../styles/img/landingPage/langgan-tinggal-packing-sec2.png')} alt="Langgan Indonesia - Tinggal Packing" title="Tinggal Packing" />
                </div>
                <div className="Sec2Text">
                  <div className="wrapperText" style={{ justifyContent: 'flex-start' }}>
                    <div className="styleLine">
                      <img className="center" src={require('../styles/svg/line-green.svg')} alt="langgan-indonesia-asset-line-green" title="line greeen langgan asset" />
                    </div>
                    <article>
                      <h3 style={{ color: '#0B877E' }}>Tinggal Packing!</h3>
                      <p>
                        Ga perlu lagi kamu cek ongkir barang yang bakal kamu kirim. Terima order, tinggal packing, lalu kirim!
                        </p>
                      <p>
                        Karena Langgan akan memberikan informasi
                        lengkap dan total yang harus dibayarkan
                        pembeli, buat kamu.
                        </p>
                    </article>
                  </div>
                </div>
              </div>

              <div className="line imageRight">
                <div className="Sec2Text">
                  <div className="wrapperText" style={{ justifyContent: 'flex-start' }}>
                    <div className="styleLine">
                      <img className="center" src={require('../styles/svg/line-green.svg')} alt="langgan-indonesia-asset-line-green" title="line greeen langgan asset" />
                    </div>
                    <article>
                      <h3 style={{ color: '#0B877E' }}>Jawab Pertanyaan Makin Praktis</h3>
                      <p>
                        Proses yang biasanya panjang, sekarang kamu cuma tinggal catat alamat pengiriman si pembeli!
                        </p>
                      <p>
                        Sistem siap membantu, ga akan khawatir jawab banyak pertanyaan!
                        Karena perjalanan dan prosedur sampai ke transaksi sangat jelas dan mudah!
                        </p>
                    </article>
                  </div>
                </div>
                <div className="gambarIlustrasiSec2 vertical-align-center">
                  <img className="center" src={require('../styles/svg/jawab-sec2.png')} alt="Langgan Indoensia - Praktis" title="Praktis" />
                </div>
              </div>

              <div className="line">
                <div className="gambarIlustrasiSec2 vertical-align-center">
                  <img className="center" src={require('../styles/img/landingPage/kenali-sec2.png')} alt="Langgan Indonesia - Data Pelangganmu" title="Data Pelangganmu" />
                </div>
                <div className="Sec2Text">
                  <div className="wrapperText" style={{ justifyContent: 'flex-start' }}>
                    <div className="styleLine">
                      <img className="center" src={require('../styles/svg/line-green.svg')} alt="langgan-indonesia-asset-line-green" title="line green langgan asset" />
                    </div>
                    <article>
                      <h3 style={{ color: '#0B877E' }}>Kenali Pelangganmu</h3>
                      <p>
                        Mengumpulkan data itu ga bisa dipisahin sama bisnis kamu! Tapi emang ribet sih kalo harus ngumpulin data satu persatu.
                        </p>
                      <p>
                        Makannya Langgan ngasih kamu sistem yang siap mengumpulkan data sebanyak banyaknya! Kamu bakal lebih unggul kalo punya data pembeli potensial yang kamu simpan.
                        </p>
                    </article>
                  </div>
                </div>
              </div>

              <div className="line imageRight">
                <div className="Sec2Text">
                  <div className="wrapperText" style={{ justifyContent: 'flex-start' }}>
                    <div className="styleLine">
                      <img className="center" src={require('../styles/svg/line-green.svg')} alt="langgan-indonesia-asset-line-green" title="line green langgan asset" />
                    </div>
                    <article>
                      <h3 style={{ color: '#0B877E' }}>Kapanpun</h3>
                      <p>
                        Dengan kamu memiliki toko online, pembeli di tokomu pun akan dipermudah.
                        </p>
                      <p>
                        Apa kamu pernah terpikir bahwa perjalan Si Pembeli di tokomu adalah sesuatu yang penting? Dan sekarang perjalanan di tokomu dari masuk sampai transaksi akan menyenangkan! Kapanpun mereka mau, mereka bisa akses  dan transaksi di tokomu yang online 24 jam.
                        </p>
                    </article>
                  </div>
                </div>
                <div className="gambarIlustrasiSec2 vertical-align-center">
                  <img className="center" src={require('../styles/img/landingPage/kapanpun-sec2.png')} alt="Langgan Indonesia - Kapanpun Cek Toko" title="Kapanpun Cek Toko" />
                </div>
              </div>

              <div className="line">
                <div className="gambarIlustrasiSec2 vertical-align-center">
                  <img className="center" src={require('../styles/img/landingPage/dimanapun-sec2.png')} alt="Langgan Indonesia - Dimanapun bisa belanja" title="Dimanapun bisa belanja" />
                </div>
                <div className="Sec2Text">
                  <div className="wrapperText" style={{ justifyContent: 'flex-start' }}>
                    <div className="styleLine">
                      <img className="center" src={require('../styles/svg/line-green.svg')} alt="langgan-indonesia-asset-line-green" title="line green langgan asset" />
                    </div>
                    <article>
                      <h3 style={{ color: '#0B877E' }}>Dimanapun</h3>
                      <p>
                        Sistem Manajemen Bisnis Langgan bakal bantu kamu dalam memastikan pembayaran para pembeli. Kamu ga usah lagi repot buka m-banking dan cek mutasi, semua sudah ada laporannya disini!
                        </p>
                      <p>
                        Di manapun! kamu bisa cek toko kamu dari smartphone kamu, juga manajemen inventori dan pengelolaan jumlah produkmu, melalui aplikasi mobile Bisnis Kit Langgan.<br />
                        <span style={{ fontSize: 12, fontStyle:'italic' }}>*Integrasi dengan payment gateway sistem</span>
                      </p>
                    </article>
                  </div>
                </div>
              </div>
            </>
            }
          </div>

          {/* THIRD BLOCK */}
          <div id="third-block" className="text-left" style={{display:'block'}}>
            <div className="line">
              <div className="margin-bottom">
                <div className="margin">
                  <article className="left">
                    <h2 style={{ color: '#0B877E' }}>Buat Website Toko Online Mu Menarik</h2>
                    <h2 className="margin-bottom">Banyak Pilihan Tema di Langgan!</h2>
                    <p>
                      Persiapkan dirimu, kamu bakal makin keren dan <br />
                          makin menarik. Dengan tema yang Langgan berikan, <br />
                          customer kamu bisa lebih betah berlama-lama <br />
                          di Toko Online kamu! <br />
                    </p>
                    <div className="text-left">
                      <Link
                        onClick={this.closeNav}
                        className="buttonTelusuriTema"
                        to={`/tema-page`}
                      >
                        Telusuri Tema
                          </Link>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>

          {/* SKEMA BERLANGGANAN BLOCK */}
          <div id="pilihpaket-block" className="text-center">
            <div className="wrapperPembayaran vertical-align-center">
              <div className="containerPembayaran1">
                <div className="contentPembayaran">
                  <div className="wrapperJudulPilihPaket">
                    <h2>Jangka Waktu yang Sesuai</h2>
                    <h2 style={{ color: '#0B877E' }}>Dengan Kebutuhanmu</h2>
                  </div>

                  <div className="wrapperKeteranganPilihPaket">
                    <p>
                      Semua sobat langgan akan mendapatkan fitur yang sama, <br />
                          Kamu hanya perlu pilih jangka waktu yg sesuai dengan kebutuhanmu.
                        </p>
                  </div>

                  <div className="margin wrapperListKeteranganPembayaran vertical-align-center">
                    <div className="content1">
                      <div className="imagePembayaran" />
                      <p>Website Toko Online</p>
                    </div>
                    <div className="content2">
                      <div className="imagePembayaran" />
                      <p>Akses Langgan <br /> Bisniskit</p>
                    </div>
                    <div className="content3">
                      <div className="imagePembayaran" />
                      <p>Gratis Domain atau <br /> Gunakan Domainmu</p>
                    </div>
                  </div>

                </div>
              </div>

              <div className="containerPembayaran2">
                <div className="margin wrapperJangkaWaktu">
                  <p>
                    Pilih Jangka Waktumu
                      </p>

                  <div className="content1">
                    <p style={{fontWeight:600}}>
                      1 Bulan
                    </p>

                    <div className="subContentPembayaran vertical-align-center"  onClick={(e) => this.handlePaketClick(e, 0)}>
                      <div className="sub1">
                        <p style={{fontSize:'20px'}}>
                          Rp. 300.000
                            </p>
                      </div>

                      <div className="sub2 right">
                        <div className="buttonPilih" onClick={(e) => this.handlePaketClick(e, 0)}>
                          <p></p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="content2">
                    <p style={{fontWeight:600}}>
                      6 Bulan
                    </p>

                    <div className="subContentPembayaran vertical-align-center" onClick={(e) => this.handlePaketClick(e, 1)}>
                      <div className="sub1">
                        <p style={{fontSize:'20px'}}>
                          Rp. 1.500.000
                            </p>
                      </div>

                      <div className="sub2 right">
                        <div className="buttonPilih" onClick={(e) => this.handlePaketClick(e, 1)}>
                          <p></p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="content3">
                    <p  style={{marginTop:'3%', fontWeight:600}}>
                      12 Bulan
                        </p>

                    <div className="subContentPembayaran vertical-align-center"  onClick={(e) => this.handlePaketClick(e, 2)}>
                      <div className="sub1">
                        <p style={{fontSize:'20px'}}>
                          Rp. 2.500.000
                            </p>
                      </div>

                      <div className="sub2 right">
                        <div className="buttonPilih" onClick={(e) => this.handlePaketClick(e, 2)}>
                          <p></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          {/* FOURTH BLOCK */}
          <div id="fourth-block" className="text-left" style={{display:'block'}}>
            <div className="line">
              <article className="s-12 m-12 l-5 left">
                <h2 className="margin-bottom">Hubungi Kami</h2>
                <p>
                  Jika kamu mempunyai pertanyaan <br /> saran, keluhan maupun pujian, <br /> kamu dapat menghubungi kami melalui :
                </p>
                <br />
                <br />
                <a href="tel:02287316626"><p><i className="fa fa-phone"></i>(022) 8731 6625</p></a>
                <a href="mailto:hello@langgan.id?Subject=Hello%20Langgan"><p><i className="fa fa-envelope-o"></i>hello@langgan.id</p></a>
                <p><i className="fa fa-map-marker"></i> Jl. Kencana Puspa IV - K47, Kota Bandung</p>
              </article>
            </div>
          </div>

          {/* FIFTH BLOCK */}
          <div id="fifth-block" className="text-center">
            <div className="line">
              <article className="s-12 m-12 l-8 center">
                <h2 className="margin-bottom">Maju Bersama Langgan</h2>
                <p>
                  Maksimalkan fitur yang Langgan berikan
                      <br />Melangkah maju dan tumbuhkan bisnismu bersama Langgan.
                    </p>
                <div className="text-center"><button onClick={this.handleLogin} className="btn-white-new" type="button">Let’s Grow</button></div>
              </article>
            </div>
          </div>
        </section>

        {/* FOOTER */}
        <footer className="footer-desktop">
          <div className="line vertical-align-center">
            <div className="s-12 l-4">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
            <div className="s-12 l-4 text-left">
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
              </div>
              <div className="s-12 l-4 p-l-40 p-t-20">
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
              <div className="s-12 l-4 social-btn-div text-center p-t-20">
                <div className="m-b-20">
                  <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                    <i className="fa fa-twitter"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                    <i className="fa fa-linkedin"></i>
                  </a>
                </div>
                <div className="m-b-20">
                  <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                    <i className="fa fa-youtube"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="s-12 l-4 text-right subscribe">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
              <div className="m-b-10" style={{ marginTop: 10 }}>
                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                  <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', float: 'right', objectFit: 'contain' }} />
                </a> */}

                {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                        <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', float:'right', objectFit:'contain', marginRight:10}}/>
                      </a> */}
              </div>
            </div>
          </div>
          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-12 l-2 text-right">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
        </footer>

        <footer className="footer-mobile">
          <div className="line vertical-align-center">
            <div className="s-12 l-12">
              <p>Langgan</p>
              <p>Jl. Kencana Puspa IV - K47, Cijawura, Buah Batu, Kota Bandung 40287</p>
              <p>(022) 8731 6625 / hello@langgan.id</p>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-4 social-btn-div text-center p-t-20">
              <div className="m-b-20">
                <a href="https://www.facebook.com/LangganIndonesia" target="_blank" className="btn">
                  <i className="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/langgan_id" target="_blank" className="btn">
                  <i className="fa fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/langgan.id" target="_blank" className="btn">
                  <i className="fa fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/20469117" target="_blank" className="btn">
                  <i className="fa fa-linkedin"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCnfCNh_TNpH7RFmAJ5m86VQ" target="_blank" className="btn">
                  <i className="fa fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>

          <div className="line vertical-align-center m-t-10">
            <div className="s-12 l-12 text-center" style={{ display: "flex" }}>
              <div className="s-6 l-2" style={{ marginLeft: "auto", paddingRight: "45px" }}>
                <div className="m-b-20"><Link to={`/faq`}>FAQ</Link></div>
                <div className="m-b-20"><Link to={`/tema-page`}>Tema</Link></div>
              </div>
              <div className="s-6 l-2" style={{ marginRight: "auto", paddingLeft: "45px" }}>
                <div className="m-b-20"><a href="https://www.facebook.com/LangganIndonesia" target="_blank">Komunitas</a></div>
                {/*<div className="m-b-20"><a href="/karir">Karir</a></div>*/}
              </div>
            </div>
          </div>
          <div className="line vertical-align-center">
            <div className="s-12 l-12 text-left subscribe-mobile">
              <div className="m-b-10">Subscribe untuk mengikuti berita paling mutakhir</div>
              <div><input type="email" placeholder="Masukan Email Kamu" className="input-email-subscribe" onChange={(e) => this.handleSubscribe(e)} value={this.state.email_subscribe} /><button onClick={this.handleSubmitSubscribe} style={{ backgroundColor: '#15BFAE' }} className="btn-subscribe">Subscribe</button></div>
            </div>
          </div>

          <div className="line vertical-align-center">
            <div className="s-12 l-12 subscribe-mobile">
              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                <img alt='Langgan Mobile Apps - Get it on Google Play' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/1280px-Google_Play_Store_badge_EN.svg.png' style={{ width: '30%', height: '15%', objectFit: 'contain', float: 'right', marginRight: '20%' }} />
              </a> */}

              {/* <a href='https://play.google.com/store/apps/details?id=com.langgan_bisnis_kit&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                      <img alt='Langgan Mobile Apps - Get it on App Store' title="Langgan Mobile Apps" src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/1280px-Download_on_the_App_Store_Badge.svg.png' style={{width:'30%', height:'15%', bjectFit:'contain', marginRight:10, float:'right'}}/>
                    </a> */}
            </div>
          </div>

          <div className="line"><hr /></div>
          <div className="line copyright">
            <div className="s-4 l-2 text-left">
              <Link to={`/syarat-ketentuan`}>Syarat & Ketentuan</Link>
            </div>
            <div className="s-4 l-2 text-left">
              <Link to={`/privacy-policy`}>Kebijakan Privasi</Link>
            </div>
          </div>
          <div className="line copyright">
            <div className="s-12 l-8">
              <p>© 2020 PT. Berlangganan Indonesia Global</p>
            </div>
          </div>
        </footer>
      </div>
    );
  }

}




const mapStateToProps = (state, ownProps) => {
  return {
    loginUi: state.loginUi,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalUiLoginModal: (modal) => dispatch(globalUiLoginModal(modal)),
    userLogout: () => dispatch(userLogout()),
    userSubscribe: (email) => dispatch(userSubscribe(email)),
    indexAddSelectedPaket: (params) => dispatch(indexAddSelectedPaket(params)),
    redirectMemberPaket: () => dispatch(push('/member-paket'))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LangganHome))



// function SecondBlockTablet() {

//   const CarouselItem = (props) => {
//     const { data } = props;
//     return (<div className="cs-item">
//       <img className="center" src={require(`../styles/${data.img}`)} />
//       <h3>{data.title}</h3>
//       <p className="desc">
//         {data.desc}
//       </p>
//     </div>)
//   }

//   const settings = {
//     dots: false,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 2,
//     slidesToScroll: 2,
//   };
//   return (
//     <>
//       <div className="cr-wrapper">
//         <Slider {...settings}>
//           <CarouselItem data={{
//             img: "img/landingPage/langgan-tinggal-packing-sec2.png",
//             title: "Tinggal Packing!",
//             desc: <>Ga perlu lagi kamu cek ongkir barang yang bakal kamu kirim. Terima order, tinggal packing, lalu kirim!<br />
//                 Karena Langgan akan memberikan informasi lengkap dan total yang harus dibayarkan pembeli, buat kamu.</>,
//           }} />
//           <CarouselItem data={{
//             img: "svg/jawab-sec2.png",
//             title: "Jawab Pertanyaan Makin Praktis",
//             desc: <>Proses yang biasanya panjang, sekarang kamu cuma tinggal catat alamat pengiriman si pembeli!<br />
//           Sistem siap membantu, ga akan khawatir jawab banyak pertanyaan! Karena perjalanan dan prosedur sampai ke transaksi sangat jelas dan mudah!</>,
//           }} />
//           <CarouselItem data={{
//             img: "img/landingPage/kenali-sec2.png",
//             title: "Kenali Pelangganmu",
//             desc: <>Mengumpulkan data itu ga bisa dipisahin sama bisnis kamu! Tapi emang ribet sih kalo harus ngumpulin data satu persatu.<br />
//           Makannya Langgan ngasih kamu sistem yang siap mengumpulkan data sebanyak banyaknya! Kamu bakal lebih unggul kalo punya data pembeli potensial yang kamu simpan</>,
//           }} />
//           <CarouselItem data={{
//             img: "img/landingPage/kapanpun-sec2.png",
//             title: "Kapanpun",
//             desc: <>Dengan kamu memiliki toko online, pembeli di tokomu pun akan dipermudah<br />
//           Apa kamu pernah terpikir bahwa perjalan Si Pembeli di tokomu adalah sesuatu yang penting? Dan sekarang perjalanan di tokomu dari masuk sampai transaksi akan menyenangkan! Kapanpun mereka mau, mereka bisa akses dan transaksi di tokomu yang online 24 jam</>,
//           }} />
//           <CarouselItem data={{
//             img: "img/landingPage/dimanapun-sec2.svg",
//             title: "Dimanapun",
//             desc: <>Sistem Manajemen Bisnis Langgan bakal bantu kamu dalam memastikan pembayaran para pembeli. Kamu ga usah lagi repot buka m-banking dan cek mutasi, semua sudah ada laporannya disini!<br />
//           Di manapun! kamu bisa cek toko kamu dari smartphone kamu, juga manajemen inventori dan pengelolaan jumlah produkmu, melalui aplikasi mobile Bisnis Kit Langgan.
//          <small>*Integrasi dengan payment gateway sistem</small></>,
//           }} />
//         </Slider>
//       </div>
//       <div className="st-bg">
//         <svg width="100%" height="720px" viewBox="0 0 768 597" version="1.1" xlinkHref="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
//           <defs>
//             <path d="M1.19371e-12 -2.02905e-13C1.19371e-12 -2.02905e-13 42.6667 39.4284 70.675 39.4284C107.282 39.4284 119.631 94.5084 136.75 96.7112C184.922 102.91 221.04 66.0245 243.087 70.0868C317.51 83.7991 328.129 116.651 357.558 123.199C400.083 132.66 459.021 105.604 491.563 105.604C565.833 105.604 563.365 159.2 630 159.2C673.875 160.276 700.246 115.911 727.483 105.604C754.721 95.2971 768 105.604 768 105.604L768 582.823C768 582.823 357.558 582.823 331.854 582.823C306.149 582.823 250.939 563.56 211.2 566.447C171.461 569.335 177.189 597 136.75 597C96.3105 597 85.9083 543.415 50.4 543.415C14.8917 543.415 0 597 0 597L1.19371e-12 -2.02905e-13Z" id="path_1" />
//             <clipPath id="mask_1">
//               <use xlinkHref="#path_1" />
//             </clipPath>
//           </defs>
//           <path d="M1.19371e-12 -2.02905e-13C1.19371e-12 -2.02905e-13 42.6667 39.4284 70.675 39.4284C107.282 39.4284 119.631 94.5084 136.75 96.7112C184.922 102.91 221.04 66.0245 243.087 70.0868C317.51 83.7991 328.129 116.651 357.558 123.199C400.083 132.66 459.021 105.604 491.563 105.604C565.833 105.604 563.365 159.2 630 159.2C673.875 160.276 700.246 115.911 727.483 105.604C754.721 95.2971 768 105.604 768 105.604L768 582.823C768 582.823 357.558 582.823 331.854 582.823C306.149 582.823 250.939 563.56 211.2 566.447C171.461 569.335 177.189 597 136.75 597C96.3105 597 85.9083 543.415 50.4 543.415C14.8917 543.415 0 597 0 597L1.19371e-12 -2.02905e-13Z" id="Rectangle" fill="#FFFFFF" stroke="none" />
//         </svg>
//       </div>
//     </>
//   )
// } 