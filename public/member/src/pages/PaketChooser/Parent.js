/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'
import bindAll from 'lodash/bindAll'
import isEmpty from 'lodash/isEmpty'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

//library facebook pixel
import ReactPixel from 'react-facebook-pixel';

import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import Card from '../components/Card'
import Stepper from '../components/Stepper'

import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import Step4 from './Step4'
import Step5 from './Step5'
import _axios from '../../dependencies/_axios'
import store from '../../helpers/user_session'

import couponImage from '../../styles/img/ilus-coupon-15.png';
import Notifications from 'react-notification-system-redux';
import {
  getTema,
} from '../../actions/tema'

import {
  paymentToko,
  getKupon,
  kuponAddDataSuccess
} from '../../actions/payment'

const step = [
  'Pilih Skema',
  'Nama Toko',
  'Nama Domain',
  'Pilih Tema',
  'Checkout'
]

class Parent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 1,
      paket: null,
      nama_toko: '',
      deskrpisi_toko: '',
      nama_domain: '',
      tld: '',
      tema: null,
      checkout: false,
      ableToNext: false,
      show_kupon: 0,
      kupon: '',
      kupon_error: '',
      kupon_price: 0,
      step_domain: false,
      kupon_code: ''
    }

    bindAll(this, [
      'nextOrFinish',
      'handleGunakan',
      'handleLewati',
      'handleKuponChange',
      'handleStepDomain'
    ])
  }

  componentDidMount() {
    const {
      getTema,
      indexPaketChooser,
    } = this.props

    const options = {
      autoConfig: true, // set pixel's autoConfig
      debug: false, // enable logs
    };
    ReactPixel.init('490052041836692', options);

    if (!indexPaketChooser.isEmpty()) {

      this.setState({ paket: indexPaketChooser.toJS(), ableToNext: true })
    }
    getTema(this.props.temaFilterData)
  }

  selectPaket = (paket) => {
    // ReactPixel.track('track', 'PageView');
    ReactPixel.track('track', 'AddToCart');
    
    this.setState({ paket: paket, ableToNext: true })
  }
  handleStepDomain() {
    this.setState({ step_domain: true })
  }
  handleGunakan() {
    const {
      kupon_code
    } = this.props
    _axios({
      url: `/api/kupon/detail`,
      timeout: 20000,
      method: 'post',
      headers: {
        'Authorization': `Bearer ${store.get()}`
      },
      data: { code: this.state.kupon },
      responseType: 'json'
    })
      .then((response) => {
        var kupon = [];
        kupon['code'] = response.data.data.code
        kupon['amount'] = response.data.data.amount
        this.setState({ show_kupon: 0, kupon_price: response.data.data.amount, kupon_code: response.data.data.code })
        this.setState({ step: this.state.step + 1 }, () => {
          const index = ['paket', 'nama_toko', 'nama_domain', 'tema']
          if (this.state[index[this.state.step - 1]]) {
            this.setState({ ableToNext: true })
          } else {
            if (this.state.step === step.length) {
              this.setState({ ableToNext: true })
            } else {
              this.setState({ ableToNext: false })
            }
          }
        })
        kuponAddDataSuccess(kupon)
      })
      .catch((error) => {
        this.setState({ kupon_error: error.response.data.messages })
      })
  }
  handleLewati() {
    this.setState({ show_kupon: 0 })
    this.setState({ step: this.state.step + 1 }, () => {
      const index = ['paket', 'nama_toko', 'nama_domain', 'tema']
      if (this.state[index[this.state.step - 1]]) {
        this.setState({ ableToNext: true })
      } else {
        if (this.state.step === step.length) {
          this.setState({ ableToNext: true })
        } else {
          this.setState({ ableToNext: false })
        }
      }
    })
  }
  handleKuponChange(e) {
    var code_kupon = e.target.value
    this.setState({ kupon: code_kupon, kupon_error: '' })
  }
  changeNamaToko = (nama_toko) => {
    if (nama_toko) {
      this.setState({ nama_toko, ableToNext: true })
    } else {
      this.setState({ nama_toko, ableToNext: false })
    }
  }
  changeDeskripsiToko = (deskrpisi) => {
    if (deskrpisi) {
      this.setState({ deskripsi_toko: deskrpisi, ableToNext: true })
    } else {
      this.setState({ deskripsi_toko: deskrpisi, ableToNext: false })
    }
  }
  changeDomain = (nama_domain) => {
    if (nama_domain) {
      this.setState({ nama_domain, ableToNext: true })
    } else {
      this.setState({ nama_domain, ableToNext: false })
    }
  }
  changeTema = (tema) => {
    this.setState({ tema: tema, ableToNext: true })
  }
  prev = () => {
    if (this.state.step_domain == true) {
      this.setState({ step_domain: false })
    } else {
      this.setState({ step: this.state.step - 1 }, () => {
        const index = ['paket', 'nama_toko', 'nama_domain', 'tema']
        const curr = this.state[index[this.state.step - 1]]
        if (curr !== '' && curr !== null) {
          this.setState({ ableToNext: true })
        }
      })
    }
  }
  nextOrFinish = () => {
    const {
      paymentToko,
      userInformation,
      kupon_code
    } = this.props

    if (this.state.step == 1) {
      this.setState({ show_kupon: 1 })
    } else if (this.state.step !== step.length) {
      this.setState({ step: this.state.step + 1 }, () => {
        const index = ['paket', 'nama_toko', 'nama_domain', 'tema']
        if (this.state[index[this.state.step - 1]]) {
          this.setState({ ableToNext: true })
        } else {
          if (this.state.step === step.length) {
            this.setState({ ableToNext: true })
          } else {
            this.setState({ ableToNext: false })
          }

        }
      })
    } else {
      //last state tertrigger

      let arrayData = {}
      let dataKupon = this.state.kupon_price

      arrayData['id_user'] = userInformation.get('id')
      arrayData['id_paket'] = this.state.paket.id
      arrayData['domain'] = this.state.nama_domain
      arrayData['nominal'] = this.state.paket.pricing
      arrayData['nama_toko'] = this.state.nama_toko
      arrayData['id_tema'] = this.state.tema.id
      if (this.state.kupon_code != '') {
        arrayData['code_kupon'] = this.state.kupon_code
      }
      console.log(kupon_code.toJS());
      console.log(arrayData);
      // console.log(arrayData) 
      paymentToko({ arrayData, dataKupon, statusPembayaran:'Buka Toko' })
    }
  }
  render() {
    const { tema } = this.props

    return (
      <div className='home'>
        <Navbar />
        <section className='text-center force-bottom-bg'>
          <div id="tema-detail-block" style={{ paddingTop: "120px" }}>
            <div className="line">
              <div className="margin-bottom">
                <div className="margin" style={{ display: 'flex', justifyContent: 'center' }}>
                  <Card style={{ boxShadow: '1px 1px 3px 0 rgba(0,0,0,0.5)' }}>
                    <Stepper active={this.state.step} style={{ marginTop: 10 }} items={step} />
                    <Wrapper active={this.state.step}>
                      {this.state.step === 1 && (
                        <Step1
                          currentPaket={this.state.paket}
                          selectPaket={(i) => this.selectPaket(i)} />
                      )}
                      {this.state.step === 2 && (
                        <Step2
                          changeNama={this.changeNamaToko}
                          changeDeskripsi={this.changeDeskripsiToko}
                          value={this.state.nama_toko}
                          deskripsi={this.state.deskripsi_toko} />
                      )}
                      {this.state.step === 3 && (
                        <Step3
                          currentPaket={this.state.paket}
                          currentDomain={this.state.nama_domain}
                          currentTLD={this.state.tld}
                          changeTLD={tld => this.setState({ tld })}
                          selectedDomain={this.changeDomain}
                          step_domain={this.state.step_domain}
                          handleStepDomain={this.handleStepDomain}
                        />
                      )}
                      {this.state.step === 4 && (
                        <Step4
                          currentTheme={this.state.tema}
                          selectedTheme={this.changeTema}
                          themaList={tema}
                        />
                      )}
                      {this.state.step === step.length && (
                        <Step5
                          price={this.state.paket.pricing}
                          currentPaket={this.state.paket}
                          currentTheme={this.state.tema}
                          currentToko={this.state.nama_toko}
                          currentDomain={this.state.nama_domain}
                          currentKupon={this.state.kupon_price}
                        />
                      )}
                    </Wrapper>
                    <div className='flex-between p-20' style={{ display: "flex", flexFlow: "row wrap" }}>
                      <div>
                        {this.state.step !== 1 &&
                          <button onClick={this.prev} className='btn-white shadow-button color-langgan' style={{ border: '0px', boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)', width: 130 }}>
                            Kembali
                          </button>
                        }
                      </div>
                      <div>
                        <Button
                          className='btn-green shadow-button'
                          onClick={this.nextOrFinish}
                          disabled={!this.state.ableToNext}
                          style={{ backgroundColor: "#15bfae", boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)', width: 130 }}>
                          {this.state.step === step.length ? 'Checkout' : 'Selanjutnya'}
                        </Button>
                      </div>
                    </div>
                  </Card>
                </div>
              </div>
            </div>
          </div>
          <div id="#modal_kupon" className={`modal kupon center ${this.state.show_kupon == 1 ? "show" : ''}`} >
            <div className="modal-content-kupon animate" ref={(el) => { this.loginModal = el }}>
              <div className="div-form-profile">
                <label style={{ fontSize: '18px', color: '#000', textAlign: 'center' }}>Silahkan masukan kode voucher untuk mendapatkan harga yang lebih murah</label>
                <form>
                  <div className="form-group" style={{ textAlign: 'center' }}>
                    <img src={couponImage} style={{ margin: 'auto' }} />
                    <input type="text" name='name_val' style={{ width: '60%', margin: 'auto' }} onChange={(e) => this.handleKuponChange(e)} className="form-control" />
                    <span className="text-danger" style={{ margin: 'auto' }} >{this.state.kupon_error}</span>
                  </div>
                </form>
              </div>
              <div className="div-login">
                <div className="row" style={{ display: "flex", justifyContent: "center" }}>
                  <div className="col-sm-4 col-md-3"><button className="btn-white shadow-button" style={
                    {
                      background: this.state.kupon != '' ? '#15bfae' : '#919191',
                      color: '#fff',
                      border: '0px',
                      boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)',
                      width: 100
                    }} onClick={this.handleGunakan} type="submit">Gunakan</button></div>
                  <div className="col-sm-4 col-md-3">
                    <button onClick={this.handleLewati} className="btn-white shadow-button color-langgan" style={{ border: '0px', boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }} type="submit">
                      Lewati
                        </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}

const Wrapper = styled.div`
  min-height:310px;
  display:${p => p.active === 5 ? '' : 'flex'};
  flex-direction:${p => p.active === 5 ? '' : 'column'};
  align-items:${p => p.active === 5 ? '' : 'center'};
`

const Button = styled.button`
  background-color:${p => p.disabled ? 'rgba(0,0,0,0.2)' : '#15bfae'} !important;
`

const mapStateToProps = (state, ownProps) => {
  return {
    tema: state.tema,
    userInformation: state.userInformation,
    kupon_code: state.kupon_code,
    indexPaketChooser: state.indexPaketChooser,
    temaFilterData: state.temaFilterData,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTema: (tema) => dispatch(getTema(tema)),
    paymentToko: (data) => dispatch(paymentToko(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Parent))