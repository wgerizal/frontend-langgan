/* eslint-disable */
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Pagination from '../components/Pagination'
import bindAll from 'lodash/bindAll'
import Carousel from '../components/Carousel'
import {
  getTema,
  temaAddFilterDataPage,
  temaAddFilterDataPerPage,
  temaAddFilterDataFilter,
} from '../../actions/tema'

import { Prompt } from 'react-router'

var shouldBlockNavigation;


class Step4 extends Component {
  constructor(props) {
    super(props)
    bindAll(this, [
      'handleModal',
      'handleChangeInput',
      'handleChooseTema',
      'handleSubmitSearch',
    ])

    this.state = {
      more_tema: false,
      tema_val: ''
    }
  }

  componentDidMount() {
    window.onbeforeunload = function (e) {
      e = e || window.event;
      if (e) {
        e.returnValue = 'Sure?';
      }
      return 'Sure?';
    };
  }

  componentDidUpdate = () => {
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true
    } else {
      window.onbeforeunload = undefined
    }
  }
  // componentDidMount() {
  //   // getTema(this.props.temaFilterData) 
  // }
  handleChangeInput(e) {
    const {
      getTema
    } = this.props
    this.setState({ [e.target.name]: e.target.value })
  }
  handleChooseTema(tema) {
    this.setState({ more_tema: !this.state.more_tema })
    this.props.selectedTheme(tema)
  }
  handleSubmitSearch(e) {
    const {
      getTema,
      temaAddFilterDataFilter,
      temaFilterData,
    } = this.props
    temaAddFilterDataFilter(this.state.tema_val)
      .then(() => {
        getTema(temaFilterData)
      })
  }
  handleModal() {
    this.setState({ more_tema: !this.state.more_tema })
  }
  render() {
    const {
      tema,
      temaFilterData,
      temaPagination,
      getTema,
      temaAddFilterDataPage,
      temaAddFilterDataPerPage,
    } = this.props

    const temaContentReducer = []

    tema.map((tems, index) =>
      temaContentReducer.push(<div onClick={(e) => { this.handleChooseTema(tems) }} className="wrapper-image col-md-3 mar-bot-40"><img key={tems.id} description={tems.description} style={{ width: '300px', height: '140px' }} className="img-tema-modal" src={tems.thumbnail} /><div className="label-tema">{tems.name}</div></div>)
    )

    let temaPageContent;

    temaPageContent = temaContentReducer

    // function untuk block halaman ketika close tab
    window.addEventListener("beforeunload", (ev) => {  
      ev.preventDefault();
      return ev.returnValue = 'Are you sure you want to close?';
    });
  
    return (
      <Fragment>
        <Prompt
          when={shouldBlockNavigation}
          message='You have unsaved changes, are you sure you want to leave?'
        />
        <Carousel currentTheme={this.props.currentTheme} themaList={this.props.themaList} selectedTheme={this.props.selectedTheme} />
        {/* <a className="default-a" onClick={this.handleModal}>Tampilkan Lebih Banyak</a> */}
        <div id="id03" className={`modal ${this.state.more_tema == true ? "show" : ''}`} >
          <div className="modal-content-tema animate" style={{ overflow: 'auto' }}>
            <div className="div-tema-content">
              <button className="button-close pull-right" onClick={this.handleModal}></button>
              <article className="s-12 m-12 l-7 center">
                <div><input type='text' className="form-control" style={{ width: '30%', display: 'inline', boxShadow: '0px 0px 7px 1px rgba(0, 0, 0, 0.4)' }} name='tema_val' value={this.state.tema_val} onChange={this.handleChangeInput} placeholder='Ketik nama tema disini...' /><button className="btn-cari" style={{ backgroundColor: "#15bfae", boxShadow: '0px 0px 7px 1px rgba(0, 0, 0, 0.4)' }} onClick={this.handleSubmitSearch} type="submit">Cari</button></div>
              </article>
            </div>
            <div id="tema-block" className="text-left">
              <div className="line">
                {temaPageContent}
              </div>
              {/* <Pagination
                    pagination={temaPagination}
                    addPageNumber={temaAddFilterDataPage}
                    addPerPageSize={temaAddFilterDataPerPage}
                    fetchData={getTema}
                    filterData={temaFilterData}
                    perPageSize={''} /> */}
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    tema: state.tema,
    temaFilterData: state.temaFilterData,
    temaPagination: state.temaPagination,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTema: (tema) => dispatch(getTema(tema)),
    temaAddFilterDataPage: (page) => dispatch(temaAddFilterDataPage(page)),
    temaAddFilterDataPerPage: (per_page) => dispatch(temaAddFilterDataPerPage(per_page)),
    temaAddFilterDataFilter: (filter) => dispatch(temaAddFilterDataFilter(filter)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Step4))

