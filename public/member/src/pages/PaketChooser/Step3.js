/* eslint-disable */
import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import Notifications from 'react-notification-system-redux'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import compose from 'recompose/compose'
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router'
import { Prompt } from 'react-router'

var shouldBlockNavigation;

const providedDomain = [
  '.com',
  '.id',
  '.web.id',
  '.net',
  '.my.id',
  '.co.id',
  '.biz.id',
]
const useStyles = theme => ({
  root: {
    width: '90%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
});

function getSteps() {
  return ['Ketentuan Migrasi Domain', 'Persiapan Migrasi Domain', 'Proses Migrasi Domain'];
}
function getStepContent(step) {
  switch (step) {
    case 0:
      return `<p>Nama domain hanya dapat dimigrasi apabila memenuhi persyaratan berikut:</p>
              <p>1. Umur domain minimal 60 hari dari tanggal registrasi atau tanggal migrasi.</p>
              <p>2. Domain belum expired.</p>
              <p>3. Sisa masa aktif domain</p>
              <p>4. Pastikan alamat email Anda tercantum di informasi WHOIS domain. Email persetujuan migrasi domain akan dikirim ke alamat email yang terdapat di informasi WHOIS domain.</p>
              <p>5. Tempat membeli nama domain sebelumnya mengizinkan domain dimigrasi, tidak terikat perjanjian.</p>`
    case 1:
      return `<p>Sebelum melakukan migrasi domain Anda harus memastikan beberapa hal:</p>
              <p>1. Nama domain tidak dalam keadaan terkunci (LOCK). Status domain harus OK (bisa dimigrasi).</p>
              <p>2. ID Protection atau Whois Protection telah dinon-aktifkan.</p>
              <p>3. Mengetahui EPP CODE atau Transfer Secret atau Domain Secret. Kode ini dibutuhkan untuk migrasi domain.</p>
              <p>4. Ketiga hal di atas harus diminta dari tempat lama Anda membeli domain. Poin kesatu dan kedua dapat diperiksa di website whois.ws</p>`
    case 2:
      return `<p>Setelah unlock nama domain, menon-aktifkan ID Protection dan mendapatkan EPP CODE maka lakukan migrasi domain di tahap ini. Berikut ini proses migrasi domain:</p>
              <p>1. Masukkan alamat domain yang akan dimigrasi</p>
              <p>2. Masukkan kode EPP CODE domain.</p>
              <p>3. Lakukan pembayaran dan konfirmasi pembayaran.</p>
              <p>4. Kami akan memeriksa pembayaran dan memproses migrasi domain.</p>
              <p>5. Anda akan menerima email yang berisi link konfirmasi transfer domain. Email ini dikirim ke alamat email yang terdapat di informasi WHOIS domain.</p>
              <p>6. Setelah link persetujuan transfer diklik, proses migrasi dimulai dan butuh waktu maksimal 7 hari.</p>
              <p>7. Kami akan menghubungi Anda melalui email apabila proses telah transfer berhasil atau gagal.</p>
              <p>8. Kami akan memberikan subdomain sementara untuk toko anda ketika sudah menyelesaikan pembayaran.</p>
              <p>9. Subdomain akan otomatis berubah menjadi domain ketika proses migrasi domain telah berhasil.</p>
              <p>10. Masukkan Nameservers Langgan di website domain anda yang sebelumnya berlangganan :</p>`
    default:
      return 'Unknown step';
  }
}
class Step3 extends Component {
  componentDidMount() {
    window.onbeforeunload = function (e) {
      e = e || window.event;
      if (e) {
        e.returnValue = 'Sure?';
      }
      return 'Sure?';
    };
  }

  componentDidUpdate = () => {
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true
    } else {
      window.onbeforeunload = undefined
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      inputDomain: this.props.currentDomain ? this.props.currentDomain.split('.')[0] : '',
      finalDomain: this.props.currentDomain ? this.props.currentDomain : '',
      checkButton: true,
      isAvail: this.props.currentDomain ? true : '',
      indexDomain: 0,
      activeStep: 0,
      paket: this.props.currentPaket
    }
    this.domainName = React.createRef()
    this.selectedDomain = React.createRef()
    bindAll(this, [
      'fakeCheck',
      'handleNext',
      'handleBack',
      'handleReset'
    ])
  }


  fakeCheck = (req, isId) => {
    const {
      notifError,
    } = this.props
    return new Promise((res, rej) => {
      axios({
        url: 'https://api.irsfa.id/oauth/token',
        timeout: 20000,
        method: 'post',
        data: {
          grant_type: 'client_credentials',
          client_id: '0cc80556-338a-428a-b81e-c6edc4410b3e',
          client_secret: 'XuEe8eHXZD031cbyFFqWXhZqE6eTxRsO89dDMXNR'
        },
        responseType: 'json',
      })
        .then((response) => {
          let token = response.data.access_token


          axios({
            url: `https://api.irsfa.id/rest/v2/domain/check/availability`,
            timeout: 20000,
            method: 'post',
            headers: {
              'Authorization': `Bearer ${token}`,
              'X-Requested-With': `XMLHttpRequest`
            },
            data: { domain: req },
            responseType: 'json'
          })
            .then((response) => {
              let data = response.data.data
              console.log(response)
              if (isId) {
                data.map((dt, index) => {
                  if (dt.status == 'Domain Exist') {
                    res(false)
                  }
                })
              } else {
                data.map((dt, index) => {
                  if (dt.status == 'active' || dt.status == 'Domain Exist') {
                    res(false)
                  }
                })
              }
              res(data)
            })
            .catch((error) => {
              notifError({
                title: 'Error',
                message: error.response.data.message,
                position: 'bl'
              })
              res(false)

            })

        })


    })
  }

  changeTLD = (indexDomain) => {
    this.setState({ indexDomain })
    this.props.changeTLD(indexDomain)
  }
  checkDomain = async () => {
    let isId = false;
    const domain = `${this.domainName.current.value}${providedDomain[this.selectedDomain.current.value]}`

    this.setState({ checkButton: false, isAvail: '' })

    if (this.selectedDomain.current.value == 1) {
      isId = true
    }

    const domainData = await this.fakeCheck(domain, isId)
    var isAvail;

    if (domainData == false) {
      isAvail = false
    } else {
      isAvail = true
    }
    this.setState({ finalDomain: domain, isAvail, checkButton: true })
    if (isAvail) {
      this.props.selectedDomain(domain)
    } else {
      this.props.selectedDomain(null)
    }
  }

  handleNext() {
    this.setState({
      activeStep: this.state.activeStep + 1
    });
  }
  handleBack() {
    this.setState({
      activeStep: this.state.activeStep - 1
    });
  }
  handleReset() {
    this.setState({ activeStep: 0 });
  }
  render() {
        // function untuk block halaman ketika close tab
        window.addEventListener("beforeunload", (ev) => {  
          ev.preventDefault();
          return ev.returnValue = 'Are you sure you want to close?';
      });
  
    
    const { handleStepDomain, step_domain, classes } = this.props
    const linkWhatsapp = 'https://api.whatsapp.com/send?phone=628112484440&text=%20Hello%20Technical%20Support%20saya%20sudah%20mempunyai%20domain%20sebelumnya,%20bagaimana%20cara%20dimigrasikan%20ke%20Langgan'

    const steps = getSteps();

    let view;
    if (step_domain == false) {
      view = <Fragment>
        <h3 style={{ paddingRight: "5px", paddingLeft: "5px", marginBottom: "10px", marginTop: "30px" }}>Silahkan masukkan nama domain yang kamu inginkan</h3>
        <DomainInputWrapper>
          <DomainInput
            ref={this.domainName}
            type='text'
            placeholder='Ketik domain mu disini'
            onChange={e => this.setState({ inputDomain: e.target.value.toLowerCase() })}
            style={{ boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }}
            value={this.state.inputDomain.toLowerCase()} />
          <DomainSelect
            ref={this.selectedDomain} onChange={e => this.changeTLD(e.target.value)} defaultValue={this.props.currentTLD} style={{ boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }}>
            {providedDomain.map((v, i) => (<option key={i.toString()} value={i}>{v}</option>))}
          </DomainSelect>
          <ButtonStep3 onClick={this.checkDomain} disabled={this.state.inputDomain ? (!this.state.checkButton ? true : false) : true} style={{ boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.5)' }}>{this.state.checkButton ? 'Cek' : 'Memproses'}</ButtonStep3>
          {this.state.isAvail === true && (
            <AvailAlertWrap style={{ boxShadow: '1px 1px 3px 0 rgba(0,0,0,0.5)', borderTopLeftRadius: '10px', borderTopRightRadius: '10px' }}>
              <AvailAlertTitle avail color={this.state.paket.color}>Domain Tersedia</AvailAlertTitle>
              <AvailAlertBody>
                <div>{this.state.finalDomain}</div>
                <div>Domain ini dapat digunakan selama skema berlangganan berlaku</div>
              </AvailAlertBody>
            </AvailAlertWrap>
          )}

          {this.state.isAvail === false && (
            <AvailAlertWrap>
              <AvailAlertTitle>Domain Tidak Tersedia</AvailAlertTitle>
              <AvailAlertBody>
                <div>{this.state.finalDomain}</div>
                <div>Silahkan pilih domain lain</div>
              </AvailAlertBody>
            </AvailAlertWrap>
          )}
        </DomainInputWrapper>
        <div style={{ marginTop: 'auto' }}>Sudah punya domain sebelumnya? Silahkan <a onClick={handleStepDomain}>hubungi kami</a> agar langsung dimigrasi</div>
        {/* <div style={{marginTop: 'auto'}}>Sudah punya domain sebelumnya? Silahkan <a href={linkWhatsapp} target={'_blank'} style={{color:'#15BFAE'}}>hubungi kami</a> agar langsung dimigrasi</div> */}
      </Fragment>
    } else {
      view = <div className={classes.root}>
        <Stepper activeStep={this.state.activeStep} orientation="vertical">
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                <Typography><div dangerouslySetInnerHTML={{ __html: getStepContent(index) }} /></Typography>
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={this.state.activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      Back
                  </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleNext}
                      className={classes.button}
                    >
                      {this.state.activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {this.activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button onClick={this.handleReset} className={classes.button}>
              Reset
          </Button>
          </Paper>
        )}
      </div>
    }
    return (
      <Fragment>
        <Prompt
          when={shouldBlockNavigation}
          message='You have unsaved changes, are you sure you want to leave?'
        />
        
        {view}
      </Fragment>
    )
  }
}

const DomainInputWrapper = styled.div`
  margin-top:10px;
  margin-bottom:10px;
  width:70%;
`

const DomainInput = styled.input`
  border-radius: 5px;
  box-shadow: 1px 1px 4px 0 rgba(0, 0, 0, 0.5);
  border: 1px solid #DFDFDF;
  height: 34px;
  padding: 6px 12px;
  font-size: 16px;
  line-height: 1.42857143;
  color: #555;
  margin-bottom:10px;
`

const DomainSelect = styled.select`
  border-radius: 5px;
  box-shadow: 1px 1px 4px 0 rgba(0, 0, 0, 0.5);
  border: 1px solid #DFDFDF;
  height: 34px;
  padding: 6px 12px;
  font-size: 16px;
  line-height: 1.42857143;
  color: #15bfae;
  margin-left:5px;
  margin-right:5px;
`

const AvailAlertWrap = styled.div`
  display:flex;
  flex:1;
  flex-direction:column;
  justify-content:center;
  align-items:center;
  width:100%;
  box-shadow: 4px 4px 10px 0 rgba(0, 0, 0, 0.5);
  margin-top:20px;
`

const AvailAlertTitle = styled.div`
  background-color:${p => p.avail ? p.color : '#ad2121'};
  width:100%;
  border-top-right-radius:3px;
  border-top-left-radius:3px;
  padding:6px;
  color:#fff;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
`

const AvailAlertBody = styled.div`
  background-color:rgb(247, 255, 255);
  width:100%;
  padding: 10px 0 10px 0;
`

const ButtonStep3 = styled.button`
  min-width:120px;
  width:auto;
  background-color:${p => p.disabled ? 'rgba(0,0,0,0.2)' : '#15bfae'} !important;
  box-shadow: 1px 1px 4px 0 rgba(0, 0, 0, 0.5);
  height: 34px;
  padding: 6px 12px;
  font-size: 16px;
  line-height: 1.42857143;
  color: #fff;
`

const Foot = styled.div`
  display:flex;
  flex:1;
  justify-content:flex-end;
  flex-direction:column;
`


const mapStateToProps = (state, ownProps) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    notifError: (data) => dispatch(Notifications.error(data)),
  }
}

export default compose(withStyles(useStyles),
  connect(mapStateToProps, mapDispatchToProps)
)(Step3)