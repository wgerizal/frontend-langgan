/* eslint-disable */
import React, { Component } from 'react'
import styled from 'styled-components'
import { Prompt } from 'react-router'

var shouldBlockNavigation;

export default class Step2 extends Component {
  componentDidMount() {
    window.onbeforeunload = function (e) {
      e = e || window.event;
      if (e) {
        e.returnValue = 'Sure?';
      }
      return 'Sure?';
    };
  }

  componentDidUpdate = () => {
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true
    } else {
      window.onbeforeunload = undefined
    }
  }

  render() {
    // function untuk block halaman ketika close tab
    window.addEventListener("beforeunload", (ev) => {  
        ev.preventDefault();
        return ev.returnValue = 'Are you sure you want to close?';
    });

    return (
      <div>  
        <Prompt
          when={shouldBlockNavigation}
          message='You have unsaved changes, are you sure you want to leave?'
          
        />
        
      <div className="step-two-info" style={{ paddingTop: "20px"}}>
        <Title>Silahkan masukkan nama yang sesuai dengan toko kamu</Title>
        <div className="form-group" style={{width:'100%', padding:'5px'}}>
          <label className="control-label">Nama toko</label>
          <Input placeholder='Contoh : Sepatu Sneakers Keren' placeholderS value={this.props.value} onChange={e => this.props.changeNama(e.target.value)} />
        </div>
        <div className="form-group" style={{width:'100%', padding:'5px'}}>
          <label className="control-label">Deskripsi toko</label>
          <textarea rows={20} placeholder='Contoh : Toko kami berdiri sejak tahun 2002, menjual berbagai macam
          sepatu. Sekarang kami sudah mempunyai 50 outlet yang tersebar di Jawa Barat' placeholderS value={this.props.deskripsi} onChange={e => this.props.changeDeskripsi(e.target.value)} style={{ height:'200px', textAlign: 'justify'}} />
        </div>
      </div>
      
      </div>
    )
  }
}


const Title = styled.h3`
  margin-bottom:20px;
`

const Input = styled.input`
  border-radius: 5px;
  box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.5);
  border: 1px solid #DFDFDF;
  height: 34px;
  padding: 6px 12px;
  font-size: 12px;
  line-height: 1.42857143;
  color: #000;
  width:50%;
`