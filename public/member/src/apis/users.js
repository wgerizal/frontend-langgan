import _axios from '../dependencies/_axios'


export function usersLoginApi(data: Object) {
	return _axios({
		url: '/api/user/login',
		timeout: 20000,
		method: 'POST',
		responseType: 'json',
		// headers: {'X-Requested-With': 'XMLHttpRequest'},
		data
	})
}



export function usersRegisterApi(data: Object) {
	return _axios({
		url: '/api/user/register',
		timeout: 20000,
		method: 'post',
		responseType: 'json',
		// headers: {'X-Requested-With': 'XMLHttpRequest'},
		data
	})
}

export function usersValidasiApi(data: Object) {
	return _axios({
		url: '/api/user/validasi',
		timeout: 20000,
		method: 'post',
		responseType: 'json',
		// headers: {'X-Requested-With': 'XMLHttpRequest'},
		data
	})
}

// export function usersLogoutApi(data: Object) {
// 	return _axios({
// 		url: '/api/user/register',
// 		timeout: 20000,
// 		method: 'post',
// 		responseType: 'json',
// 		// headers: {'X-Requested-With': 'XMLHttpRequest'},
// 		data
// 	})
// }

