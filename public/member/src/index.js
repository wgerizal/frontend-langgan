/* eslint-disable */
import React, {lazy, Suspense} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux'
import { Switch, Router } from 'react-router'
import { Route } from 'react-router-dom'
import history from './dependencies/history'
import configureStore from './store/configureStore'
import App from './App';
import PublicIndex from './components/root/PublicIndex'
import RequireAuth from './pages/RequireAuth'
import ModalReminderToko from './pages/partials/ModalReminderToko'
import LangganHome from './pages/LangganHome';

import SocialAuth from './pages/SocialAuth';

import * as serviceWorker from './serviceWorker';
// import bootstrap from 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'nprogress/nprogress.css'
import './styles/scss/style.scss';


//modal login
const ModalLogin = lazy(() => import("./pages/partials/ModalLogin"));
const ModalAgree = lazy(() => import("./pages/partials/ModalAgree"));
const ModalRegister = lazy(() => import("./pages/partials/ModalRegister"));
const ModalBerhasil = lazy(() => import("./pages/partials/ModalBerhasil"));

//landing page
const TemaPage = lazy(() => import("./pages/TemaPage"));
const PrivacyPolicyPage = lazy(() => import("./pages/PrivacyPolicyPage"));
const FAQPage = lazy(() => import("./pages/FAQPage"));
const SyaratKetentuanPage = lazy(() => import("./pages/SyaratKetentuanPage"));
const LupaPswPage = lazy(() => import("./pages/LupaPswPage"));
const ActivePage = lazy(() => import("./pages/ActivePage"));
const ResetPassPage = lazy(() => import("./pages/ResetPassPage"));


//member area
const MemberArea = lazy(() => import("./pages/MemberArea"));
const PaketChooser = lazy(() => import("./pages/PaketChooser"));
const UpdateChooser = lazy(() => import("./pages/UpdateChooser"));
const SyaratDomain = lazy(() => import("./pages/SyaratDomain"));
const Invoice = lazy(() => import("./pages/Invoice"));

//fitur & tampilan page lazy load
const FiturPage = lazy(() => import("./pages/LandingPage/FiturPage/FiturPage"));
const TampilanPage = lazy(() => import("./pages/LandingPage/TampilanPage/TampilanPage"));

const store = configureStore()

const publicIndexTarget = document.getElementById("root")

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <PublicIndex>
        <Suspense fallback={<div></div>}>
          <ModalLogin />
          <ModalAgree />
          <ModalBerhasil />
          <ModalRegister />        
        </Suspense>

        <Switch>
          <Route exact path='/' component={LangganHome} />
          <Route path='/social' component={SocialAuth} />

          <Suspense fallback={<div>Loading.....</div>}>

            {/* landingPage*/}
            <Route exact path='/tema-page' component={TemaPage} />
            <Route exact path='/privacy-policy' component={PrivacyPolicyPage} />
            <Route exact path='/faq' component={FAQPage} />
            <Route exact path='/syarat-ketentuan' component={SyaratKetentuanPage} />
            <Route exact path='/lupa-psw' component={LupaPswPage} />
            <Route exact path='/activation' component={ActivePage} />
            <Route exact path='/password/reset' component={ResetPassPage} />

            {/* member area */}
            <Route exact path='/member-area' component={RequireAuth(MemberArea)} />
            <Route exact path='/member-paket' component={RequireAuth(PaketChooser)} />
            <Route exact path='/update-paket/:id' component={RequireAuth(UpdateChooser)} />
            <Route exact path='/syarat-domain' component={RequireAuth(SyaratDomain)} />
            <Route exact path='/invoice/:kode_invoice' component={RequireAuth(Invoice)} />


            {/* page fitur & tampilan landingpage */}
            <Route exact path='/fitur-page' component={FiturPage} />
            <Route exact path='/tampilan-page' component={TampilanPage} />
          </Suspense>



        </Switch>
      </PublicIndex>
    </Router>
  </Provider>

  ,
  publicIndexTarget);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
