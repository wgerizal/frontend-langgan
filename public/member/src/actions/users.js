import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'
import { List } from 'immutable'
import Notifications from 'react-notification-system-redux'
import {
    globalUiLoginModal,
    globalFirstLoginModal,
} from './globalUi'


export function userAskLoggedIn(params) {
    return {
        type: 'USER_ASK_TO_LOGGED_IN',
        params
    }
}

export function userResetValidation(params) {
    return {
        type: 'USER_AUTH_ADD_ERROR_MESSAGE',
        error: params
    }
}

export function userRegistration(params) {
    return {
        type: 'USER_NEW_REGISTERED',
        params
    }
}

export function userRegistrationIsConfirm() {
    return {
        type: 'USER_NEW_REGISTERED_IS_CONFIRM',
    }
}

export function successLogin(logged_in) {
    return {
        type: 'USER_AUTH_SUCCESS',
        logged_in: logged_in
    }
}

export function addUserName(user) {
    return {
        type: 'ADD_USER_NAME',
        user: user
    }
}
export function addUserEmail(user) {
    return {
        type: 'ADD_USER_EMAIL',
        user: user
    }
}
export function addUserProv(user) {
    return {
        type: 'ADD_USER_PROV',
        user: user
    }
}
export function addUserCity(user) {
    return {
        type: 'ADD_USER_PROV',
        user: user
    }
}
export function addUserPostalCode(user) {
    return {
        type: 'ADD_USER_POSTAL_CODE',
        user: user
    }
}
export function addUserPhone(user) {
    return {
        type: 'ADD_USER_PHONE',
        user: user
    }
}
export function getUserInformationSuccess(user) {
    return {
        type: 'USER_GET_INFORMATION_SUCCESS',
        user
    }
}

export function updateDataErrorSuccess(error) {
    return {
        type: "UPDATE_ERROR_CHECK_SUCCESS",
        error
    }
}

export function userAddNotificationSuccess(notif) {
    return {
        type: "USER_ADD_NOTIFICATION_SUCCESS",
        notif
    }
}

export function userCheckCredential() {
    return (dispatch) => {

        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/user`,
                timeout: 20000,
                method: 'get',
                headers: {
                    'Authorization': `Bearer ${store.get()}`
                },
                responseType: 'json'
            })
                .then((response) => {
                    let data = response.data

                    // let first_login  = data.first_login
                    dispatch(successLogin(true))
                    dispatch(addUserName(response.data.name))
                    if (data.message) {
                        throw response
                    }
                    dispatch(getUserInformationSuccess(data))

                    dispatch(userGetNotifikasi())
                    // if (first_login == 0) {
                    //     dispatch(globalFirstLoginModal(true))
                    // }
                })
                .catch((error) => {
                    dispatch(successLogin(false))
                    dispatch(push('/'))
                    dispatch(globalUiLoginModal(true))
                })
        })
    }
}

export function userCheckHomePageCredentials() {
    return (dispatch) => {

        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/user`,
                timeout: 20000,
                method: 'get',
                headers: {
                    'Authorization': `Bearer ${store.get()}`
                },
                responseType: 'json'
            })
                .then((response) => {
                    let data = response.data
                    dispatch(successLogin(true))
                    dispatch(addUserName(response.data.name))
                    if (data.message) {
                        throw response
                    }

                    resolve(dispatch(getUserInformationSuccess(data)))

                })
                .catch((error) => {
                    dispatch(successLogin(false))
                    // dispatch(push('/'))
                })
        })
    }

}

export function userGetNotifikasi() {
    return (dispatch) => {

        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/list-pesan`,
                timeout: 20000,
                method: 'get',
                headers: {
                    'Authorization': `Bearer ${store.get()}`
                },
                responseType: 'json'
            })
                .then((response) => {
                    let data = response.data
                    // console.log(data)
                    dispatch(userAddNotificationSuccess(data))
                    // let first_login  = data.first_login

                })
                .catch((error) => {

                })
        })
    }
}

export function userResetNotif() {
    return (dispatch) => {

        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/reset-notifikasi`,
                timeout: 20000,
                method: 'post',
                headers: {
                    'Authorization': `Bearer ${store.get()}`
                },
                responseType: 'json'
            })
                .then((response) => {
                    let data = response.data
                    // console.log(data)
                    // dispatch(userAddNotificationSuccess(data))
                    // let first_login  = data.first_login

                })
                .catch((error) => {

                })
        })
    }
}



export function userLogout() {
    return (dispatch) => {
        store.removeUserCredentials()
        store.remove()
        dispatch(successLogin(false))
        dispatch(push('/'))
    }
}


export function submitForgotPass(email) {
    return (dispatch) => {

        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/user/send_link_reset`,
                timeout: 20000,
                method: 'post',
                responseType: 'json',
                data: { email: email }
            })
                .then((response) => {
                    dispatch(
                        Notifications.success({
                            title: 'Success',
                            message: response.data.message,
                            position: 'bl'
                        })
                    )

                })
                .catch((error) => {

                    dispatch(
                        Notifications.error({
                            title: 'Error',
                            message: error.response.data.message,
                            position: 'bl'
                        })

                    )

                })
        })
    }

}

export function submitResetPassword(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            _axios({
                url: `/api/user/change_password?=`,
                timeout: 20000,
                method: 'post',
                responseType: 'json',
                headers: {
                    'Authorization': `Bearer ${store.get()}`,
                },
                data: {
                    old_password: data.old_password_val,
                    password: data.password_val,
                    password_confirmation: data.password_confirmation_val,
                }
            })
                .then((response) => {
                    let data = response.data
                    if (data.status == 'error') {
                        throw response
                    }
                    dispatch(
                        Notifications.success({
                            title: 'Success',
                            message: response.data.data,
                            position: 'bl'
                        })
                    )
                    window.location.reload(false)
                    dispatch(push('/member-area'))
                })
                .catch((error) => {
                    console.log(error.data)
                    dispatch(
                        Notifications.error({
                            title: 'Error',
                            message: error.data.messages.password,
                            position: 'bl'
                        })

                    )

                })
        })
    }

}

export function userUpdateForm(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {

            let formData = new FormData()

            formData.append('email', data.email_val || '')
            formData.append('photo', data.photo || '')
            formData.append('name', data.name_val || '')
            formData.append('province_id', data.province_val || '')
            formData.append('city_id', data.city_val || '')
            formData.append('no_hp', data.phone_val || '')
            formData.append('kode_pos', data.pos_code_val || '')
            formData.append('address', data.address_val || '')
            formData.append('password', data.password || '')
            formData.append('wa_used', data.is_wa_number ? 1 : 0)
            _axios({
                url: `/api/user/profile`,
                timeout: 20000,
                method: 'post',
                responseType: 'json',
                headers: {
                    'Authorization': `Bearer ${store.get()}`,
                    'Content-Type': 'multipart/form-data'
                },
                data: formData
            })
                .then((response) => {
                    data = response.data
                    if (data.status == 'error') {
                        throw response
                    }
                    store.removeEmail()
                    store.setUserEmail(data.data.email)
                    dispatch(userCheckCredential())
                    dispatch(
                        Notifications.success({
                            title: 'Selamat',
                            message: 'Profil kamu berhasil diperbarui',
                            position: 'tr'
                        })
                    )
                    dispatch(push('/member-area'))
                    resolve('done')

                })
                .catch((error) => {
                    console.log(error)
                    if (typeof error.data == "undefined") {
                        dispatch(
                            Notifications.error({
                                title: 'Pemberitahuan',
                                message: 'Profil kamu tidak berhasil diperbarui, silahkan cek koneksi internet kamu',
                                position: 'bl'
                            })

                        )
                    } else {
                        dispatch(updateDataErrorSuccess(error.data.messages))
                        dispatch(
                            Notifications.error({
                                title: 'Pemberitahuan',
                                message: 'Profil kamu tidak berhasil diperbarui, silahkan coba lagi nanti',
                                position: 'bl'
                            })

                        )

                    }
                    resolve('done')

                })
        })
    }
}
export function userSubscribe(email) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/subscribe/submit`,
                timeout: 20000,
                method: 'post',
                responseType: 'json',
                data: {
                    email: email
                }
            })
                .then((response) => {
                    email = response.data
                    if (email.status == 'error') {
                        throw response
                    }
                    dispatch(
                        Notifications.success({
                            title: 'Selamat',
                            message: 'Selamat, kamu berhasil berlangganan informasi dari Langgan',
                            position: 'tr',
                        }),
                        resolve('done'),
                    )

                })
                .catch((error) => {
                    if (typeof error.email == "undefined") {
                        dispatch(
                            Notifications.error({
                                title: 'Pemberitahuan',
                                message: 'Silahkan masukan email kamu dengan benar',
                                position: 'bl'
                            })

                        )
                    } else {
                        dispatch(updateDataErrorSuccess(error.data.messages))
                        dispatch(
                            Notifications.error({
                                title: 'Pemberitahuan',
                                message: 'Silahkan masukan email kamu dengan benar',
                                position: 'bl'
                            })

                        )

                    }
                    resolve('done')


                })
        })
    }
}
