import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import { push } from 'react-router-redux';
import omit from 'lodash/omit'
import store from '../helpers/user_session'
import {List} from 'immutable'
import Notifications from 'react-notification-system-redux'


export function addDataTemaSuccess(tema){
    return{
         type:'ADD_TEMA_SUCCESS',
          tema
    }
   
}

export function temasFetchPaginationSuccess(pagination){
	return {
		type: 'TEMAS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function temaAddFilterDataPerPageSuccess(per_page){
	return{
		type:'TEMA_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
		per_page
	}
}

export function temaAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(temaAddFilterDataPerPageSuccess(per_page)))
		})
	}
}

export function temaAddFilterDataPageSuccess(page){
    return{
        type:'TEMA_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}

export function temaAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(temaAddFilterDataPageSuccess(page)))
		})
	}
}

export function temaAddFilterDataFilterSuccess(filter){
    return{
        type:'TEMA_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}

export function temaAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(temaAddFilterDataFilterSuccess(filter)))
		})
	}
}

export function getTema(params){
    return (dispatch) => {

		return new Promise((resolve, reject) => {
		
			_axios({
				url:`/api/tema/filter`,
				timeout: 20000,
				method: 'get',
                params:params.toJS(),
				responseType: 'json'
			})
			.then((response) => {
				let data = response.data.data.data
				let resp = response.data.data
				const pagination = omit(resp, 'data')
				const per_page = resp.per_page
                // console.log(pagination)
                dispatch(addDataTemaSuccess(data))
				dispatch(temasFetchPaginationSuccess(pagination))
				dispatch(temaAddFilterDataPerPageSuccess(per_page))
			})
			.catch((error) => {
				console.log('error')
				// dispatch(push('/'))
			})
		})
	}
}