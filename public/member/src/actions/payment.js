import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'
import { List } from 'immutable'
import Notifications from 'react-notification-system-redux'
import NProgress from 'nprogress/nprogress.js';

//library facebook pixel
import ReactPixel from 'react-facebook-pixel';


//import actions
import {
  tokoFetchData,
} from '../actions/toko'

export function kuponAddDataSuccess(kupon) {
	return {
		type: 'KUPON_FETCH_DATA_SUCCESS',
		kupon
	}
}

export function paymentToko(data) {
  const options = {
    autoConfig: true, // set pixel's autoConfig
    debug: false, // enable logs
  };
  ReactPixel.init('490052041836692', options);

	return (dispatch) => {
		if(data.statusPembayaran === 'Bayar'){
			window.snap.pay(data.arrayData.snap_token, {
				// Optional
				onSuccess: function (result) {
          window.location.reload()
				},
				// Optional
				onPending: function (result) {
          window.location.reload()
				},
				// Optional
				onError: function (result) {
          window.location.reload()
				}
			});
		} else if(data.statusPembayaran === 'Ubah'){
			NProgress.start()
			return new Promise((resolve, reject) => {
				_axios({
					url: `/api/billing/submit`,
					timeout: 20000,
					method: 'post',
					headers: {
						'Authorization': `Bearer ${store.get()}`
					},
					data: data.arrayData,
					responseType: 'json'
				}).then((response) => {
						console.log(response)
						NProgress.done()
						let data = response.data

						window.snap.pay(data.snap_token, {
							// Optional
							onSuccess: function (result) {
                window.location.reload()
							},
							// Optional
							onPending: function (result) {
                window.location.reload()
							},
							// Optional
							onError: function (result) {
                window.location.reload()
							}
						});
					}).catch((error) => {
						NProgress.done()
						console.log(error)
						dispatch(
							Notifications.error({
								title: 'Error',
								message: error.response.messages,
								position: 'tr'
							})
						)
					})
			})
		} else {
			if (data.arrayData.nominal === parseInt(data.dataKupon)) {
				NProgress.start()
				return new Promise((resolve, reject) => {
					_axios({
						url: `/api/billing/submit`,
						timeout: 20000,
						method: 'post',
						headers: {
							'Authorization': `Bearer ${store.get()}`
						},
						data: data.arrayData,
						responseType: 'json'
					})
						.then((response) => {
							NProgress.done()
              let data = response.data;
              
              ReactPixel.track('track', 'PageView');
              ReactPixel.track('track', 'Purchase', {
                value: 1500000,
                currency: 'IDR',
              });

							dispatch(
								push('/member-area'),
								Notifications.success({
									title: 'Selamat',
									message: 'Toko kamu berhasil dibuat, silahkan akses bisnis kit kamu, jika kamu belum bisa mengakses bisnis kit kamu, silahkan tunggu beberapa saat lalu coba kembali, #LetsGrow.',
									position: 'tr'
								})
							)
	
						})
						.catch((error) => {
							NProgress.done()
							console.log(error.response)
							dispatch(
								push('/member-area'),
								Notifications.error({
									title: 'Opps',
									message: error.response.data.messages,
									position: 'tr'
								})
							)
						})
				})
			} else {
				NProgress.start()
				return new Promise((resolve, reject) => {
					_axios({
						url: `/api/billing/submit`,
						timeout: 20000,
						method: 'post',
						headers: {
							'Authorization': `Bearer ${store.get()}`
						},
						data: data.arrayData,
						responseType: 'json'
					})
						.then((response) => {
							NProgress.done()
							let data = response.data
	
							window.snap.pay(data.snap_token, {
								// Optional
								onSuccess: function (result) {
									dispatch(
                    push('/member-area'),                 
										Notifications.success({
											title: 'Selamat',
											message: 'Toko kamu berhasil dibuat, silahkan akses bisnis kit kamu, jika kamu belum bisa mengakses bisnis kit kamu, silahkan tunggu beberapa saat lalu coba kembali, #LetsGrow.',
											position: 'tr'
										})
									)
								},
								// Optional
								onPending: function (result) {
									dispatch(
                    push('/member-area'),               
										Notifications.success({
											title: 'Selamat',
											message: 'Toko kamu berhasil dibuat, silahkan akses bisnis kit kamu.',
											position: 'tr'
										})
									)
								},
								// Optional
								onError: function (result) {
									dispatch(
                    push('/member-area'),               
										Notifications.error({
											title: 'Opss',
											message: 'Telah terjadi kesalahan, silahkan coba lagi beberapa saat.',
											position: 'tr'
										})
									)
								}
							});
						})
						.catch((error) => {
							NProgress.done()
							dispatch(
								Notifications.error({
									title: 'Error',
									message: error.response.messages,
									position: 'tr'
								})
							)
						})
				})
			}
		}
	}
}


export function paymentUpdateToko(data) {
	return (dispatch) => {
		NProgress.start()
		return new Promise((resolve, reject) => {

			_axios({
				url: `/api/billing/updatePaket`,
				timeout: 20000,
				method: 'post',
				headers: {
					'Authorization': `Bearer ${store.get()}`
				},
				data: data,
				responseType: 'json'
			})
				.then((response) => {
					NProgress.done()
					let data = response.data
					window.snap.pay(data.snap_token, {
						// Optional
						onSuccess: function (result) {
							dispatch(push('/member-area'))
						},
						// Optional
						onPending: function (result) {
							dispatch(push('/member-area'))
						},
						// Optional
						onError: function (result) {
							dispatch(push('/member-area'))
						}
					});
				})
				.catch((error) => {
					NProgress.done()
				})
		})
	}
}

export function paymentFinish(data) {
	return (dispatch) => {

		return new Promise((resolve, reject) => {
			_axios({
				url: `/api/billing/notification/handler`,
				timeout: 20000,
				method: 'post',
				headers: {
					'Authorization': `Bearer ${store.get()}`
				},
				data: data,
				responseType: 'json'
			})
				.then((response) => {
					let data = response.data.message
					resolve(data)
				})
				.catch((error) => {

				})
		})
	}
}
export function getDetailInvoice(data) {
	return (dispatch) => {

		return new Promise((resolve, reject) => {
			_axios({
				url: `/api/invoice/${data}`,
				timeout: 20000,
				method: 'post',
				headers: {
					'Authorization': `Bearer ${store.get()}`
				},
				contentType: 'application/json'
			})
				.then((response) => {
					let data = response.data.message
					resolve(data)


				})
				.catch((error) => {

				})
		})
	}
}