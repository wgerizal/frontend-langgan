import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function tokosAddDataSuccess(tokos) {
	return {
		type: 'TOKOS_FETCH_DATA_SUCCESS',
		tokos
	}
}

export function loaderFetchDataToko(data) {
	return {
		type: 'LOADER_FETCH_DATA_TOKO_SUCCESS',
		data
	}
}

export function jumlahTokoSuccess(jumlah_toko) {
	return {
		type: 'JUMLAH_TOKO_SUCCESS',
		jumlah_toko
	}
}

export function tokosFetchPaginationSuccess(pagination) {
	return {
		type: 'TOKOS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function tokoAddFilterDataPageSuccess(page) {
	return {
		type: 'TOKO_FILTER_DATA_ADD_PAGE_SUCCESS',
		page: page
	}
}


export function tokoAddFilterDataPerPageSuccess(per_page) {
	return {
		type: 'TOKO_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
		per_page: per_page
	}
}


export function tokoAddFilterDataFilterSuccess(filter) {
	return {
		type: 'TOKO_FILTER_DATA_ADD_FILTER_SUCCESS',
		filter: filter
	}
}


export function tokoAddFilterDataOrderBySuccess(order_by) {
	return {
		type: 'TOKO_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
		order_by: order_by
	}
}


export function tokoAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(tokoAddFilterDataPageSuccess(page)))
		})
	}
}


export function tokoAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(tokoAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function tokoAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(tokoAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function tokoAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(tokoAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function tokoFetchData(filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))
		let loader = true;
		return _axios({
			url: `/api/toko/user`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
				// 'X-Requested-With': 'XMLHttpRequest',
				'Authorization': `Bearer ${store.get()}`,
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			data: filter_data.toJS()
		}).then((response) => {
			console.log(response)
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			let jumlah_toko = response.data.jumlah_toko
			let toko = []
			if (resp != null) {
				// resp.status_flag = "Toko Aktif"
				// resp.status_pembayaran = 2	
				toko = [resp]
			}
			// const toko = resp.data
			const per_page = resp.per_page
			// const current_page = resp.current_page
			const pagination = omit(resp, 'data')

			//loader fetch data
			loader = false;
			dispatch(loaderFetchDataToko(loader))

			
			dispatch(tokosAddDataSuccess(toko))
			// dispatch(jumlahTokoSuccess(jumlah_toko))
			dispatch(tokosFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(tokoAddFilterDataPerPageSuccess(per_page))
		}).catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))
			// dispatch(candidatesIsLoading(false))

			//loader fetch data toko
			loader = false;
			dispatch(loaderFetchDataToko(loader))

			console.error(error)
		})
	}
}
export function shopOffline(id, filter_data) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/toko/offline`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Authorization': `Bearer ${store.get()}`
			},
			data: { id: id }
		})
			.then((response) => {
				// dispatch(candidatesIsLoading(false))


				dispatch(tokoFetchData(filter_data))

			})
			.catch((error) => {
				// dispatch(Notifications.error({
				//   title: 'Error',
				// 	message: error.response.data.message,
				// 	position: 'bl'
				// }))

				// dispatch(candidatesIsLoading(false))

				console.error(error)
			})
	}
}