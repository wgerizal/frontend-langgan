import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'
import {List} from 'immutable'
import Notifications from 'react-notification-system-redux'

export function addProvincesDataSuccess(province){
    return{
        type:'ADD_PROVINCE_SUCCESS',
        province
    }
}

export function getProvinces(){
     return (dispatch) => {

		return new Promise((resolve, reject) => {
		
			_axios({
				url:`/api/provinsi`,
				timeout: 20000,
				method: 'post',
				responseType: 'json'
			})
			.then((response) => {
				let data = response.data.data
                dispatch(addProvincesDataSuccess(data))
                
			})
			.catch((error) => {
              
			})
		})
	}
}


export function addCitysDataSuccess(city){
    return{
        type:'ADD_CITY_SUCCESS',
        city
    }
}

export function getCitys(province_id){
     return (dispatch) => {

		return new Promise((resolve, reject) => {
		
			_axios({
				url:`/api/kota`,
				timeout: 20000,
				method: 'post',
                data:{
                    province_id:province_id
                    },
				responseType: 'json'
			})
			.then((response) => {
				let data = response.data.data
                dispatch(addCitysDataSuccess(data))
                resolve()
                
			})
			.catch((error) => {
              
			})
		})
	}
}