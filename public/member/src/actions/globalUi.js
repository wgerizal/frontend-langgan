
export function globalUiLoginModal(login_modal){
    return{
        type:'LOGIN_MODAL_SUCCESS',
        login_modal:login_modal
    }
}

export function globalUiRegisterModal(register_modal){   
    return{
        type:'REGISTER_MODAL_SUCCESS',
        register_modal:register_modal
    }
}

export function globalUiAgreementModal(agreement_modal){   
    return{
        type:'AGREEMENT_MODAL_SUCCESS',
        agreement_modal:agreement_modal
    }
}

export function globalUiActivationModal(activation_modal){   
    return{
        type:'ACTIVATION_MODAL_SUCCESS',
        activation_modal:activation_modal
    }
}

export function globalFirstLoginModal(is_first_login){
    return{
        type:'FIRST_LOGIN_MODAL_SUCCESS',
        is_first_login:is_first_login
    }
}

export function indexAddSelectedPaketSuccess(paket){
    return{
        type:'INDEX_SELECTED_PAKET_ADD',
        paket
    }
}

export function indexAddSelectedPaket(paket){
    return(dispatch)=>{
        return new Promise((resolve, reject) => {
            resolve(dispatch(indexAddSelectedPaketSuccess(paket)))
        })
    }
}