import { combineReducers } from "redux";
import { routerReducer as routing } from "react-router-redux";
import { reducer as notifications } from 'react-notification-system-redux'

import {
    loginUi,
    registerError,
    indexPaketChooser,
}from './globalUi'

import{
loginError,
}from './login'

import{
    user,
    userInformation,
    registerSaveTemp,
    userNotification,
}from './auth'

import{
  province,
  city,
}from './region'

import{
  tema,
  temaPagination,
  temaFilterData,
}from './tema'

import{
tokoFilterData,
tokos,
tokosPagination,
jumlah_toko,
loaderFetchDataToko,
}from './toko'

import{
kupon_code,
}from './payment'

import{
  updateProfileError,
}from'./update'



export default combineReducers({
  routing,
  loginUi,
  indexPaketChooser,
  loginError,
  registerError,
  notifications,
  user,
  userInformation,
  userNotification,
  province,
  city,
  tema,
  temaPagination,
  temaFilterData,
  tokoFilterData,
  tokos,
  jumlah_toko,
  tokosPagination,
  kupon_code,
  updateProfileError,
  registerSaveTemp,
  loaderFetchDataToko,
});