import { List, Map, fromJS } from 'immutable'

const initFilterTema = fromJS({
	page: 1,
    per_page:10,
    filter:'',
    order_by:'desc'
})

export function tema(state= List([]),action){
	switch (action.type) {
		case 'ADD_TEMA_SUCCESS':
			return List(action.tema);
		default:
			return state;
	}
}

export function temaFilterData(state = initFilterTema, action) {
	switch(action.type) {
		case 'TEMA_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'TEMA_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'TEMA_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'TEMA_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}


export function temaPagination(state = Map({}), action) {
	switch(action.type) {
		case 'TEMAS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}