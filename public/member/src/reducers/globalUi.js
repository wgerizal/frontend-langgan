import { List, Map, fromJS } from 'immutable'

const initUiData = Map({
	login_modal: false,
	register_modal:false,
	is_first_login:false,
	agreement_modal:false,
	activation_modal:false,
})

const initRegisterError = Map({
	error:[]
})

export function loginUi(state = initUiData, action) {
	switch(action.type) {
		case 'LOGIN_MODAL_SUCCESS':
			return state.set('login_modal', action.login_modal)
		case 'REGISTER_MODAL_SUCCESS':
			return state.set('register_modal', action.register_modal)
		case 'AGREEMENT_MODAL_SUCCESS':
			return state.set('agreement_modal', action.agreement_modal)
		case 'ACTIVATION_MODAL_SUCCESS':
			return state.set('activation_modal', action.activation_modal)
		case 'FIRST_LOGIN_MODAL_SUCCESS':
			return state.set('is_first_login', action.is_first_login)
		default:
			return state
	}
}

export function registerError(state = initRegisterError, action){
	switch(action.type) {
		case 'REGISTER_ERROR_CHECK_SUCCESS':
			return state.set('error', action.error)
		default:
			return state
	}

}

export function indexPaketChooser(state=Map({}), action){
	switch (action.type) {
		case 'INDEX_SELECTED_PAKET_ADD':
			return Map(action.paket)
			
		default:
			return state
	}
}