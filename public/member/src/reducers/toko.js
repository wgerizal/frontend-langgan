import { List, Map, fromJS } from 'immutable'

const initFilterToko = fromJS({
	page: 1,
    per_page:3,
    filter:'',
	order_by:'desc',
})


export function tokoFilterData(state = initFilterToko, action) {
	switch(action.type) {
		case 'TOKO_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'TOKO_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'TOKO_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'TOKO_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function tokos(state = List([]), action) {
	switch (action.type) {
		case 'TOKOS_FETCH_DATA_SUCCESS':
			return fromJS(action.tokos)
		default:
			return state
	}
}

export function jumlah_toko(state = '', action) {
	switch (action.type) {
		case 'JUMLAH_TOKO_SUCCESS':
		console.log(action.jumlah_toko)
			return fromJS(action.jumlah_toko)
		default:
			return state
	}
}


export function tokosPagination(state = Map({}), action) {
	switch(action.type) {
		case 'TOKOS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}


//loader ketika fetch toko
export function loaderFetchDataToko(state = Map({}), action) {
	switch(action.type) {
		case 'LOADER_FETCH_DATA_TOKO_SUCCESS':
			return fromJS(action.data)
		default:
			return state
	}
}