import { List, Map, fromJS } from 'immutable'

const initUser = Map({
	logged_in: false,
	name:'',
	email:'',
	address:'',
	phone:'',
	id_prov:'',
	id_kota:'',
	postal_code:''
})

export function user(state = initUser, action) {
	switch(action.type) {
		case 'USER_AUTH_SUCCESS':
			return state.set('logged_in', action.logged_in)
		case 'ADD_USER_NAME':
			return state.set('name', action.user)
		case 'ADD_USER_EMAIL':
			return state.set('email', action.user)
		case 'ADD_USER_ADDRESS':
			return state.set('address', action.user)
		case 'ADD_USER_PROV':
			return state.set('phone', action.user)
		case 'ADD_USER_CITY':
			return state.set('id_prov', action.user)
		case 'ADD_USER_POSTAL_CODE':
			return state.set('id_kota', action.user)
		case 'ADD_USER_PHONE':
			return state.set('postal_code', action.user)
		default:
			return state
	}
}

export function userInformation(state= Map({}),action){
	switch (action.type) {
		case 'USER_GET_INFORMATION_SUCCESS':
			return Map(action.user) ;
		default:
			return state;
	}
}

export function registerSaveTemp(state= Map({}),action){
	switch (action.type) {
		case 'REGISTER_SAVE_INFORMATION_REGISTER_TEMP_SUCCESS':
			return Map(action.register_temp) ;
		default:
			return state;
	}
}

export function userNotification(state= Map({}),action){
	switch (action.type) {
		case 'USER_ADD_NOTIFICATION_SUCCESS':
			return Map(action.notif) ;
		default:
			return state;
	}
}