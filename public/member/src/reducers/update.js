import { List, Map, fromJS } from 'immutable'


const initUpdateError = Map({
	error:[]
})


export function updateProfileError(state = initUpdateError, action){
	switch(action.type) {
		case 'UPDATE_ERROR_CHECK_SUCCESS':
			return state.set('error', action.error)
		default:
			return state
	}

}