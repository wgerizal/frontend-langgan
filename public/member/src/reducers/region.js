import { List, Map, fromJS } from 'immutable'

export function province(state= List([]),action){
	switch (action.type) {
		case 'ADD_PROVINCE_SUCCESS':
			return List(action.province) ;
		default:
			return state;
	}
}


export function city(state= List([]),action){
	switch (action.type) {
		case 'ADD_CITY_SUCCESS':
			return List(action.city);
		default:
			return state;
	}
}