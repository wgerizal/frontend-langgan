/* eslint-disable */
import { put, call, all, select, take, delay } from "redux-saga/effects";
import { Map, List, fromJS } from "immutable";
import { push } from 'react-router-redux';
import Notifications from 'react-notification-system-redux'
import store from '../helpers/user_session'
import omit from 'lodash/omit'
import NProgress from 'nprogress/nprogress.js'
import {
    usersLoginApi,
    usersRegisterApi,
    usersValidasiApi,
} from "../apis/users";

import {
    getregisterSaveTemp
} from "../selectors"


export function* userAskLoggedIn({
    params
}: Object): Generator<*, *, *> {
    try {
        const credentials = {
            email: params.email,
            password: params.pass
        }
        const remember = params.checkbox
        let response: Object = {};
        NProgress.start()
        response = yield call(usersLoginApi, credentials)
        if (response.status >= 200 && response.status < 300 && response.data.status != "error") {
            let token_user = response.data.access_token
            let token_user_dashboard = response.data.token_dashboard
            if (remember == true) {
                store.setUserEmail(params.email)
                store.setUserPass(params.pass)
            }
            NProgress.done()
            store.remove()
            store.set(token_user)
            store.setTokenDashboard(token_user_dashboard)
            yield put({
                type: 'USER_AUTH_SUCCESS',
                logged_in: true
            })
            yield put({
                type: 'ADD_USER_NAME',
                user: response.data.user.name
            })
            yield put({
                type: 'ADD_USER_EMAIL',
                user: response.data.user.email
            })
            yield put({
                type: 'ADD_USER_ADDRESS',
                user: response.data.user.address
            })
            yield put({
                type: 'ADD_USER_PROV',
                user: response.data.user.id_prov
            })
            yield put({
                type: 'ADD_USER_CITY',
                user: response.data.user.id_kota
            })
            yield put({
                type: 'ADD_USER_POSTAL_CODE',
                user: response.data.user.kode_pos
            })
            yield put({
                type: 'ADD_USER_PHONE',
                user: response.data.user.no_hp
            })
            yield put({
                type: 'LOGIN_MODAL_SUCCESS',
                login_modal: false
            })

            // yield put({

            // })
            yield put(
                push('/member-area')
            )
        } else {
            throw response
        }
    } catch (error) {
        NProgress.done()
        yield put({
            type: 'USER_AUTH_SUCCESS',
            logged_in: false
        })


        if (error.data.messages) {
            yield put({
                type: 'USER_AUTH_ADD_ERROR_MESSAGE',
                error: error.data.messages
            })
        }
        store.removeUserCredentials()
    }
}


export function* userRegistration({
    params
}: Object): Generator<*, *, *> {
    try {
        const data = {
            email: params.email,
            name: params.email.substring(0, params.email.lastIndexOf("@")),
            password: params.pass,
            password_confirmation: params.pass_confirm,
        }

        let response: Object = {};
        response = yield call(usersValidasiApi, data)
        if (response.status >= 200 && response.status < 300 && response.data.status != "error") {

            yield put({
                type: 'REGISTER_MODAL_SUCCESS',
                register_modal: false
            })
            yield put({
                type: 'AGREEMENT_MODAL_SUCCESS',
                agreement_modal: true
            })
            yield put({
                type: 'REGISTER_SAVE_INFORMATION_REGISTER_TEMP_SUCCESS',
                register_temp: data
            })
            // yield put(
            //     Notifications.success({
            // 		title:'Success',
            // 		message:'Registration success',
            //         position:'bl'
            // 	})
            // )


        } else {
            throw response
        }
    } catch (error) {
        if (typeof error.data == 'undefined') {
            yield put(
                Notifications.error({
                    title: 'Error',
                    message: 'Check your connection and try again . . .',
                    position: 'bl'
                })
            )
        } else {
            yield put({
                type: 'REGISTER_ERROR_CHECK_SUCCESS',
                error: error.data.messages
            })
            yield put(
                Notifications.error({
                    title: 'Error',
                    message: 'Please check your form . . .',
                    position: 'bl'
                })
            )
        }

    }
}


export function* userRegistrationIsConfirm(): Generator<*, *, *> {
    try {

        const registerSaveTemp: Map<string, any> = yield select(
            getregisterSaveTemp
        );
        // console.log(registerSaveTemp.get('pass_confirm'))
        const data = {
            email: registerSaveTemp.get('email'),
            name: registerSaveTemp.get('email').substring(0, registerSaveTemp.get('email').lastIndexOf("@")),
            password: registerSaveTemp.get('password'),
            password_confirmation: registerSaveTemp.get('password_confirmation'),
        }
        let response: Object = {};
        response = yield call(usersRegisterApi, data)
        if (response.status >= 200 && response.status < 300 && response.data.status != "error") {
            yield put({
                type: 'REGISTER_MODAL_SUCCESS',
                register_modal: false
            })

            yield put(
                Notifications.success({
                    title: 'Success',
                    message: 'Registration success',
                    position: 'bl'
                })
            )


        } else {
            throw response
        }
    } catch (error) {
        // console.log(error)
        if (typeof error.data == 'undefined') {
            yield put(
                Notifications.error({
                    title: 'Error',
                    message: 'Check your connection and try again . . .',
                    position: 'bl'
                })
            )
        } else {
            yield put({
                type: 'REGISTER_ERROR_CHECK_SUCCESS',
                error: error.data.messages
            })
            yield put(
                Notifications.error({
                    title: 'Error',
                    message: 'Please check your form . . .',
                    position: 'bl'
                })
            )
        }

    }
}