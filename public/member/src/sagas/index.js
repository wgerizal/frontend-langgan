import { all, takeLatest } from "redux-saga/effects";

import {
	userAskLoggedIn,
    userRegistration,
    userRegistrationIsConfirm,
} from './users'


export default function* root(){
    yield all([
        takeLatest("USER_ASK_TO_LOGGED_IN", userAskLoggedIn),
        takeLatest("USER_NEW_REGISTERED", userRegistration),
        takeLatest("USER_NEW_REGISTERED_IS_CONFIRM", userRegistrationIsConfirm),
        
    ])
}