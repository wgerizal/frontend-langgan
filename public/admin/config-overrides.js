/* config-overrides.js */
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const DuplicatePackageCheckerPlugin = require("duplicate-package-checker-webpack-plugin");

module.exports = {
  // The Webpack config to use when compiling your react app for development or production.
  // can be used for override webpack CRA
  webpack: function(config, env) {
    config.optimization.runtimeChunk = true;
    config.optimization.splitChunks = {
      chunks: 'all',
      minSize: 30000,
      maxSize: 50000,
      minChunks: 1,
      maxAsyncRequests: 6,
      maxInitialRequests: 4,
      automaticNameDelimiter: '~',
      cacheGroups: {
        default: false,
        reuseExistingChunk: false
      }
    };
    
    // ...add your webpack config
    config.plugins = config.plugins.concat([
      new webpack.DefinePlugin({ // <-- key to reducing React's size
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }),
      new DuplicatePackageCheckerPlugin(),
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        test: /\.js(\?.*)?$/i,
        uglifyOptions: {
          compress: true,
          ecma: 6,
          mangle: true
        },
        sourceMap: true
      }),
      new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks
      new CompressionPlugin({   //<-- Add this
        //asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8
      })
    ])
    return config;
  },
  optimization: {
    minimize: true
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    "alias": {
      "react": "preact-compat",
      "react-dom": "preact-compat"
    }
  },
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname + '/build/',
    publicPath: '/',
    filename: 'bundle.js'
  },
  devtool: "source-map",
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel'
    }]
  }
}