const USER_SESSION = '1987277asdbahsunhcb6v8b90-m,./,.98129???o=122**61';
const USER_EMAIL = '2345678aoskdjhgtxzyfcgv1v526781irjfvtasrdbga';
const USER_PASS = '12673uihc76at23h12>1923HAbHBAGBSDJAHNun1212NAUH';

let store = {
  get() {
    let userDataStr = localStorage.getItem(USER_SESSION);
    
    const token = JSON.parse(userDataStr)
    return token;
  },

  getUserEmail() {
    let userDataStr = localStorage.getItem(USER_EMAIL);
    const email = JSON.parse(userDataStr) 
    return email;
  },

  getUserPass() {
    let userDataStr = localStorage.getItem(USER_PASS);
    const pass = JSON.parse(userDataStr)
    return pass;
  },
  //key code
  getCodeOtp(key){
    let userDataStr = localStorage.getItem(key);
    const otp = JSON.parse(userDataStr)
    return otp;
  },

  set(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_SESSION, JSON.stringify(userData));
    }
  },

  setUserEmail(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_EMAIL, JSON.stringify(userData));
    }
  },

  setUserPass(userData) {
    let pass = userData != null;
    if (pass) {
      localStorage.setItem(USER_PASS, JSON.stringify(userData));
    }
  },

  setCodeOtp(key,code){
    let pass = code != null;
    if (pass) {
      localStorage.setItem(key, JSON.stringify(code));
    }
  },

  remove() {
    localStorage.removeItem(USER_SESSION);
  },

  removeUserCredentials() {
    localStorage.removeItem(USER_EMAIL);
    localStorage.removeItem(USER_PASS);
  },

  isLoggedIn() {
    let userData = this.get();
    if (userData) {
      return true
    }

    this.remove();
    return false;
  }
};

export default store;
