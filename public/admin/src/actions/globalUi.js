
export function globalUiLoginModal(login_modal){
    return{
        type:'LOGIN_MODAL_SUCCESS',
        login_modal:login_modal
    }
}

export function globalUiRegisterModal(register_modal){
    return{
        type:'REGISTER_MODAL_SUCCESS',
        register_modal:register_modal
    }
}
