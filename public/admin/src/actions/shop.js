import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function shopsAddDataSuccess(shops){
    return{
        type:'SHOPS_FETCH_DATA_SUCCESS',
        shops
    }
}

export function shopsFetchPaginationSuccess(pagination) {
	return {
		type: 'SHOPS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function shopAddFilterDataPageSuccess(page){
    return{
        type:'SHOP_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function shopsAddFilterDataPerPageSuccess(per_page){
    return{
        type:'SHOP_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function shopAddFilterDataFilterSuccess(filter){
    return{
        type:'SHOP_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function shopAddFilterDataOrderBySuccess(order_by){
    return{
        type:'SHOP_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function shopAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(shopAddFilterDataPageSuccess(page)))
		})
	}
}


export function shopsAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(shopsAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function shopAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(shopAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function shopAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(shopAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function shopFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/toko/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const shop = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(shopsAddDataSuccess(shop))
			dispatch(shopsFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(shopsAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}

export function shopActivate( id,filter_data) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/toko/activate`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: {id:id}
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			
			
			dispatch(shopFetchData(filter_data))
			
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}