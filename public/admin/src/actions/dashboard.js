import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function dashboardDataAddUserSuccess(total_user){
    return{
        type:'DASHBOARD_DATA_ADD_USER_SUCCESS',
        total_user:total_user
    }
}


export function dashboardDataAddUser(total_user) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(dashboardDataAddUserSuccess(total_user)))
		})
	}
}

export function dashboardDataAddTokoSuccess(total_toko){
    return{
        type:'DASHBOARD_DATA_ADD_TOKO_SUCCESS',
        total_toko:total_toko
    }
}


export function dashboardDataAddToko(total_toko) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(dashboardDataAddTokoSuccess(total_toko)))
		})
	}
}

export function dashboardDataAddPendapatanSuccess(total_pendapatan){
    return{
        type:'DASHBOARD_DATA_ADD_PENDAPATAN_SUCCESS',
        total_pendapatan:total_pendapatan
    }
}


export function dashboardDataAddPendapatan(total_pendapatan) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(dashboardDataAddPendapatanSuccess(total_pendapatan)))
		})
	}
}

export function dashboardDataAddActivitySuccess(activity){
    return{
        type:'DASHBOARD_DATA_ADD_ACTIVITY_SUCCESS',
        activity:activity
    }
}


export function dashboardDataAddActivity(activity) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(dashboardDataAddActivitySuccess(activity)))
		})
	}
}

export function dashboardDataAddKlasifikasiSuccess(klasifikasi_pembelian){
    return{
        type:'DASHBOARD_DATA_ADD_KLASIFIKASI_SUCCESS',
        klasifikasi_pembelian:klasifikasi_pembelian
    }
}


export function dashboardDataAddKlasifikasi(klasifikasi_pembelian) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(dashboardDataAddKlasifikasiSuccess(klasifikasi_pembelian)))
		})
	}
}

export function dashboardFetchTotalUser() {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/dashboard/total_user`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			// const account = resp.data
            // const per_page = resp.per_page
            // const current_page = resp.current_page
            // const pagination = omit(resp, 'data')
			
			dispatch(dashboardDataAddUserSuccess(resp))
            // console.log(resp)
			// dispatch(accountsFetchPaginationSuccess(pagination))
			// // dispatch(candidatesFetchIds(ids))
			// dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}

export function dashboardFetchTotalToko() {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/dashboard/total_toko`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			// const account = resp.data
            // const per_page = resp.per_page
            // const current_page = resp.current_page
            // const pagination = omit(resp, 'data')
			
			dispatch(dashboardDataAddToko(resp))
			// dispatch(accountsFetchPaginationSuccess(pagination))
			// // dispatch(candidatesFetchIds(ids))
			// dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}

export function dashboardFetchTotalPendapatan() {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/dashboard/total_pendapatan`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			// const account = resp.data
            // const per_page = resp.per_page
            // const current_page = resp.current_page
            // const pagination = omit(resp, 'data')
			
			dispatch(dashboardDataAddPendapatan(resp))
			// dispatch(accountsFetchPaginationSuccess(pagination))
			// // dispatch(candidatesFetchIds(ids))
			// dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}

export function dashboardFetchActivity() {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/dashboard/activity`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			// const account = resp.data
            // const per_page = resp.per_page
            // const current_page = resp.current_page
            // const pagination = omit(resp, 'data')
			
			dispatch(dashboardDataAddActivity(resp))
			// dispatch(accountsFetchPaginationSuccess(pagination))
			// // dispatch(candidatesFetchIds(ids))
			// dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}


export function dashboardFetchKlasifikasi() {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/dashboard/klasifikasi_pembelian`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			console.log(resp)
			// const account = resp.data
            // const per_page = resp.per_page
            // const current_page = resp.current_page
            // const pagination = omit(resp, 'data')
			
			dispatch(dashboardDataAddKlasifikasi(resp))
			// dispatch(accountsFetchPaginationSuccess(pagination))
			// // dispatch(candidatesFetchIds(ids))
			// dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}