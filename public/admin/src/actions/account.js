import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function accountsAddDataSuccess(accounts){
    return{
        type:'ACCOUNTS_FETCH_DATA_SUCCESS',
        accounts
    }
}

export function accountsFetchPaginationSuccess(pagination) {
	return {
		type: 'ACCOUNTS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function accountAddFilterDataPageSuccess(page){
    return{
        type:'ACCOUNT_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function accountAddFilterDataPerPageSuccess(per_page){
    return{
        type:'ACCOUNT_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function accountAddFilterDataFilterSuccess(filter){
    return{
        type:'ACCOUNT_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function accountAddFilterDataOrderBySuccess(order_by){
    return{
        type:'ACCOUNT_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function accountAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(accountAddFilterDataPageSuccess(page)))
		})
	}
}


export function accountAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(accountAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function accountAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(accountAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function accountAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(accountAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function accountFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/admin/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const account = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(accountsAddDataSuccess(account))
			dispatch(accountsFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(accountAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}


export function userRegisterValidation(params){
    return{
        type:'ACCOUNT_REGISTER_ADD_ERROR_MESSAGE',
        error:params
    }
}

export function accountRegister(data,filter_data){
    return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/admin/register`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: data
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			
			if (response.data.status == 'error') {
                throw response
            }
			
			dispatch(accountFetchData(filter_data))
			dispatch(push("/admin/account"))
		})
		.catch((error) => {

            if (error.data.messages) {
                dispatch(userRegisterValidation(error.data.messages))
            }
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}