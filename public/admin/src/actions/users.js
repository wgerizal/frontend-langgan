import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import { push } from 'react-router-redux';
import store from '../helpers/user_session';
import omit from 'lodash/omit';
import {
globalUiLoginModal,
}from './globalUi'


export function userAskLoggedIn(params){
    return{
        type:'USER_ASK_TO_LOGGED_IN',
        params
    }
}

export function userResetValidation(params){
    return{
        type:'USER_AUTH_ADD_ERROR_MESSAGE',
        error:params
    }
}

export function userSubmitOtp(otp_user){
	return{
		type:'USER_SUBMIT_OTP',
		otp_user,
	}
}

export function userRegistration(params){
    return{
        type:'USER_NEW_REGISTERED',
        params
    }
}

export function successLogin(logged_in){
    return{
        type:'USER_AUTH_SUCCESS',
        logged_in:logged_in
    }
}

export function userCheckCredential(){
    return (dispatch) => {

		return new Promise((resolve, reject) => {
		
			_axios({
				url:`/api/user`,
				timeout: 20000,
				method: 'get',
                headers:{
                    'Authorization':`Bearer ${store.get()}`
                },
				responseType: 'json'
			})
			.then((response) => {
				let data = response.data
                dispatch(successLogin(true))
                if (data.message) {
                    throw response
                }
                let token_user = data.api_token
               
                
                // store.remove()
                // store.set(token_user)
			})
			.catch((error) => {
                dispatch(successLogin(false))
				dispatch(push('/auth/login'))
			})
		})
	}
}

export function userCheckHomePageCredentials(){
     return (dispatch) => {

		return new Promise((resolve, reject) => {
		
			_axios({
				url:`/api/user`,
				timeout: 20000,
				method: 'get',
                headers:{
                    'Authorization':`Bearer ${store.get()}`
                },
				responseType: 'json'
			})
			.then((response) => {
				let data = response.data
                dispatch(successLogin(true))
                if (data.message) {
                    throw response
                }
                
			})
			.catch((error) => {
                dispatch(successLogin(false))
				// dispatch(push('/'))
			})
		})
	}

}




export function userLogout(){
    return (dispatch) => {
        store.removeUserCredentials()
        store.remove()
        dispatch(successLogin(false))
        dispatch(push('/auth/login'))
	}
}


export function usersAddDataSuccess(users){
    return{
        type:'USERS_FETCH_DATA_SUCCESS',
        users
    }
}

export function usersFetchPaginationSuccess(pagination) {
	return {
		type: 'USERS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function userAddFilterDataPageSuccess(page){
    return{
        type:'USER_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function userAddFilterDataPerPageSuccess(per_page){
    return{
        type:'USER_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function userAddFilterDataFilterSuccess(filter){
    return{
        type:'USER_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function userAddFilterDataOrderBySuccess(order_by){
    return{
        type:'USER_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function userAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(userAddFilterDataPageSuccess(page)))
		})
	}
}


export function userAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(userAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function userAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(userAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function userAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(userAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function userFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/user/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const user = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(usersAddDataSuccess(user))
			dispatch(usersFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(userAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}