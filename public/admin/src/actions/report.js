import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'


export function reportDataSuccess(report){
    return{
        type:"REPORT_DATA_SUCCESS",
        report
    }
}

export function reportData(report) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(reportDataSuccess(report)))
		})
	}
}


export function reportGenerate(data,type){
    return (dispatch)=>{
        return new Promise((resolve,reject)=>{
            let sendData;
            if (type == 'domain') {
                 sendData = {
                    title:data.title,
                    type:type,
                    date_start:data.tanggal_min,
                    date_end:data.tanggal_max,
                }  
            }else{
                 sendData = {
                    title:data.title,
                    type:type,
                    date_start:data.tanggal_min,
                    date_end:data.tanggal_max,
                    toko_id:data.selected
                }  
            }
            

            _axios({
                url: `/api/report/generate`,
                timeout: 20000,
                method: 'post',
                responseType: 'json',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization':`Bearer ${store.get()}`
                    },
                data: sendData
            })
            .then((response) => {
                // dispatch(candidatesIsLoading(false))
                let resp = response.data.data
                let data_report = resp.data
                console.log(resp)
                // dispatch(reportData(data_report))
                resolve()
            })
            .catch((error) => {
                // dispatch(Notifications.error({
                //   title: 'Error',
                // 	message: error.response.data.message,
                // 	position: 'bl'
                // }))

                // dispatch(candidatesIsLoading(false))
                resolve()
                // console.error(error)
            })
        })
    }
}

export function reportFetchData( filter_data = Map({})) {
	return (dispatch) => {
	
		// dispatch(candidatesIsLoading(true))
        return new Promise((resolve, reject) => {

            _axios({
                url: `/api/report/filter`,
                timeout: 20000,
                method: 'get',
                responseType: 'json',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization':`Bearer ${store.get()}`
                    },
                data: filter_data.toJS()
            })
            .then((response) => {
                // dispatch(candidatesIsLoading(false))
                let resp = response.data.data
                let data_report = resp.data
                console.log(resp)
                dispatch(reportData(data_report))
                resolve()
            })
            .catch((error) => {
                // dispatch(Notifications.error({
                //   title: 'Error',
                // 	message: error.response.data.message,
                // 	position: 'bl'
                // }))

                // dispatch(candidatesIsLoading(false))
                resolve()
                console.error(error)
            })
			
		})
	}
}