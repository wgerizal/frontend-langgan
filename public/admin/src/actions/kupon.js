import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function kuponsAddDataSuccess(kupons){
    return{
        type:'KUPONS_FETCH_DATA_SUCCESS',
        kupons
    }
}

export function kuponsFetchPaginationSuccess(pagination) {
	return {
		type: 'KUPONS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function kuponAddFilterDataPageSuccess(page){
    return{
        type:'KUPON_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function kuponsAddFilterDataPerPageSuccess(per_page){
    return{
        type:'KUPON_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function kuponAddFilterDataFilterSuccess(filter){
    return{
        type:'KUPON_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function kuponAddFilterDataOrderBySuccess(order_by){
    return{
        type:'KUPON_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function kuponAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(kuponAddFilterDataPageSuccess(page)))
		})
	}
}


export function kuponsAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(kuponsAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function kuponAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(kuponAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function kuponAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(kuponAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function kuponFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/kupon/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const kupon = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(kuponsAddDataSuccess(kupon))
			dispatch(kuponsFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(kuponsAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}


export function kuponGenerateData( data,filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))
        
		return _axios({
			url: `/api/kupon/generate`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: data
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
            dispatch(kuponFetchData(filter_data))
			
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}