import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function activitysAddDataSuccess(activitys){
    return{
        type:'ACTIVITYS_FETCH_DATA_SUCCESS',
        activitys
    }
}

export function activitysFetchPaginationSuccess(pagination) {
	return {
		type: 'ACTIVITYS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function activityAddFilterDataPageSuccess(page){
    return{
        type:'ACTIVITY_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function activityAddFilterDataPerPageSuccess(per_page){
    return{
        type:'ACTIVITY_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function activityAddFilterDataFilterSuccess(filter){
    return{
        type:'ACTIVITY_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function activityAddFilterDataOrderBySuccess(order_by){
    return{
        type:'ACTIVITY_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function activityAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(activityAddFilterDataPageSuccess(page)))
		})
	}
}


export function activityAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(activityAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function activityAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(activityAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function activityAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(activityAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function activityFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/activity/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const activity = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(activitysAddDataSuccess(activity))
			dispatch(activitysFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(activityAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}