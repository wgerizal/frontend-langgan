import Promise from 'bluebird'
import _axios from '../dependencies/_axios'
import omit from 'lodash/omit'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'

export function billingsAddDataSuccess(billings){
    return{
        type:'BILLINGS_FETCH_DATA_SUCCESS',
        billings
    }
}

export function billingsFetchPaginationSuccess(pagination) {
	return {
		type: 'BILLINGS_FETCH_PAGINATION_SUCCESS',
		pagination
	}
}

export function billingAddFilterDataPageSuccess(page){
    return{
        type:'BILLING_FILTER_DATA_ADD_PAGE_SUCCESS',
        page:page
    }
}


export function billingAddFilterDataPerPageSuccess(per_page){
    return{
        type:'BILLING_FILTER_DATA_ADD_PER_PAGE_SUCCESS',
        per_page:per_page
    }
}


export function billingAddFilterDataFilterSuccess(filter){
    return{
        type:'BILLING_FILTER_DATA_ADD_FILTER_SUCCESS',
        filter:filter
    }
}


export function billingAddFilterDataOrderBySuccess(order_by){
    return{
        type:'BILLING_FILTER_DATA_ADD_ORDER_BY_SUCCESS',
        order_by:order_by
    }
}


export function billingAddFilterDataPage(page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(billingAddFilterDataPageSuccess(page)))
		})
	}
}


export function billingAddFilterDataPerPage(per_page) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(billingAddFilterDataPerPageSuccess(per_page)))
		})
	}
}


export function billingAddFilterDataFilter(filter) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(billingAddFilterDataFilterSuccess(filter)))
		})
	}
}


export function billingAddFilterDataOrderBy(order_by) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			resolve(dispatch(billingAddFilterDataOrderBySuccess(order_by)))
		})
	}
}


export function billingFetchData( filter_data = Map({})) {
	return (dispatch, getState) => {
	
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/billing/filter`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: filter_data.toJS()
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			let resp = response.data.data
			const billing = resp.data
            const per_page = resp.per_page
            const current_page = resp.current_page
            const pagination = omit(resp, 'data')
			
			dispatch(billingsAddDataSuccess(billing))
			dispatch(billingsFetchPaginationSuccess(pagination))
			// dispatch(candidatesFetchIds(ids))
			dispatch(billingAddFilterDataPerPageSuccess(per_page))
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}
export function completeBilling( id,filter_data) {
	return (dispatch, getState) => {
		// dispatch(candidatesIsLoading(true))

		return _axios({
			url: `/api/billing/update/status`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization':`Bearer ${store.get()}`
                },
			data: {id:id,status:3}
		})
		.then((response) => {
			// dispatch(candidatesIsLoading(false))
			
			
			dispatch(billingFetchData(filter_data))
			
		})
		.catch((error) => {
			// dispatch(Notifications.error({
			//   title: 'Error',
			// 	message: error.response.data.message,
			// 	position: 'bl'
			// }))

			// dispatch(candidatesIsLoading(false))

			console.error(error)
		})
	}
}