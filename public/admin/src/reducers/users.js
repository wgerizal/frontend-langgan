import { List, Map, fromJS } from 'immutable'

const initFilterUser = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:'desc'
})


export function userFilterData(state = initFilterUser, action) {
	switch(action.type) {
		case 'USER_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'USER_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'USER_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'USER_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function users(state = List([]), action) {
	switch (action.type) {
		case 'USERS_FETCH_DATA_SUCCESS':
			return fromJS(action.users)
		default:
			return state
	}
}

export function usersPagination(state = Map({}), action) {
	switch(action.type) {
		case 'USERS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}
