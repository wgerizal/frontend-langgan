import { List, Map, fromJS } from 'immutable'

const initFilterShop = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:'desc'
})


export function shopFilterData(state = initFilterShop, action) {
	switch(action.type) {
		case 'SHOP_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'SHOPG_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'SHOP_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'SHOP_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function shops(state = List([]), action) {
	switch (action.type) {
		case 'SHOPS_FETCH_DATA_SUCCESS':
			return fromJS(action.shops)
		default:
			return state
	}
}

export function shopsPagination(state = Map({}), action) {
	switch(action.type) {
		case 'SHOPS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}
