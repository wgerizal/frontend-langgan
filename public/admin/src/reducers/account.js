import { List, Map, fromJS } from 'immutable'

const initFilterAccount= fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:'desc'
})

export function registerAdminError(state = Map({}), action) {
	switch(action.type) {
		case 'ACCOUNT_REGISTER_ADD_ERROR_MESSAGE':
			return fromJS(action.error)
		default:
			return state
	}
}

export function accountFilterData(state = initFilterAccount, action) {
	switch(action.type) {
		case 'ACCOUNT_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'ACCOUNT_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'ACCOUNT_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'ACCOUNT_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function accounts(state = List([]), action) {
	switch (action.type) {
		case 'ACCOUNTS_FETCH_DATA_SUCCESS':
			return fromJS(action.accounts)
		default:
			return state
	}
}

export function accountsPagination(state = Map({}), action) {
	switch(action.type) {
		case 'ACCOUNTS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}
