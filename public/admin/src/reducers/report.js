import { List, Map, fromJS } from 'immutable'

const initFilterShop = fromJS({
	page: 1,
    per_page:'',
    order_by:'desc'
})


export function reportFilterData(state = initFilterShop, action) {
	switch(action.type) {
		case 'REPORT_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'REPORT_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'REPORT_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function reports(state = List([]), action) {
	switch (action.type) {
		case 'REPORT_DATA_SUCCESS':
			return fromJS(action.report)
		default:
			return state
	}
}

