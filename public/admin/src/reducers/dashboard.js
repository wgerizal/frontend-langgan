import { List, Map, fromJS } from 'immutable'

const initDataDashboard = fromJS({
	total_user: 0,
    total_toko:0,
    total_pendapatan:0,
    activity:[],
    klasifikasi_pembelian:[],
})


export function dashboardData(state = initDataDashboard, action) {
	switch(action.type) {
		case 'DASHBOARD_DATA_ADD_USER_SUCCESS':
			return state.set('total_user', action.total_user)
        case 'DASHBOARD_DATA_ADD_TOKO_SUCCESS':
            return state.set('total_toko', action.total_toko)
        case 'DASHBOARD_DATA_ADD_PENDAPATAN_SUCCESS':
            return state.set('total_pendapatan', action.total_pendapatan)
        case 'DASHBOARD_DATA_ADD_ACTIVITY_SUCCESS':
            return state.set('activity', List(action.activity))
        case 'DASHBOARD_DATA_ADD_KLASIFIKASI_SUCCESS':
            return state.set('klasifikasi_pembelian', List(action.klasifikasi_pembelian))
		default:
			return state
	}
}