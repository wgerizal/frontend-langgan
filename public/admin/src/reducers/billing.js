import { List, Map, fromJS } from 'immutable'

const initFilterBilling = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:'desc'
})


export function billingFilterData(state = initFilterBilling, action) {
	switch(action.type) {
		case 'BILLING_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'BILLING_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'BILLING_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'BILLING_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function billings(state = List([]), action) {
	switch (action.type) {
		case 'BILLINGS_FETCH_DATA_SUCCESS':
			return fromJS(action.billings)
		default:
			return state
	}
}

export function billingsPagination(state = Map({}), action) {
	switch(action.type) {
		case 'BILLINGS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}
