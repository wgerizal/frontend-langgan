import { combineReducers } from "redux";
import { routerReducer as routing } from "react-router-redux";
import { reducer as notifications } from 'react-notification-system-redux'

import {
    loginUi,
    registerError,
    otp,
}from './globalUi'

import{
  loginError,
}from './login'

import{
  dashboardData
}from './dashboard'

import{
  activityFilterData,
  activitys,
  activitysPagination,
}from './activity'

import{
  accountFilterData,
  accounts,
  accountsPagination,
  registerAdminError,
}from './account'

import{
  userFilterData,
  users,
  usersPagination,
  
}from './users'


import{
  billingFilterData,
  billings,
  billingsPagination,
  
}from './billing'


import{
  shopFilterData,
  shops,
  shopsPagination,
  
}from './shop'


import{
  kuponFilterData,
  kupons,
  kuponsPagination,
  
}from './kupon'

import{
reportFilterData,
reports,
} from './report'

import{
    user
}from './auth'

export default combineReducers({
  routing,
  loginUi,
  registerError,
  notifications,
  user,
  activityFilterData,
  activitys,
  activitysPagination,
  accountFilterData,
  accounts,
  accountsPagination,
  registerAdminError,
  userFilterData,
  users,
  usersPagination,
  billingFilterData,
  billings,
  billingsPagination,
  dashboardData,
  shopFilterData,
  shops,
  shopsPagination,
  kuponFilterData,
  kupons,
  kuponsPagination,
  reportFilterData,
  reports,
  otp,
  loginError,
});