import { List, Map, fromJS } from 'immutable'

const initUser = Map({
	logged_in: false,
	email:'',
	otp:'',
	errorOtp:'',
})

export function user(state = initUser, action) {
	switch(action.type) {
		case 'USER_AUTH_SUCCESS':
			return state.set('logged_in', action.logged_in)
		case 'USER_AUTH_SAVE_EMAIL_SUCCESS':
			return state.set('email', action.email)	
		case 'USER_AUTH_SAVE_OTP_SUCCESS':
			return state.set('otp', action.otp)	
		case 'OTP_ERROR_SUCCESS':
			return state.set('errorOtp',action.errorOtp)
		default:
			return state
	}
}
