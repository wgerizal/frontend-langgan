import { List, Map, fromJS } from 'immutable'

const initFilterKupon = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:'desc'
})


export function kuponFilterData(state = initFilterKupon, action) {
	switch(action.type) {
		case 'KUPON_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'KUPON_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'KUPON_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'KUPON_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function kupons(state = List([]), action) {
	switch (action.type) {
		case 'KUPONS_FETCH_DATA_SUCCESS':
			return fromJS(action.kupons)
		default:
			return state
	}
}

export function kuponsPagination(state = Map({}), action) {
	switch(action.type) {
		case 'KUPONS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}
