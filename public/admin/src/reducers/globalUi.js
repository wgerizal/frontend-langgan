import { List, Map, fromJS } from 'immutable'

const initUiData = Map({
	login_modal: false,
	register_modal:false,
})

const initRegisterError = Map({
	error:[]
})


const initOtpUi = fromJS({
	otp_page: false,
    
})


export function otp(state = initUiData, action) {
	switch(action.type) {
		case 'OTP_PAGE_OPEN_SUCCESS':
			return state.set('otp_page', action.otp_page)
		default:
			return state
	}
}


export function loginUi(state = initUiData, action) {
	switch(action.type) {
		case 'LOGIN_MODAL_SUCCESS':
			return state.set('login_modal', action.login_modal)
		case 'REGISTER_MODAL_SUCCESS':
			return state.set('register_modal', action.register_modal)
		default:
			return state
	}
}

export function registerError(state = initRegisterError, action){
	switch(action.type) {
		case 'REGISTER_ERROR_CHECK_SUCCESS':
			return state.set('error', action.error)
		default:
			return state
	}

}
