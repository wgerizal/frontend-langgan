import { List, Map, fromJS } from 'immutable'

const initUser = Map({
	logged_in: false,
})

export function loginError(state = Map({}), action) {
	switch(action.type) {
		case 'USER_AUTH_ADD_ERROR_MESSAGE':
			return fromJS(action.error)
		default:
			return state
	}
}
