import { List, Map, fromJS } from 'immutable'

const initFilterActivity = fromJS({
	page: 1,
    per_page:1,
    filter:'',
    order_by:'desc'
})


const initFilterBilling = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:''
})


const initFilterUser = fromJS({
	page: 1,
    per_page:'',
    filter:'',
    order_by:''
})


export function activityFilterData(state = initFilterActivity, action) {
	switch(action.type) {
		case 'ACTIVITY_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'ACTIVITY_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'ACTIVITY_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'ACTIVITY_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function activitys(state = List([]), action) {
	switch (action.type) {
		case 'ACTIVITYS_FETCH_DATA_SUCCESS':
			return fromJS(action.activitys)
		default:
			return state
	}
}

export function activitysPagination(state = Map({}), action) {
	switch(action.type) {
		case 'ACTIVITYS_FETCH_PAGINATION_SUCCESS':
			return fromJS(action.pagination)
		default:
			return state
	}
}

export function billingFilterData(state = initFilterBilling, action) {
	switch(action.type) {
		case 'BILLING_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'BILLING_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'BILLING_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'BILLING_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}

export function userFilterData(state = initFilterUser, action) {
	switch(action.type) {
		case 'USER_FILTER_DATA_ADD_PAGE_SUCCESS':
			return state.set('page', action.page)
        case 'USER_FILTER_DATA_ADD_PER_PAGE_SUCCESS':
            return state.set('per_page', action.per_page)
        case 'USER_FILTER_DATA_ADD_FILTER_SUCCESS':
            return state.set('filter', action.filter)
        case 'USER_FILTER_DATA_ADD_ORDER_BY_SUCCESS':
            return state.set('order_by', action.order_by)
		default:
			return state
	}
}
