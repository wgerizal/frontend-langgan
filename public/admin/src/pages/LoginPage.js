import React,{Fragment} from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import bindAll from 'lodash/bindAll'
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import OtpPage from './partials/OtpPage'

import {
  globalUiLoginModal,
  globalUiRegisterModal,
}from '../actions/globalUi'

import {
  userAskLoggedIn,
  userSubmitOtp,
  userCheckHomePageCredentials,
  userResetValidation,
}from '../actions/users'



function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Langgan
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = theme => ({
  root: {
    height: '100vh',
    '& label.Mui-focused': {
      color: '#009688',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#009688',
    },
    '& .MuiOutlinedInput-root': {
      // '& fieldset': {
      //   borderColor: 'red',
      // },
      // '&:hover fieldset': {
      //   borderColor: 'yellow',
      // },
      '&.Mui-focused fieldset': {
        borderColor: '#009688',
      },
    },
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    color:'black'
  },
});


class LoginPage extends React.Component{
    
     constructor(props) {
		super(props)
		bindAll(this, [
            'handleInputChange',
            'handleSubmitLogin',
            'OpenModalRegister',
            'handleOtpSubmit',
            'handleOtpChange',
		])

        this.state = {
			      email: '',
            password: '',
            checkbox:true,
            role:'admin',
            otp_number:'',
		}

		
	}

  OpenModalRegister(e){
        const{
        globalUiRegisterModal,
        globalUiLoginModal
        }=this.props
        globalUiLoginModal(false)
        // console.log('hitted')
        globalUiRegisterModal(true)

    }

    handleOtpChange(e){
      this.setState({otp_number:e.target.value})
    }

    handleOtpSubmit(e,value){
      const{userSubmitOtp}=this.props
      // console.log(this.state.otp_number)
      userSubmitOtp(this.state.otp_number)
    }

    handleSubmitLogin(e){
        const{
            userAskLoggedIn
        }=this.props
        
        
        userAskLoggedIn(this.state)
    }
    
    handleInputChange(e){
      const{
        userResetValidation
      }=this.props
        if (e.target.name=='checkbox') {
            return this.setState({[e.target.name]:!this.state.checkbox})
        }
        
        this.setState({[e.target.name]:e.target.value},
        ()=>userResetValidation({})
        )
    }
    render(){
        const { classes,otp ,loginError, user} = this.props;
        const{
            email,
            password,
            checkbox,
        }=this.state
        let page;
        if (otp.get('otp_page')==true) {
          page = <OtpPage errorOtp={user.get('errorOtp')} valueOtp={this.state.otp_number} handleOtpChange={this.handleOtpChange} handleOtpSubmit={this.handleOtpSubmit}/>
        }else{
          page = 
           <Grid container component="main" className={classes.root}>
            
                <CssBaseline />
                <Grid item xs={false} sm={4} md={7} className={classes.image} />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <div className={classes.form} noValidate>
                        <TextField
                        error= {loginError.get('email')? 
                             true
            
                            :
                                false
                            }
                        variant="outlined"
                        margin="normal"
                        className="custom-input-admin"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        onChange={this.handleInputChange}
                        value={email}
                        autoFocus
                        />

                        {loginError.get('email')? 
                             <span className="span-error">{loginError.get('email')}</span>
            
                            :
                                null
                            }
                    
                        <TextField
                        error= {loginError.get('password')? 
                             true
            
                            :
                                false
                            }
                        variant="outlined"
                        margin="normal"
                        required
                        className="custom-input-admin"
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={this.handleInputChange}
                        value={password}
                        />
                         {loginError.get('password')? 
                             <span className="span-error">{loginError.get('password')}</span>
            
                            :
                                null
                            }
                       
                        <br/>
                        <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                        />
                        <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={this.handleSubmitLogin}
                        >
                        Sign In
                        </Button>
                        <Grid container>
                        {/* <Grid item xs>
                            <Link href="#" variant="body2">
                            Forgot password?
                            </Link>
                        </Grid> */}
                        {/* <Grid item>
                            <Link href="#" onClick={this.OpenModalRegister} variant="body2">
                            {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid> */}
                        </Grid>
                        <Box mt={5}>
                        <Copyright />
                        </Box>
                    </div>
                    </div>
                </Grid>
                </Grid>
        }
        return(
         <Fragment>
           {page}
         </Fragment>
         
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
		loginUi: state.loginUi,
    otp: state.otp,
    loginError: state.loginError,
    user:state.user
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    globalUiLoginModal:(modal)=>dispatch(globalUiLoginModal(modal)),
    globalUiRegisterModal:(modal)=>dispatch(globalUiRegisterModal(modal)),
    userAskLoggedIn:(data)=>dispatch(userAskLoggedIn(data)),
    userCheckHomePageCredentials:()=>dispatch(userCheckHomePageCredentials()),
    userSubmitOtp:(otp)=>dispatch(userSubmitOtp(otp)),
    userResetValidation:(params)=>dispatch(userResetValidation(params)),
	}
}

export default compose(withStyles(useStyles),
connect(mapStateToProps, mapDispatchToProps)
)(LoginPage)