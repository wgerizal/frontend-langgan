import React,{Component} from 'react';
import store from '../helpers/user_session'
import LangganLogo from '../styles/img/logo-langgan.png';
import HunLogo from '../styles/img/hun.png';

class MemberPaket extends Component{

    render(){
        const user_email = store.getUserEmail()
        return(
                <div>
                <header>
                        <nav>
                            <div className="line">
                            <div className="s-12 l-2 p-t-20">
                                <img className="s-5 l-12 center" src={LangganLogo}/>
                            </div>
                            <div className="top-nav s-12 l-10 right">
                                <ul className="right">
                                    <li><a>Toko Saya</a></li>
                                    <li><a>Buat Toko</a></li>
                                    <li><a className="color-langgan"><i className="fa fa-envelope"></i></a></li>
                                    <li><a className="color-langgan"><i className="fa fa-user-circle"></i> {user_email} <i className="fa fa-caret-down"></i></a></li>
                                </ul>
                            </div>
                            </div>
                        </nav>
                    </header>
                    <section className="text-center">
                        {/* <!-- SECOND BLOCK --> */}
                        <div id="tema-detail-block">
                            <div className="line">
                            <div className="margin-bottom">
                                <div className="margin">
                                    <div className="s-12 m-12 l-9 center div-nav-tab">
                                        <div>
                                        <ol className="ProgressBar">
                                            <li className="ProgressBar-step">
                                            <svg className="ProgressBar-icon"><use xlinkHref="#checkmark-bold"/></svg>
                                            <span className="ProgressBar-stepLabel">Pilih Skema</span>
                                            </li>
                                            <li className="ProgressBar-step">
                                            <svg className="ProgressBar-icon"><use xlinkHref="#checkmark-bold"/></svg>
                                            <span className="ProgressBar-stepLabel">Nama Toko</span>
                                            </li>
                                            <li className="ProgressBar-step">
                                            <svg className="ProgressBar-icon"><use xlinkHref="#checkmark-bold"/></svg>
                                            <span className="ProgressBar-stepLabel">Nama Domain</span>
                                            </li>
                                            <li className="ProgressBar-step">
                                            <svg className="ProgressBar-icon"><use xlinkHref="#checkmark-bold"/></svg>
                                            <span className="ProgressBar-stepLabel">Pilih Tema</span>
                                            </li>
                                            <li className="ProgressBar-step">
                                            <svg className="ProgressBar-icon"><use xlinkHref="#checkmark-bold"/></svg>
                                            <span className="ProgressBar-stepLabel">Checkout</span>
                                            </li>
                                        </ol>
                                        </div>
                                        <div className="flex-container flex-between">
                                        <div className="div-paket border-starter">
                                            <div className="starter-label vertical-align-center">Starter</div>
                                            <div className="text-left list-paket">
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                            </div>
                                            <div>Starter</div>
                                        </div>
                                        <div className="div-paket border-premium">
                                            <div className="premium-label vertical-align-center">Premium</div>
                                            <div className="text-left list-paket">
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                            </div>
                                            <div>Premium</div>
                                        </div>
                                        <div className="div-paket border-enterprise">
                                            <div className="enterprise-label vertical-align-center">Enterprise</div>
                                            <div className="text-left list-paket">
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                                <p className="m-b-10">Starter</p>
                                            </div>
                                            <div>Enterprise</div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </section>
                    {/* <!-- FOOTER --> */}
                    <footer>
                        <div className="line">
                            <div className="s-12 l-4">
                            <p>Langgan</p>
                            <p>Graha DLA, Lantai 2 - Suite 06 Jl. Otto Iskandar Dinata No. 392 Bandung 40242</p>
                            <p>+62 811 2481 110 / hello@langgan.id</p>
                            </div>
                            <div className="s-12 l-4 text-center">
                            FAQ
                            </div>
                            <div className="s-12 l-4">
                            Subscribe untuk mengikuti berita paling mutakhir
                            </div>
                        </div>
                        <hr />
                        <div className="line">
                            <div className="s-12 l-6">
                            <p>© 2019 PT. Berlangganan Indonesia Global</p>
                            </div>
                            <div className="s-12 l-6 text-right">
                            Syarat & Ketentuan
                            </div>
                        </div>
                    </footer>

            </div>

            )
        }
    }

export default MemberPaket