import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { push } from 'react-router-redux';
import {
  reportGenerate,
} from '../../actions/report'
import {
  shopFetchData,
  shopAddFilterDataFilter,
} from '../../actions/shop'



class AddReportToko extends Component{
     constructor(props) {
        super(props);
        this.state={
          title:'',
          tanggal_min:'',
          tanggal_max:'',
          selected:'',
        }
        bindAll(this, [
        	'handleSubmit',
          'handleOnChange',
        ])
      } 

    handleOnChange(e){
      this.setState({[e.target.name]:e.target.value})
    }    

    handleSubmit(){
      const{
        reportGenerate,
        redirectTo,
      }=this.props
      reportGenerate(this.state,'toko')
      .then(()=>{
        redirectTo('/admin/reports')
      })
    }

    componentDidMount(){
      const{
        shopFetchData,
        shopFilterData,
        
      }=this.props

      shopFetchData(shopFilterData)
    }

    render(){
      const{
      usersPagination,
      users,
      userFilterData,
      userAddFilterDataPage,
      userAddFilterDataPerPage,
      userFetchData,
      registerAdminError,
      shops,
      }=this.props
        
        let tokoOption=[]
        tokoOption.push(
           <option value="" disabled>Pilih Toko</option>
        )
        shops.map((val,index)=>{
          tokoOption.push(
            <option value={val.get('id')} >{val.get('name')}</option>
          )
        })
       
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Report Transaksi Toko
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12 div-table-reports">
                        <article className="s-12 m-12 l-7 center">
                          <h2 className="margin-bottom">Report Transaksi Toko</h2>
                          <div className="div-reset-pass">
                            <div className="flex-container flex-between">Pilih Toko 
                            <select onChange={this.handleOnChange} defaultValue={this.state.selected} name='selected' className="input-reset-password">
                              {tokoOption}
                            </select>
                            </div>
                            
                            <div className="flex-container flex-between">Title Report <input className="input-reset-password" type="text" name="title" onChange={this.handleOnChange} placeholder=""></input></div>
                            
                            <div className="flex-container flex-between">Tanggal <div className="flex-container flex-between"><input style={{width: '145px'}} className="input-reset-password" type="date" onChange={this.handleOnChange} name="tanggal_min"></input><input style={{width: '145px'}} className="input-reset-password" type="date" onChange={this.handleOnChange} name="tanggal_max"></input></div></div>
                          </div>
                          <div><button onClick={this.handleSubmit} className="btn btn-primary" type="submit">Generate</button></div>
                        </article>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    shops:state.shops,
    shopFilterData:state.shopFilterData,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
   reportGenerate:(data,type)=>(dispatch(reportGenerate(data,type))),
   redirectTo:(link)=>(dispatch(push(link))),
   shopFetchData:(filterData)=>dispatch(shopFetchData(filterData)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddReportToko)