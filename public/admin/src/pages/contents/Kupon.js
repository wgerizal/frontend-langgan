import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import KuponListItem from '../partials/KuponListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'
import {
  kuponFetchData,
  kuponsAddFilterDataPerPage,
  kuponAddFilterDataPage,
  kuponGenerateData,
  kuponAddFilterDataFilter,
} from '../../actions/kupon'

class Kupon extends Component{
     constructor(props) {
        super(props);

        this.state={
            expired:'',
            amount:'',
            code:'',
        }
        bindAll(this, [
        	'handleInputChange',
            'handleGenerate',
            'handleInputDate',
            'handleSearch',
        ])
      }

      handleSearch(e){
        const{
          kuponAddFilterDataFilter,
          kuponFilterData,
          kuponFetchData,
        }=this.props
        const value = e.target.value
  
        setTimeout(()=>{
          if (value.trim().length > 2) {
 
            kuponAddFilterDataFilter(value)
            .then(()=>{
              kuponFetchData(this.props.kuponFilterData)
            })
          }else if (value.trim().length==0) {
            kuponAddFilterDataFilter('')
            .then(()=>{
              kuponFetchData(this.props.kuponFilterData)
            })
          }
        },500)
    }

   handleInputDate(date){
     this.setState({expired:moment(date).format('YYYY-MM-DD HH:mm')})
   }
   handleGenerate(e){
       const{
           kuponGenerateData,
            kuponFilterData,
       }=this.props
       kuponGenerateData(this.state, kuponFilterData)
   }

    handleInputChange(e){
        this.setState({[e.target.name]:e.target.value})
    }

    componentDidMount(){
      const{
        kuponFetchData,
        kuponFilterData,
        kuponsAddFilterDataPerPage,
        kuponAddFilterDataPage,
      }=this.props

      kuponFetchData(kuponFilterData)
    }
    render(){
      const{
      kuponsPagination,
      kupons,
      kuponFilterData,
      kuponAddFilterDataPage,
      kuponsAddFilterDataPerPage,
      kuponFetchData,
      }=this.props
  
    const{
        expired,
        amount,
        code
    }=this.state
        let kuponPagination=''
        if (!this.props.kupons.isEmpty()) {
          kuponPagination =
            <Pagination
              pagination={kuponsPagination}
              addPageNumber={kuponAddFilterDataPage}
              addPerPageSize={kuponsAddFilterDataPerPage}
              fetchData={kuponFetchData}
              filterData={kuponFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Kupon
              </h1>
              <div id="example1_filter" className="dataTables_filter">
                    <label><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
            </section>

            {/* Main content - code */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  
                  <div className="box-body">
                    <div className="row">
                      <div className="col-md-3">
                        <label>
                          Kode Kupon
                        </label>
                        <input 
                          value={code}
                          name='code'
                          type='text'
                          className="form-control"
                          onChange={this.handleInputChange}
                        />
                      </div>

                      <div className="col-md-3">
                        <label>
                          Jumlah
                        </label>
                        <input 
                          value={amount}
                          name='amount'
                          type='text'
                          className="form-control"
                          onChange={this.handleInputChange}
                        />
                      </div>

                      <div className="col-md-3">
                        <label>
                          Masa Berlaku
                        </label>

                        <DatePicker
                          name="expired"
                          onChange={(date)=>this.handleInputDate(date)}
                          value={this.state.expired  == '' ? null: moment(this.state.expired).format('MMM, DD YYYY HH:mm')}
                          className="form-control"
                          style={{height: "30px"}}
                          showTimeSelect
                          timeFormat="HH:mm"
                          timeIntervals={5}
                          timeCaption="time"
                        />
                      </div>

                      <div className="col-md-3 p-18">
                        <button onClick={this.handleGenerate} className="btn btn-primary"> Generate </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <KuponListItem activityList={kupons}/>
                      </div>
                    </div>
                    <div className="row">
                       {kuponPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    kupons:state.kupons,
    kuponFilterData:state.kuponFilterData,
    kuponsPagination:state.kuponsPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    kuponFetchData:(filterData)=>dispatch(kuponFetchData(filterData)),
    kuponsAddFilterDataPerPage:(per_page)=>dispatch(kuponsAddFilterDataPerPage(per_page)),
    kuponAddFilterDataPage:(page)=>dispatch(kuponAddFilterDataPage(page)),
    kuponGenerateData:(data,filterData)=>dispatch(kuponGenerateData(data,filterData)),
    kuponAddFilterDataFilter:(filter)=>dispatch(kuponAddFilterDataFilter(filter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Kupon)