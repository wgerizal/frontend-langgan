import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import { connect } from 'react-redux'
import {
  dashboardFetchTotalUser,
  dashboardFetchTotalToko,
  dashboardFetchTotalPendapatan,
  dashboardFetchActivity,
  dashboardFetchKlasifikasi,
} from '../../actions/dashboard'
import DoughnutChart from '../partials/DoughnutChart'

class Dashboard extends Component{

    componentDidMount(){
      const{
        dashboardFetchTotalUser,
        dashboardFetchTotalToko,
        dashboardFetchTotalPendapatan,
        dashboardFetchActivity,
        dashboardFetchKlasifikasi,
      }=this.props
      dashboardFetchTotalUser()
      .then(()=>{
        dashboardFetchTotalToko()
      })
      .then(()=>{
        dashboardFetchTotalPendapatan()
      })
      .then(()=>{
        dashboardFetchActivity()
      })
      .then(()=>{
        dashboardFetchKlasifikasi()
      })
    }

    render(){
      const{
        dashboardData
      }=this.props
      console.log(dashboardData.toJS())
      let klasifikasi_pembeli_data = dashboardData.get('klasifikasi_pembelian').toJS()
      console.log(klasifikasi_pembeli_data)
      // let klasifikasi_pembeli_data = [
      //   {label:'kelapa', value:40},
      //   {label:'muda', value:60},
      // ]
      let renderActivity;
     renderActivity = dashboardData.get('activity').map((activity,index)=>
                 <li key={index}>
                    <div className="flex-container flex-between">
                      <div>
                        {/* <img src={require('../../styles/img/list1.png')} /> */}
                        <span className="text">{activity.email}</span>
                      </div>
                      <div>
                        <span className="text">{activity.detail}</span>
                      </div>
                    </div>
                 </li>
      )
        return(
            <Fragment>
             <section className="content-header">
              <label style={{color: '#9191AF',fontSize: '15px', fontWeight: 'normal', textTransform: 'uppercase'}}>
                Overview
              </label>
            </section>

            {/* Main content */}
            <section className="content">
              {/* Small boxes (Stat box) */}
              <div className="row">
                <div className="col-lg-4 col-xs-6">
                  {/* small box */}
                  <div className="small-box bg-white">
                    <div className="inner">
                      <p>TOTAL USER BARU MINGGU INI</p>
                      <h3>{dashboardData.get('total_user')}</h3>
                    </div>
                    <div className="icon">
                      <i className="ion ion-bag" />
                    </div>
                  </div>
                </div>{/* ./col */}
                <div className="col-lg-4 col-xs-6">
                  {/* small box */}
                  <div className="small-box bg-white">
                    <div className="inner">
                      <p>TOTAL TOKO BARU MINGGU INI</p>
                      <h3>{dashboardData.get('total_toko')}</h3>
                    </div>
                    <div className="icon">
                      <i className="ion ion-stats-bars" />
                    </div>
                  </div>
                </div>{/* ./col */}
                <div className="col-lg-4 col-xs-6">
                  {/* small box */}
                  <div className="small-box bg-white">
                    <div className="inner">
                      <p>TOTAL PENDAPATAN MINGGU INI</p>
                      <h3>{dashboardData.get('total_pendapatan')}</h3>
                    </div>
                    <div className="icon">
                      <i className="ion ion-person-add" />
                    </div>
                  </div>
                </div>{/* ./col */}
              </div>{/* /.row */}
              {/* Main row */}
              <div className="row">
                {/* Left col */}
                <section className="col-lg-7 connectedSortable">
                  {/* TO DO List */}
                  <div className="box box-primary">
                    <div className="box-header">
                      <i className="ion ion-clipboard" />
                      <h3 className="box-title">ACTIVITY</h3>
                      <div className="box-tools pull-right">
                        <i className="fa fa-ellipsis-h"></i>
                      </div>
                    </div>{/* /.box-header */}
                    <div className="box-body">
                      <ul className="todo-list">
                       {renderActivity}
                  
                      </ul>
                    </div>{/* /.box-body */}
                    <div className="box-footer clearfix no-border">
                      <button className="btn btn-default btn-load-more">Load more</button>
                    </div>
                  </div>{/* /.box */}
                </section>{/* /.Left col */}
                {/* right col (We are only adding the ID to make the widgets sortable)*/}
                <section className="col-lg-5 connectedSortable">
                  {/* solid sales graph */}
                  <div className="box box-solid">
                    <div className="box-header">
                      <h3 className="box-title">PEMBELIAN PAKET MINGGU INI</h3>
                    </div>
                    <div className="box-body border-radius-none">
                      <DoughnutChart
                        data={klasifikasi_pembeli_data}
                        title={'Harga kelapa'}
                        colors={['#C75CDF', '#2CC6AF', '#5997F1']}
                      />
                      {/* <div className="chart" id="line-chart" style={{height: '250px'}} /> */}
                    </div>{/* /.box-body */}
                    <div className="box-footer no-border">
                      <div className="row">
                      </div>{/* /.row */}
                    </div>{/* /.box-footer */}
                  </div>{/* /.box */}
                </section>{/* right col */}
              </div>{/* /.row (main row) */}
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    dashboardData:state.dashboardData,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    dashboardFetchTotalUser:()=>dispatch(dashboardFetchTotalUser()),
    dashboardFetchTotalToko:()=>dispatch(dashboardFetchTotalToko()),
    dashboardFetchTotalPendapatan:()=>dispatch(dashboardFetchTotalPendapatan()),
    dashboardFetchActivity:()=>dispatch(dashboardFetchActivity()),
    dashboardFetchKlasifikasi:()=>dispatch(dashboardFetchKlasifikasi()),
    // activityAddFilterDataPerPage:(per_page)=>dispatch(activityAddFilterDataPerPage(per_page)),
    // activityAddFilterDataPage:(page)=>dispatch(activityAddFilterDataPage(page)),
    // activityAddFilterDataFilter:(filter)=>dispatch(activityAddFilterDataFilter(filter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)