import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'

import {
  userFetchData,
  userAddFilterDataPerPage,
  userAddFilterDataPage,
  userAddFilterDataFilter,
} from '../../actions/users'

class Users extends Component{
     constructor(props) {
        super(props);

        bindAll(this, [
        	'handleSearch',
        ])
      }
    
    handleSearch(e){
        const{
          userAddFilterDataFilter,
          userFilterData,
          userFetchData,
        }=this.props
        const value = e.target.value
  
        setTimeout(()=>{
          if (value.trim().length > 2) {
 
            userAddFilterDataFilter(value)
            .then(()=>{
              userFetchData(this.props.userFilterData)
            })
          }else if (value.trim().length==0) {
            userAddFilterDataFilter('')
            .then(()=>{
              userFetchData(this.props.userFilterData)
            })
          }
        },500)
       
  
    
       
      }

    componentDidMount(){
      const{
        userFetchData,
        userFilterData,
        userAddFilterDataPerPage,
        userAddFilterDataPage,
      }=this.props

      userFetchData(userFilterData)
    }

    render(){
      const{
      usersPagination,
      users,
      userFilterData,
      userAddFilterDataPage,
      userAddFilterDataPerPage,
      userFetchData,
      }=this.props
  
        let userPagination=''
        if (!this.props.users.isEmpty()) {
          userPagination =
            <Pagination
              pagination={usersPagination}
              addPageNumber={userAddFilterDataPage}
              addPerPageSize={userAddFilterDataPerPage}
              fetchData={userFetchData}
              filterData={userFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <div id="example1_filter" className="dataTables_filter">
              <label className="pull-right"><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
              <h1>
                Users
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <UsersListItem activityList={users}/>
                      </div>
                    </div>
                    <div className="row">
                       {userPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    users:state.users,
    userFilterData:state.userFilterData,
    usersPagination:state.usersPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    userFetchData:(filterData)=>dispatch(userFetchData(filterData)),
    userAddFilterDataPerPage:(per_page)=>dispatch(userAddFilterDataPerPage(per_page)),
    userAddFilterDataPage:(page)=>dispatch(userAddFilterDataPage(page)),
    userAddFilterDataFilter:(filter)=>dispatch(userAddFilterDataFilter(filter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)