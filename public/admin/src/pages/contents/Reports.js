import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { Link } from 'react-router-dom'
import {
  userFetchData,
  userAddFilterDataPerPage,
  userAddFilterDataPage,
} from '../../actions/users'
import io from 'socket.io-client'
import{
  reportFetchData,
}from '../../actions/report'

class Reports extends Component{
     constructor(props) {
        super(props);

        this.state = {
          isUserOpen:''
        }

        bindAll(this, [
          'handleOpen',
          'connectSocket',
          'handleSocketMessage',  
        ])

      }

    componentDidMount(){
      const{
      reportFetchData,
      reportFilterData
      }=this.props
     
      reportFetchData(reportFilterData)
      .then(()=>{
        this.connectSocket()
      })
      
    }

    handleSocketMessage(message){
      const{reportFetchData,reportFilterData}=this.props
      reportFetchData(reportFilterData)
    }

    connectSocket(){
      io('https://notif.langgan.id').on(`channel:report-admin`, (message) => {
        // console.log(message)
        this.handleSocketMessage(message)
      })
    }

    handleOpen(e){
      if (this.state.isUserOpen == '' ) {
        this.setState({isUserOpen: 'open'})
      } else {
        this.setState({isUserOpen: ''})
      }
    }


    render(){
      const{
      usersPagination,
      users,
      userFilterData,
      userAddFilterDataPage,
      userAddFilterDataPerPage,
      userFetchData,
      reports,
      }=this.props
       
        let rowReport = []
        reports.map((value,index)=>{
          if (value.get('url') == null) {
          rowReport.push(
             <tr key={index}><td>{value.get('id')}</td><td>{value.get('title')}</td><td>{value.get('type')}</td><td>{value.get('created_at')}</td><td><a href={value.get('url')}>Generating</a></td></tr>
          )
          }else{
          rowReport.push(
             <tr key={index}><td>{value.get('id')}</td><td>{value.get('title')}</td><td>{value.get('type')}</td><td>{value.get('created_at')}</td><td><a href={value.get('url')}>Download</a></td></tr>
          )
          }
         
        })
        let userPagination=''
        if (!this.props.users.isEmpty()) {
          userPagination =
            <Pagination
              pagination={usersPagination}
              addPageNumber={userAddFilterDataPage}
              addPerPageSize={userAddFilterDataPerPage}
              fetchData={userFetchData}
              filterData={userFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Reports
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  <div className="text-right">
                    <div className={`dropdown ${this.state.isUserOpen}`}>
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                        <button onClick={this.handleOpen} className="btn-add-report"><i className="fa fa-plus"></i>Create Report</button>
                      </a>
                      <ul className="dropdown-menu dropdown-menu-report">
                        <li className="user-footer">
                          <div className="pull-left">
                            <Link to="/admin/addreporttoko"><a className="btn btn-default btn-flat">Report Transaksi Toko</a></Link>
                            <Link to="/admin/addreportdomain"><a className="btn btn-default btn-flat">Report Pembelian Domain</a></Link>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12 div-table-reports">
                        <table>
                          <thead>
                           <tr><td>Report ID</td><td>Report title</td><td>Report type</td><td>Created at</td><td>Action</td></tr>
                          </thead>
                          <tbody>
                            {rowReport}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    users:state.users,
    userFilterData:state.userFilterData,
    usersPagination:state.usersPagination,
    reportFilterData:state.reportFilterData,
    reports:state.reports,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    userFetchData:(filterData)=>dispatch(userFetchData(filterData)),
    userAddFilterDataPerPage:(per_page)=>dispatch(userAddFilterDataPerPage(per_page)),
    userAddFilterDataPage:(page)=>dispatch(userAddFilterDataPage(page)),
    reportFetchData:(reportfilter)=>dispatch(reportFetchData(reportfilter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports)