import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import ShopListItem from '../partials/ShopListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import {
  shopFetchData,
  shopsAddFilterDataPerPage,
  shopAddFilterDataPage,
  shopAddFilterDataFilter,
  shopActivate,
} from '../../actions/shop'

class Shop extends Component{
     constructor(props) {
        super(props);

        bindAll(this, [
        	'handleSearch',
          'handleActive',
        
        ])
      }

      handleActive(id){
        const{
          shopActivate,
          shopFilterData,
        }=this.props

        shopActivate(id,shopFilterData)
      }

      handleSearch(e){
        const{
          shopAddFilterDataFilter,
          shopFilterData,
          shopFetchData,
        }=this.props
        const value = e.target.value
  
        setTimeout(()=>{
          if (value.trim().length > 2) {
 
            shopAddFilterDataFilter(value)
            .then(()=>{
              shopFetchData(this.props.shopFilterData)
            })
          }else if (value.trim().length==0) {
            shopAddFilterDataFilter('')
            .then(()=>{
              shopFetchData(this.props.shopFilterData)
            })
          }
        },500)
    }
      
    componentDidMount(){
      const{
        shopFetchData,
        shopFilterData,
        shopsAddFilterDataPerPage,
        shopAddFilterDataPage,
      }=this.props

      shopFetchData(shopFilterData)
    }

    render(){
      const{
      shopsPagination,
      shops,
      shopFilterData,
      shopAddFilterDataPage,
      shopsAddFilterDataPerPage,
      shopFetchData,
      }=this.props
  
        let shopPagination=''
        if (!this.props.shops.isEmpty()) {
          shopPagination =
            <Pagination
              pagination={shopsPagination}
              addPageNumber={shopAddFilterDataPage}
              addPerPageSize={shopsAddFilterDataPerPage}
              fetchData={shopFetchData}
              filterData={shopFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Shop
              </h1>
              <div id="example1_filter" className="dataTables_filter">
                    <label><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <ShopListItem activityList={shops} handleActive={this.handleActive}/>
                      </div>
                    </div>
                    <div className="row">
                       {shopPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    shops:state.shops,
    shopFilterData:state.shopFilterData,
    shopsPagination:state.shopsPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    shopFetchData:(filterData)=>dispatch(shopFetchData(filterData)),
    shopsAddFilterDataPerPage:(per_page)=>dispatch(shopsAddFilterDataPerPage(per_page)),
    shopAddFilterDataPage:(page)=>dispatch(shopAddFilterDataPage(page)),
    shopAddFilterDataFilter:(filter)=>dispatch(shopAddFilterDataFilter(filter)),
    shopActivate:(id,filter_data)=>dispatch(shopActivate(id,filter_data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Shop)