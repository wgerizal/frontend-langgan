import React,{Component,Fragment} from 'react'
import { Link,Route } from 'react-router-dom'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import AccountListItem from '../partials/AccountListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import {
  accountFetchData,
  accountAddFilterDataPerPage,
  accountAddFilterDataPage,
  accountAddFilterDataFilter,
} from '../../actions/account'

class Account extends Component{
     constructor(props) {
        super(props);

        bindAll(this, [
        	"handleSearch"
        ])
      }


      handleSearch(e){
        const{
          accountAddFilterDataFilter,
          accountFilterData,
          accountFetchData,
        }=this.props
        const value = e.target.value
  
        setTimeout(()=>{
          if (value.trim().length > 2) {
 
            accountAddFilterDataFilter(value)
            .then(()=>{
              accountFetchData(this.props.accountFilterData)
            })
          }else if (value.trim().length==0) {
            accountAddFilterDataFilter('')
            .then(()=>{
              accountFetchData(this.props.accountFilterData)
            })
          }
        },500)
       
  
    
       
      }

    componentDidMount(){
      const{
        accountFetchData,
        accountFilterData,
        accountAddFilterDataPerPage,
        accountAddFilterDataPage,
      }=this.props

      accountFetchData(accountFilterData)
    }

    render(){
      const{
      accountsPagination,
      accounts,
      accountFilterData,
      accountAddFilterDataPage,
      accountAddFilterDataPerPage,
      accountFetchData,
      }=this.props
  
        let accountPagination=''
        if (!this.props.accounts.isEmpty()) {
          accountPagination =
            <Pagination
              pagination={accountsPagination}
              addPageNumber={accountAddFilterDataPage}
              addPerPageSize={accountAddFilterDataPerPage}
              fetchData={accountFetchData}
              filterData={accountFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header flex-container flex-between align-items-end">
              
              <div id="example1_filter" className="dataTables_filter">
              <h1>
                Account
              </h1>
                    <label><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
                    <Link to="/admin/adduser"><button className="btn btn-primary btn-add-user"> Add User </button></Link>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <AccountListItem accountList={accounts}/>
                      </div>
                    </div>
                    <div className="row">
                       {accountPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    accounts:state.accounts,
    accountFilterData:state.accountFilterData,
    accountsPagination:state.accountsPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    accountFetchData:(filterData)=>dispatch(accountFetchData(filterData)),
    accountAddFilterDataPerPage:(per_page)=>dispatch(accountAddFilterDataPerPage(per_page)),
    accountAddFilterDataPage:(page)=>dispatch(accountAddFilterDataPage(page)),
    accountAddFilterDataFilter:(filter)=>dispatch(accountAddFilterDataFilter(filter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)