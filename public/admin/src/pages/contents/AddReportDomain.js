import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { push } from 'react-router-redux';
import {
  reportGenerate,
} from '../../actions/report'

class AddReportDomain extends Component{
     constructor(props) {
        super(props);
        this.state={
          title:'',
          tanggal_min:'',
          tanggal_max:'',
        }
        bindAll(this, [
        	'handleSubmit',
          'handleOnChange',
        ])
      } 

    handleOnChange(e){
      this.setState({[e.target.name]:e.target.value})
    }    

    handleSubmit(){
      const{
        reportGenerate,
        redirectTo,
      }=this.props
      reportGenerate(this.state,'domain')
      .then(()=>{
        redirectTo('/admin/reports')
      })
    }

    
    render(){
     
  
       
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Report Transaksi Pembelian Domain
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12 div-table-reports">
                        <article className="s-12 m-12 l-7 center">
                          <h2 className="margin-bottom">Report Transaksi Pembelian Domain</h2>
                          <div className="div-reset-pass">
                            
                            <div className="flex-container flex-between">Title Report <input className="input-reset-password" onChange={this.handleOnChange} type="text" name="title" placeholder=""></input></div>
                            
                            <div className="flex-container flex-between">Tanggal <div className="flex-container flex-between"><input style={{width: '145px'}} className="input-reset-password" type="date" onChange={this.handleOnChange} name="tanggal_min"></input><input style={{width: '145px'}} className="input-reset-password" type="date" onChange={this.handleOnChange} name="tanggal_max"></input></div></div>
                          </div>
                          <div><button onClick={this.handleSubmit} className="btn btn-primary" type="submit">Generate</button></div>
                        </article>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    users:state.users,
    userFilterData:state.userFilterData,
    accountFilterData:state.accountFilterData,
    registerAdminError:state.registerAdminError,
    usersPagination:state.usersPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
   reportGenerate:(data,type)=>(dispatch(reportGenerate(data,type))),
   redirectTo:(link)=>(dispatch(push(link))),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddReportDomain)