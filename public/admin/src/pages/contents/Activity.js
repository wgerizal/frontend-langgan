import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import ActivityListItem from '../partials/ActivityListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import {
  activityFetchData,
  activityAddFilterDataPerPage,
  activityAddFilterDataPage,
  activityAddFilterDataFilter,
} from '../../actions/activity'

class Activity extends Component{
     constructor(props) {
        super(props);

        bindAll(this, [
        	"handleSearch"
        ])
      }


      handleSearch(e){
        const{
          activityAddFilterDataFilter,
          activityFilterData,
          activityFetchData,
        }=this.props
        const value = e.target.value
  
        setTimeout(()=>{
          if (value.trim().length > 2) {
 
            activityAddFilterDataFilter(value)
            .then(()=>{
              activityFetchData(this.props.activityFilterData)
            })
          }else if (value.trim().length==0) {
            activityAddFilterDataFilter('')
            .then(()=>{
              activityFetchData(this.props.activityFilterData)
            })
          }
        },500)
       
  
    
       
      }

    componentDidMount(){
      const{
        activityFetchData,
        activityFilterData,
        activityAddFilterDataPerPage,
        activityAddFilterDataPage,
      }=this.props

      activityFetchData(activityFilterData)
    }

    render(){
      const{
      activitysPagination,
      activitys,
      activityFilterData,
      activityAddFilterDataPage,
      activityAddFilterDataPerPage,
      activityFetchData,
      }=this.props
  
        let activityPagination=''
        if (!this.props.activitys.isEmpty()) {
          activityPagination =
            <Pagination
              pagination={activitysPagination}
              addPageNumber={activityAddFilterDataPage}
              addPerPageSize={activityAddFilterDataPerPage}
              fetchData={activityFetchData}
              filterData={activityFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Activity
              </h1>
              <div id="example1_filter" className="dataTables_filter">
                    <label><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <ActivityListItem activityList={activitys}/>
                      </div>
                    </div>
                    <div className="row">
                       {activityPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    activitys:state.activitys,
    activityFilterData:state.activityFilterData,
    activitysPagination:state.activitysPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    activityFetchData:(filterData)=>dispatch(activityFetchData(filterData)),
    activityAddFilterDataPerPage:(per_page)=>dispatch(activityAddFilterDataPerPage(per_page)),
    activityAddFilterDataPage:(page)=>dispatch(activityAddFilterDataPage(page)),
    activityAddFilterDataFilter:(filter)=>dispatch(activityAddFilterDataFilter(filter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Activity)