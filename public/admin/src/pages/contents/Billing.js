import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import BillingListItem from '../partials/BillingListItem'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import {
  billingFetchData,
  billingAddFilterDataPerPage,
  billingAddFilterDataPage,
  billingAddFilterDataFilter,
  completeBilling
} from '../../actions/billing'

class Billing extends Component{
     constructor(props) {
        super(props);

        bindAll(this, [
        	'handleSearch',
        	'handleComplete'
        ])
      }
    handleComplete(id){
        const{
          completeBilling,
          billingFilterData,
        }=this.props
        completeBilling(id,billingFilterData)
      }
    handleSearch(e){
      const{
        billingAddFilterDataFilter,
        billingFilterData,
        billingFetchData,
      }=this.props
      const value = e.target.value

      setTimeout(()=>{
        if (value.trim().length > 2) {
          
          billingAddFilterDataFilter(value)
          .then(()=>{
            billingFetchData(this.props.billingFilterData)
          })
        }else if (value.trim().length==0) {
          billingAddFilterDataFilter('')
          .then(()=>{
            billingFetchData(this.props.billingFilterData)
          })
        }
      },500)
     

  
     
    }

    componentDidMount(){
      const{
        billingFetchData,
        billingFilterData,
        billingAddFilterDataPerPage,
        billingAddFilterDataPage,
      }=this.props

      billingFetchData(billingFilterData)
    }

    render(){
      const{
      billingsPagination,
      billings,
      billingFilterData,
      billingAddFilterDataPage,
      billingAddFilterDataPerPage,
      billingFetchData,
      }=this.props
  
        let billingPagination=''
        if (!this.props.billings.isEmpty()) {
          billingPagination =
            <Pagination
              pagination={billingsPagination}
              addPageNumber={billingAddFilterDataPage}
              addPerPageSize={billingAddFilterDataPerPage}
              fetchData={billingFetchData}
              filterData={billingFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Billings
              </h1>
              <div id="example1_filter" className="dataTables_filter">
                    <label><input type="search" onChange={this.handleSearch} className="form-control input-sm" placeholder="" aria-controls="example1" /></label></div>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12">
                       <BillingListItem activityList={billings} handleComplete={this.handleComplete}/>
                      </div>
                    </div>
                    <div className="row">
                       {billingPagination}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    billings:state.billings,
    billingFilterData:state.billingFilterData,
    billingsPagination:state.billingsPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    billingFetchData:(filterData)=>dispatch(billingFetchData(filterData)),
    billingAddFilterDataPerPage:(per_page)=>dispatch(billingAddFilterDataPerPage(per_page)),
    billingAddFilterDataPage:(page)=>dispatch(billingAddFilterDataPage(page)),
    billingAddFilterDataFilter:(filter)=>dispatch(billingAddFilterDataFilter(filter)),
    completeBilling:(id,filter_data)=>dispatch(completeBilling(id,filter_data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Billing)