import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { Link } from 'react-router-dom'
import {
  userFetchData,
  userAddFilterDataPerPage,
  userAddFilterDataPage,
} from '../../actions/users'
import io from 'socket.io-client'
import{
  reportFetchData,
}from '../../actions/report'

class Theme extends Component{
     constructor(props) {
        super(props);

        this.state = {
          isUserOpen:''
        }

        bindAll(this, [
          'handleOpen',
          'connectSocket',
          'handleSocketMessage',  
        ])

      }

    componentDidMount(){
      const{
      reportFetchData,
      reportFilterData
      }=this.props
     
      reportFetchData(reportFilterData)
      .then(()=>{
        this.connectSocket()
      })
      
    }

    handleSocketMessage(message){
      const{reportFetchData,reportFilterData}=this.props
      reportFetchData(reportFilterData)
    }

    connectSocket(){
      io('https://notif.langgan.id').on(`channel:report-admin`, (message) => {
        // console.log(message)
        this.handleSocketMessage(message)
      })
    }

    handleOpen(e){
      if (this.state.isUserOpen == '' ) {
        this.setState({isUserOpen: 'open'})
      } else {
        this.setState({isUserOpen: ''})
      }
    }


    render(){
      const{
      usersPagination,
      users,
      userFilterData,
      userAddFilterDataPage,
      userAddFilterDataPerPage,
      userFetchData,
      reports,
      }=this.props
       
        let rowReport = []
        reports.map((value,index)=>{
          if (value.get('url') == null) {
          rowReport.push(
             <tr key={index}><td>{value.get('id')}</td><td>{value.get('title')}</td><td>{value.get('type')}</td><td>{value.get('created_at')}</td><td><a href={value.get('url')}>Generating</a></td></tr>
          )
          }else{
          rowReport.push(
             <tr key={index}><td>{value.get('id')}</td><td>{value.get('title')}</td><td>{value.get('type')}</td><td>{value.get('created_at')}</td><td><a href={value.get('url')}>Download</a></td></tr>
          )
          }
         
        })
        let userPagination=''
        if (!this.props.users.isEmpty()) {
          userPagination =
            <Pagination
              pagination={usersPagination}
              addPageNumber={userAddFilterDataPage}
              addPerPageSize={userAddFilterDataPerPage}
              fetchData={userFetchData}
              filterData={userFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Theme Settings
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  <div className="text-right">
                    <div>
                      <Link to="/admin/addtheme"><button className="btn-add-report"><i className="fa fa-plus"></i> Create </button></Link>
                    </div>
                  </div>
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12 div-table-reports">
                        <table>
                          <thead>
                           <tr><td>Title Template</td><td>Category Template</td><td>Base Template</td><td>File CSS</td><td>File Index</td><td>File Javascript</td></tr>
                          </thead>
                          <tbody>
                            <tr><td>Test</td><td>Tiska</td><td>Seiko</td><td>template.css</td><td>index1.php</td><td>coba.js</td></tr>
                            <tr><td>Test</td><td>Tiska</td><td>Seiko</td><td>template.css</td><td>index2.php</td><td>coba.js</td></tr>
                            <tr><td>Test</td><td>Tiska</td><td>Seiko</td><td>template.css</td><td>index3.php</td><td>coba.js</td></tr>
                            <tr><td>Test</td><td>Tiska</td><td>Seiko</td><td>template.css</td><td>index4.php</td><td>coba.js</td></tr>
                            <tr><td>Test</td><td>Tiska</td><td>Seiko</td><td>template.css</td><td>index5.php</td><td>coba.js</td></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    users:state.users,
    userFilterData:state.userFilterData,
    usersPagination:state.usersPagination,
    reportFilterData:state.reportFilterData,
    reports:state.reports,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    userFetchData:(filterData)=>dispatch(userFetchData(filterData)),
    userAddFilterDataPerPage:(per_page)=>dispatch(userAddFilterDataPerPage(per_page)),
    userAddFilterDataPage:(page)=>dispatch(userAddFilterDataPage(page)),
    reportFetchData:(reportfilter)=>dispatch(reportFetchData(reportfilter)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Theme)