import React,{Component,Fragment} from 'react'
import LangganLogo from '../../styles/img/logo-langgan.png';
import Pagination from '../partials/Pagination'
import UsersListItem from '../partials/UsersListItem'
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import {
  userFetchData,
  userAddFilterDataPerPage,
  userAddFilterDataPage,
} from '../../actions/users'

import {
  accountRegister,
} from '../../actions/account'

class AddUser extends Component{
     constructor(props) {
        super(props);
        this.state={
          name:'',
          email:'',
          password:'',
          pass_confirm:''
        }
        bindAll(this, [
        	'handleSubmit',
          'handleOnChange',
        ])
      } 

    handleOnChange(e){
      this.setState({[e.target.name]:e.target.value})
    }    

    handleSubmit(){
      const{
        accountRegister,
        accountFilterData,
      }=this.props
      accountRegister(this.state,accountFilterData)
    }

    componentDidMount(){
      const{
        userFetchData,
        userFilterData,
        userAddFilterDataPerPage,
        userAddFilterDataPage,
      }=this.props

      userFetchData(userFilterData)
    }

    render(){
      const{
      usersPagination,
      users,
      userFilterData,
      userAddFilterDataPage,
      userAddFilterDataPerPage,
      userFetchData,
      registerAdminError,
      }=this.props
  
        let userPagination=''
        if (!this.props.users.isEmpty()) {
          userPagination =
            <Pagination
              pagination={usersPagination}
              addPageNumber={userAddFilterDataPage}
              addPerPageSize={userAddFilterDataPerPage}
              fetchData={userFetchData}
              filterData={userFilterData}
              perPageSize={''} />
        }
        return(
            <Fragment>
             <section className="content-header">
              <h1>
                Add User
              </h1>
            </section>

            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="box">
                  <div className="box-header">
                  </div>
                  <div className="box-body">
                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                    <div className="row">
                      <div className="col-sm-12 div-table-reports">
                        <article className="s-12 m-12 l-7 center">
                          <h2 className="margin-bottom">Add User</h2>
                          <div className="div-reset-pass">
                            <div className="flex-container flex-between">Name <input className="input-reset-password" onChange={this.handleOnChange} type="text" name="name" placeholder=""></input></div>
                            
                            <div className="flex-container flex-between">E-Mail Address <input className="input-reset-password" onChange={this.handleOnChange} type="email" name="email" placeholder=""></input></div>
                            {registerAdminError.get('email')? 
                             <span className="span-error" style={{fontSize:'16px'}}>{registerAdminError.get('email')}</span>
            
                            :
                                null
                            }
                            <div className="flex-container flex-between">Password <input className="input-reset-password" onChange={this.handleOnChange} type="password" name="password"></input></div>
                            {registerAdminError.get('password')? 
                             <span className="span-error" style={{fontSize:'16px'}}>{registerAdminError.get('password')}</span>
            
                            :
                                null
                            }
                            <div className="flex-container flex-between">Confirm Password <input className="input-reset-password" onChange={this.handleOnChange} type="password" name="password_confirmation"></input></div>
                        
                          </div>
                          <div><button className="btn btn-primary" onClick={this.handleSubmit} type="submit">Register</button></div>
                        </article>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>{/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
    users:state.users,
    userFilterData:state.userFilterData,
    accountFilterData:state.accountFilterData,
    registerAdminError:state.registerAdminError,
    usersPagination:state.usersPagination,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    userFetchData:(filterData)=>dispatch(userFetchData(filterData)),
    userAddFilterDataPerPage:(per_page)=>dispatch(userAddFilterDataPerPage(per_page)),
    userAddFilterDataPage:(page)=>dispatch(userAddFilterDataPage(page)),
    accountRegister:(data,filterData)=>dispatch(accountRegister(data,filterData)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUser)