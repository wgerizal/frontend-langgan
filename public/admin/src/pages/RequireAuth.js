import React,{Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import bindAll from 'lodash/bindAll'
import { push } from 'react-router-redux';
import store from '../helpers/user_session'
import {
  globalUiLoginModal,
}from '../actions/globalUi'

import {
  userCheckCredential,
}from '../actions/users'

export default function(ComposedComponent) {

  class RequireAuth extends Component {

     constructor(props) {
		super(props)
        this.loginModal = React.createRef();
		bindAll(this, [
			'_checkAndRedirect',
		])

		
	}
    // Push to login route if not authenticated on mount
    componentDidMount() {
       this._checkAndRedirect();
    }

    componentDidUpdate() {
      this._checkAndRedirect();
    }

    _checkAndRedirect(){
        const{
            user,
            globalUiLoginModal,
            redirect,
            userCheckCredential,
        }=this.props
        const token_credential = store.isLoggedIn()
        const isLogged = user.get('logged_in') 
        if (token_credential==true) {
          userCheckCredential()
        }else{
          if (isLogged==false) {
            redirect();
          }
        }
        
    }

    // Push to login route if not authenticated on update

    // Otherwise render the original component
    render() {
        const{
            user
        }=this.props
      const isLogged = user.get('logged_in') 
      return(
        <div>
            {isLogged==true?<ComposedComponent {...this.props}/> :null }
            
        </div>
      )
    }

  }

  const mapStateToProps = (state, ownProps) => {
	return {
		user: state.user,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    globalUiLoginModal:(login_modal)=>dispatch(globalUiLoginModal(login_modal)),
    userCheckCredential:()=>dispatch(userCheckCredential()),
    redirect: () => dispatch(push('/auth/login'))
	}
}

  return withRouter(connect(mapStateToProps, mapDispatchToProps)(RequireAuth))

}