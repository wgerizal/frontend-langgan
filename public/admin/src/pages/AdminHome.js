import React,{Component} from 'react';
import { connect } from 'react-redux'
import bindAll from 'lodash/bindAll'
import { withRouter, Switch } from 'react-router'
import { Link,Route } from 'react-router-dom'
import logo from '../logo.svg';
import LangganLogo from '../styles/img/logo-langgan.png';
import AdminLogo from '../styles/img/logo-admin.png';
import ModalLogin from './partials/ModalLogin'
import $ from 'jquery'
import Dashboard from './contents/Dashboard'
import Activity from './contents/Activity'
import Account from './contents/Account'
import Users from './contents/Users'
import Reports from './contents/Reports'
import Theme from './contents/Theme'
import Billing from './contents/Billing'
import Shop from './contents/Shop'
import Kupon from './contents/Kupon'
import AddUser from './contents/AddUser'
import AddTheme from './contents/AddTheme'
import AddReportToko from './contents/AddReportToko'
import AddReportDomain from './contents/AddReportDomain'
// import 'bootstrap/dist/css/bootstrap.min.css';
// import '../styles/bootstrap/css/bootstrap.css';

import {
  userLogout,
}from '../actions/users'

class AdminHome extends Component {
    constructor(props) {
		super(props)

    this.state = {
      isUserOpen:''
    }

		bindAll(this, [
            'handleLogout',
            'handleOpen',
		]) 
    }

    handleLogout(e){
      const{userLogout}=this.props
      userLogout()
    }

    handleOpen(e){
      if (this.state.isUserOpen == '' ) {
        this.setState({isUserOpen: 'open'})
      } else {
        this.setState({isUserOpen: ''})
      }
    }

  render(){
    const{location}=this.props
   
      return (
        <div className="skin-blue">
        <div className="wrapper">
          <header className="main-header">
            {/* Logo */}
            <a href="" className="logo"><img src={AdminLogo} /></a>
            {/* Header Navbar: style can be found in header.less */}
            <nav className="navbar navbar-static-top" role="navigation">
              {/* Sidebar toggle button*/}
              <label className="label-dashboard">Dashboard</label>
              <div className="navbar-custom-menu">
                <ul className="nav navbar-nav">
                  {/* Messages: style can be found in dropdown.less*/}
                  <li className="dropdown messages-menu" style={{marginTop: '8px'}}>
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                      <i className="fa fa-search" />
                    </a>
                    <ul className="dropdown-menu">
                      <li className="header">You have 4 messages</li>
                      <li>
                        {/* inner menu: contains the actual data */}
                        <ul className="menu">
                          <li>{/* start message */}
                            <a href="#">
                              <div className="pull-left">
                                <img src={require('../styles/img/user2-160x160.jpg')} className="img-circle" alt="User Image" />
                              </div>
                              <h4>
                                Support Team
                                <small><i className="fa fa-clock-o" /> 5 mins</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>{/* end message */}
                          <li>
                            <a href="#">
                              <div className="pull-left">
                                <img src={require('../styles/img/user3-128x128.jpg')} className="img-circle" alt="user image" />
                              </div>
                              <h4>
                                AdminLTE Design Team
                                <small><i className="fa fa-clock-o" /> 2 hours</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <div className="pull-left">
                                <img src={require('../styles/img/user4-128x128.jpg')} className="img-circle" alt="user image" />
                              </div>
                              <h4>
                                Developers
                                <small><i className="fa fa-clock-o" /> Today</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <div className="pull-left">
                                <img src={require('../styles/img/user3-128x128.jpg')} className="img-circle" alt="user image" />
                              </div>
                              <h4>
                                Sales Department
                                <small><i className="fa fa-clock-o" /> Yesterday</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <div className="pull-left">
                                <img src={require('../styles/img/user4-128x128.jpg')} className="img-circle" alt="user image" />
                              </div>
                              <h4>
                                Reviewers
                                <small><i className="fa fa-clock-o" /> 2 days</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li className="footer"><a href="#">See All Messages</a></li>
                    </ul>
                  </li>
                  {/* Notifications: style can be found in dropdown.less  */}
                  <li className="dropdown notifications-menu" style={{marginTop: '8px'}}>
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                      <i className="fa fa-bell-o" />
                      <span className="label label-danger">4</span>
                    </a>
                    <ul className="dropdown-menu">
                      <li className="header">You have 10 notifications</li>
                      <li>
                        <ul className="menu">
                          <li>
                            <a href="#">
                              <i className="fa fa-users text-aqua" /> 5 new members joined today
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="fa fa-warning text-yellow" /> Very long description here that may not fit into the page and may cause design problems
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="fa fa-users text-red" /> 5 new members joined
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="fa fa-shopping-cart text-green" /> 25 sales made
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="fa fa-user text-red" /> You changed your username
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li className="footer"><a href="#">View all</a></li>
                    </ul>
                  </li>
                 {/*User Account: style can be found in dropdown.less */}
                  <li onClick={this.handleOpen} className={`dropdown user user-menu ${this.state.isUserOpen}`}>
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                      <img src={require('../styles/img/user2-160x160.jpg')} className="user-image" alt="User Image" />
                      <span><i className="fa fa-angle-down"></i></span>
                    </a>
                    <ul className="dropdown-menu">
                      {/* User image */}
                      <li className="user-header">
                        <img src={require('../styles/img/user2-160x160.jpg')} className="img-circle" alt="User Image" />
                        <p>
                          Alexander Pierce - Web Developer
                          <small>Member since Nov. 2012</small>
                        </p>
                      </li>
                      {/* Menu Footer*/}
                      <li className="user-footer">
                        <div className="pull-right">
                          <a onClick={this.handleLogout} className="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
          {/* Left side column. contains the logo and sidebar */}
          <aside className="main-sidebar">
            {/* sidebar: style can be found in sidebar.less */}
            <section className="sidebar">
              {/* sidebar menu: : style can be found in sidebar.less */}
              <ul className="sidebar-menu">
                <li className={`${location=='/admin'?'active':''}`}>
                  <Link to="/admin">
                    <img style={{display: 'inline', marginRight: '20px'}} src={require('../styles/img/dashboard.png')} /> <span>Dashboard</span>
                  </Link>
                </li>
                <li className={`${location=='/admin/activity'?'active':''}`} style={{marginTop: '50px'}}>
                  <Link to="/admin/activity">
                    <i className="fa fa-clock-o" />
                    <span>Activity</span>
                  </Link>
                 
                </li>
                <li className={`${location=='/admin/account'?'active':''}`}>
                  <Link to="/admin/account">
                    <i className="fa fa-user-o" /> <span>Account</span>
                  </Link>
                </li>
                <li className={`${location=='/admin/users'?'active':''}`}>
                  <Link to="/admin/users">
                    <i className="fa fa-user-o" /> <span>Users</span>
                  </Link>
                </li>
                <li className={`${location=='/admin/billing'?'active':''}`} style={{marginTop: '60px'}}>
                  <Link to="/admin/billing">
                    <i className="fa fa-align-left" />
                    <span>Billing</span>
                   </Link>
                </li>
                <li className={`${location=='/admin/reports'?'active':''}`}>
                  <Link to="/admin/reports">
                    <i className="fa fa-area-chart" />
                    <span>Report</span>
                  </Link>
                </li>
                 <li className={`${location=='/admin/kupon'?'active':''}`}>
                 <Link to="/admin/kupon">
                    <i className="fa fa-shopping-bag" />
                    <span>Kupon</span>
                 </Link>
                </li>
                <li className={`${location=='/admin/shop'?'active':''}`}>
                    <Link to="/admin/shop">
                    <i className="fa fa-shopping-bag" />
                    <span>Toko</span>
                 </Link>
                </li>
                <li className={`${location=='/admin/theme'?'active':''}`}>
                  <Link to="/admin/theme">
                    <i className="fa fa-gear" /> <span>Theme Settings</span>
                  </Link>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-envelope-o" /> <span>Tawk.to Chat</span>
                  </a>
                </li>
              </ul>
            </section>
            {/* /.sidebar */}
          </aside>
          {/* Right side column. Contains the navbar and content of the page */}
          <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <Switch>
              <Route exact path="/admin" component={Dashboard}/>
              <Route path="/admin/activity" component={Activity}/>
              <Route path="/admin/account" component={Account}/>
              <Route path="/admin/users" component={Users}/>
              <Route path="/admin/reports" component={Reports}/>
              <Route path="/admin/theme" component={Theme}/>
              <Route path="/admin/billing" component={Billing}/>
              <Route path="/admin/shop" component={Shop}/>
              <Route path="/admin/kupon" component={Kupon}/>
              <Route path="/admin/adduser" component={AddUser}/>
              <Route path="/admin/addtheme" component={AddTheme}/>
              <Route path="/admin/addreporttoko" component={AddReportToko}/>
              <Route path="/admin/addreportdomain" component={AddReportDomain}/>
            </Switch>
          </div>{/* /.content-wrapper */}
        </div>{/* ./wrapper */}
      </div>
      );

  }
  
}


const mapStateToProps = (state, ownProps) => {
	return {
    location:ownProps.location.pathname
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    userLogout:()=>dispatch(userLogout())
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminHome))
