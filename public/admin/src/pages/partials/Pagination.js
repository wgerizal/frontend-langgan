import React,{Component,Fragment} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import range from 'lodash/range'
import bindAll from 'lodash/bindAll'
import { Map } from 'immutable'

class Pagination extends Component{
    constructor(props) {
		super(props);

		bindAll(this, [
			'pageRange',
			'handleLinkClick',
			// 'handleLastClick',
			// 'handleFirstLinkClick'
		])
	}

    pageRange(): Array<number> {
		const { pagination } = this.props

		let startPage: number = 0
		let endPage: number = 0
		let pages: Array<number> = []

		const totalPages = Math.ceil(pagination.get('total') / pagination.get('per_page'))

        if (totalPages <= 3) {
        startPage = 1
        endPage = totalPages
        } else {

        if (pagination.get('current_page') == 1) {
            startPage = 1
            endPage = 3
        } else if (pagination.get('current_page') == pagination.get('last_page')) {
            startPage = pagination.get('last_page') - 2
            endPage = pagination.get('last_page')
        }else if(pagination.get('current_page') <= pagination.get('last_page') && pagination.get('current_page') >=  pagination.get('last_page')-2 ){
            startPage = pagination.get('last_page') - 3
            endPage = pagination.get('last_page')
        } else if(pagination.get('current_page') >= 2 && pagination.get('current_page') <=3){
            startPage = 1
            endPage = 4
        } else {
            startPage = pagination.get('current_page') - 2
            endPage = pagination.get('current_page') + 2
        }



    
        }

            pages = range(startPage, endPage + 1)

		return pages
	}


    handleLinkClick(e) {
		const {
			addPageNumber,
			fetchData,
			addPerPageSize,
			perPageSize,
		}=this.props

		const pageNumber: number = e.target.dataset.page
       
		addPageNumber(pageNumber).then(() => {
            
			if (perPageSize==0 || perPageSize==''|| perPageSize== undefined) {
			
				fetchData( this.props.filterData)
			}else{
				addPerPageSize(perPageSize).then(()=>{
						fetchData( this.props.filterData)
				})
				
				
			}
			
		
		})
	}


    render(){
        const{pagination}=this.props
        const pages: Array<Object> = []
        const total_pages= pagination.get('total') / pagination.get('per_page')
        if (total_pages > 1) {
            const pageRange: Array<number> = this.pageRange()

            for (var i = 0; i < pageRange.length; i++) {
				const pageNumber: number = pageRange[i]

				if (pageNumber === pagination.get('current_page')) {
					pages.push(
                        <li key={'activity-link-' + pageNumber} className="paginate_button"><a href="#" className="active" aria-controls="example1" data-dt-idx="1" tabindex="0">{pageNumber}</a></li>
					)
				} else {
					pages.push(
                        <li key={'activity-link-' + pageNumber} onClick={this.handleLinkClick} className="paginate_button "><a data-page={pageNumber} href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">{pageNumber}</a></li>
			
					)
				}
			}

        }else{
            pages.push(
                <li key='activity-link-1' className="paginate_button"><a href="#" className="active" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
            )
        }

        return(
            <div className="col-sm-12">
                        <div className="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                          <ul className="pagination">
                              {pages}
                          </ul>
                        </div>
            </div>
        )
    }
}

export default Pagination