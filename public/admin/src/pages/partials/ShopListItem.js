import React,{Component} from 'react'

class ShopListItem extends Component{
    render(){
    const{activityList,handleActive}=this.props
    let listAct;
    // console.log(activityList)
    if (activityList.isEmpty()) {
        listAct = "No Activity"
    }else{
       
        listAct = activityList.map((activity, index)=>
                    <tr role="row" className={`${index%2 == 0 ?'odd':'even'}`}>
                        <td className="sorting_1"><img style={{width:'80px',marginLeft:"30%"}} src={activity.get('logo')} /></td>
                          <td>{activity.get('name')}</td>
                           <td>{activity.get('description')}</td>
                           <td>{activity.get('status')==false?'Not Active':'Active'}</td>
                           <td>{activity.get('status')==false?<button onClick={()=>handleActive(activity.get('id'))} className="btn btn-primary"> Activate </button>:<button onClick={this.handleActive} className="btn btn-primary" disabled> Activated </button>}</td>
                        </tr>
        )
    }
        return(

            <table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}></th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style={{width: '246px'}}>Nama Toko</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Deskripsi</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Status</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Aksi</th>
                       </tr>
                        </thead>
                        <tbody>
                        {listAct}
                        </tbody>
                </table>

        )
    }
}

export default ShopListItem