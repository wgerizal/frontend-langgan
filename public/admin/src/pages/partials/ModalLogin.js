import React,{Component} from 'react'
import bindAll from 'lodash/bindAll'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import LoginIMG from '../../styles/img/login-img.png';

import {
  globalUiLoginModal,
  globalUiRegisterModal,
}from '../../actions/globalUi'

import {
  userAskLoggedIn,
  userCheckHomePageCredentials,
}from '../../actions/users'


class ModalLogin extends Component{
    
    constructor(props) {
		super(props)
        this.loginModal = React.createRef();
		bindAll(this, [
			'handleClick',
            'handleClose',
            'handleLogin',
            'handleInputChange',
            'OpenModalRegister',
		])

        this.state = {
			email: '',
            pass: '',
            checkbox:true,
		}

		
	}
    
   

    componentWillMount(){
    const{
      userCheckHomePageCredentials
    }=this.props
     userCheckHomePageCredentials()
        document.addEventListener('mousedown', this.handleClick, false)
    }
    componentWillUnmount(){
        document.addEventListener('mousedown', this.handleClick, false)
    }

    OpenModalRegister(e){
        const{
        globalUiRegisterModal,
        globalUiLoginModal
        }=this.props
        globalUiLoginModal(false)
       
        globalUiRegisterModal(true)

    }

    handleInputChange(e){
        if (e.target.name=='checkbox') {
            return this.setState({[e.target.name]:!this.state.checkbox})
        }
        this.setState({[e.target.name]:e.target.value})
    }

    handleLogin(e){
        const{
            userAskLoggedIn
        }=this.props
        
        
        userAskLoggedIn(this.state)
    }

    handleClick(e){
        const{
            loginUi,
            globalUiLoginModal
        }=this.props
    
        const notLogin = loginUi.get('login_modal')
        if(this.loginModal.contains(e.target)){
            return;
        }
        if (notLogin==true) {
            globalUiLoginModal(!notLogin);
        }
        
    }

    handleClose(e){
        const{
            loginUi,
            globalUiLoginModal,
        }=this.props
        const notLogin = loginUi.get('login_modal')
        globalUiLoginModal(!notLogin)
    }

    render(){
        const{
            loginUi,
            handleClose
        }=this.props
        const{
            email,
            pass,
            checkbox,
        }=this.state
        const notLogin = loginUi.get('login_modal')
        return(
            <div id="id01" className={`modal ${notLogin==true?"show":''}`} >
                <img className="img-login animate" src={LoginIMG} />
                    <div className="modal-content-login animate" ref={(el) => {this.loginModal = el}}>
                        <div className="imgcontainer">
                        <span onClick={this.handleClose} className="close" title="Close Modal">&times;</span>
                        Halo !
                        </div>

                        <div className="container">
                        <input onChange={this.handleInputChange} value={email} type="email" placeholder="Email" name="email" required/>

                        <input onChange={this.handleInputChange} value={pass} type="password" placeholder="Password" name="pass" required/>
                            
                        <div>
                        <label>
                            <input type="checkbox" checked={checkbox} onChange={this.handleInputChange} name="checkbox"/> Ingatkan saya
                        </label>
                        <span className="psw">Lupa akun ?</span>
                        </div>

                        <div className="div-login"><button onClick={this.handleLogin} type="submit">Masuk</button></div>
                        </div>

                        <div className="container background-white">
                        <div className="div-atau">
                            <span>
                            Atau
                            </span>
                        </div>
                        </div>
                        <div className="container">
                            <label>
                            Belum punya akun? <a onClick={this.OpenModalRegister}>Daftar disini.</a>
                            </label>
                        </div>
                    </div>
                    </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
	return {
		loginUi: state.loginUi,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    globalUiLoginModal:(modal)=>dispatch(globalUiLoginModal(modal)),
    globalUiRegisterModal:(modal)=>dispatch(globalUiRegisterModal(modal)),
    userAskLoggedIn:(data)=>dispatch(userAskLoggedIn(data)),
    userCheckHomePageCredentials:()=>dispatch(userCheckHomePageCredentials()),
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalLogin))
