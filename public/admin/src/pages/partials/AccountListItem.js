import React,{Component} from 'react'

class AccountListItem extends Component{
    render(){
    const{accountList}=this.props
    let listAct;
    let isEmptyList=false;
    if (accountList.isEmpty()) {
        listAct = "No Account"
        isEmptyList=true
    }else{
        listAct = accountList.map((account, index)=>
                    <tr role="row" className={`${index%2 == 0 ?'odd':'even'}`}>
                           <td className="sorting_1"><img style={{width:'80px',marginLeft:"30%"}} src={account.get('avatar')} /></td>
                          <td>{account.get('email')}</td>
                          <td>{account.get('name')}</td>
                        </tr>
        )
    }
    let renderTh;
    if (!isEmptyList) {
        renderTh= <thead>
                        <tr role="row">
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}>Avatar</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style={{width: '246px'}}>Email</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Name</th>
                        </tr>
                        </thead>
    }
        return(

            <table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        {renderTh}
                        <tbody>
                        {listAct}
                        </tbody>
                </table>

        )
    }
}

export default AccountListItem