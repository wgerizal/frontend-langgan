import React,{Component} from 'react'

class BillingListItem extends Component{
    render(){
    const{activityList,handleComplete}=this.props
    let listAct;

    if (activityList.isEmpty()) {
        listAct = "No Activity"
    }else{
        listAct = activityList.map((activity, index)=>
                    <tr role="row" className={`${index%2 == 0 ?'odd':'even'}`}>
                          <td>{activity.get('invoice')}</td>
                          <td className="sorting_1"><img src={require('../../styles/img/list1.png')} /></td>
                           <td>{activity.get('email')}</td>
                           <td>{activity.get('paket')}</td>
                           <td>{activity.get('channel')}</td>
                           <td>{activity.get('nominal')}</td>
                           <td>{activity.get('tanggal_kadaluarsa')}</td>
                           <td>{activity.get('status')==null?'':activity.get('status')}</td>
                           <td>{activity.get('status')!=3?<button onClick={()=>handleComplete(activity.get('id'))} className="btn btn-primary"> Complete </button>:<button onClick={this.handleComplete} className="btn btn-primary" disabled> Completed </button>}</td>
                          
                        </tr>
        )
    }
        return(

            <table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}>Invoice</th>
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}></th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style={{width: '246px'}}>User</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Paket</th>
                         <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style={{width: '172px'}}>Channel Payment</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style={{width: '126px'}}>Nominal</th>
                         <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style={{width: '126px'}}>Tanggal Pembelian</th>
                          <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style={{width: '126px'}}>Status</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style={{width: '126px'}}>Action</th>
                       </tr>
                        </thead>
                        <tbody>
                        {listAct}
                        </tbody>
                </table>

        )
    }
}

export default BillingListItem