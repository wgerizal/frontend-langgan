import React,{Component} from 'react'

class ActivityListItem extends Component{
    render(){
    const{activityList}=this.props
    let listAct;
    if (activityList.isEmpty()) {
        listAct = "No Activity"
    }else{
        listAct = activityList.map((activity, index)=>
                    <tr role="row" className={`${index%2 == 0 ?'odd':'even'}`}>
                          <td className="sorting_1"><img src={require('../../styles/img/list1.png')} /></td>
                          <td>{activity.get('email')}</td>
                          <td>{activity.get('status')}</td>
                          <td>{activity.get('created_at')}</td>
                          <td>{activity.get('detail')}</td>
                        </tr>
        )
    }
        return(

            <table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}></th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style={{width: '246px'}}>User</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Status</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style={{width: '172px'}}>Date</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style={{width: '126px'}}>Detail</th></tr>
                        </thead>
                        <tbody>
                        {listAct}
                        </tbody>
                </table>

        )
    }
}

export default ActivityListItem