import React,{Component} from 'react'

class KuponListItem extends Component{
    render(){
    const{activityList}=this.props
    let listAct;

    if (activityList.isEmpty()) {
        listAct = "No Activity"
    }else{
        listAct = activityList.map((activity, index)=>
                    <tr role="row" className={`${index%2 == 0 ?'odd':'even'}`}>
                            <td>{activity.get('code')}</td>
                          <td>{activity.get('amount')}</td>
                           <td>{activity.get('expired')}</td>
                          
                        </tr>
        )
    }
        return(

            <table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                        <th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{width: '60px'}}>Kode</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style={{width: '246px'}}>Jumlah</th>
                        <th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style={{width: '219px'}}>Kadaluarsa</th>
                       </tr>
                        </thead>
                        <tbody>
                        {listAct}
                        </tbody>
                </table>

        )
    }
}

export default KuponListItem