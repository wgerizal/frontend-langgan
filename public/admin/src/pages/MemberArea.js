import React,{Component} from 'react';
import store from '../helpers/user_session'
import LangganLogo from '../styles/img/logo-langgan.png';
import HunLogo from '../styles/img/hun.png';
class MemberArea extends Component{
    render(){
        const user_email = store.getUserEmail()
        return(
                <div>
                <header>
                    <nav>
                        <div className="line">
                        <div className="s-12 l-2 p-t-20">
                            <img className="s-5 l-12 center" src={LangganLogo}/>
                        </div>
                        <div className="top-nav s-12 l-10 right">
                            <ul className="right">
                                <li><a>Toko Saya</a></li>
                                <li><a>Buat Toko</a></li>
                                <li><a className="color-langgan"><i className="fa fa-envelope"></i></a></li>
                                <li><a className="color-langgan"><i className="fa fa-user-circle"></i> {user_email} <i className="fa fa-caret-down"></i></a></li>
                            </ul>
                        </div>
                        </div>
                    </nav>
                </header>
                <section className="text-center">
                    {/* <!-- FIRST BLOCK --> */}
                    <div id="tema-header-block" className="text-center">
                        <div className="line">
                        <div className="margin-bottom">
                            <div className="margin">
                                <article className="s-12 m-12 l-7 center">
                                    <h2 className="margin-bottom">Toko Saya</h2>
                                    <p className="text-center f-20">Pilih dan edit atau bisa melihat dashboard toko kamu
                                    </p>
                                </article>
                            </div>
                        </div>
                        </div>
                    </div>
                    {/* <!-- SECOND BLOCK --> */}
                    <div id="tema-detail-block" className="text-left">
                        <div className="line">
                        <div className="margin-bottom">
                            <div className="margin">
                                <div className="s-12 m-12 l-2 left">
                                    <img src={HunLogo} alt="" />
                                </div>
                                <div className="s-12 m-12 l-2 left">
                                    <h5 className="margin-bottom bold">Hun</h5>
                                    <h6 className="text-left">www.hun.com</h6>
                                    <h6 className="text-left">Paket Premium (6 bulan)</h6>
                                </div>
                                <div className="s-12 m-12 l-3 left">
                                    <h6 className="text-left">Tema East Anastacio</h6>
                                    <h6 className="text-left">Warna Biru</h6>
                                </div>
                                <div className="s-12 m-12 l-3 left">
                                    <h6 className="text-left">Berlaku s/d 4 Januari 2020</h6>
                                    <h6 className="text-left">Status Live</h6>
                                </div>
                                <div className="s-12 m-12 l-2 left">
                                    <div><button className="btn-offline" type="submit">Jadikan Offline</button></div>
                                    <div><button className="btn-dashboard" type="submit">Dashboard</button></div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
                {/* <!-- FOOTER --> */}
                <footer>
                    <div className="line">
                        <div className="s-12 l-4">
                        <p>Langgan</p>
                        <p>Graha DLA, Lantai 2 - Suite 06 Jl. Otto Iskandar Dinata No. 392 Bandung 40242</p>
                        <p>+62 811 2481 110 / hello@langgan.id</p>
                        </div>
                        <div className="s-12 l-4 text-center">
                        FAQ
                        </div>
                        <div className="s-12 l-4">
                        Subscribe untuk mengikuti berita paling mutakhir
                        </div>
                    </div>
                    <hr />
                    <div className="line">
                        <div className="s-12 l-6">
                        <p>© 2019 PT. Berlangganan Indonesia Global</p>
                        </div>
                        <div className="s-12 l-6 text-right">
                        Syarat & Ketentuan
                        </div>
                    </div>
                </footer>

            </div>

            )
        }
    }

export default MemberArea