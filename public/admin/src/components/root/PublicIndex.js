import React,{Component,Fragment} from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Notifications from 'react-notification-system-redux'

class PublicIndex extends Component{

    render() {
		const {
			children,
			notifications,
		} = this.props

		return(
	    <Fragment>
				{children}
				<Notifications notifications={notifications} />
			</Fragment>
		)
	}

}


const mapStateToProps = (state, ownProps) => {
	return {
		notifications: state.notifications,
		
	}
}

export default withRouter(connect(mapStateToProps, null)(PublicIndex))
