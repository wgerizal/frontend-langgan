import { put, call, all, select, take ,delay} from "redux-saga/effects";
import { Map, List, fromJS } from "immutable";
import { push } from 'react-router-redux';
import Notifications from 'react-notification-system-redux'
import store from '../helpers/user_session'
import omit from 'lodash/omit'
import {
  usersLoginApi,
  usersRegisterApi,
  usersCheckOtp,
} from "../apis/users";

import{
    getuser
} from "../selectors"


export function* userAskLoggedIn({
    params
}:Object):Generator<*,*,*>{
    try {
        const credentials = {
            email:params.email,
            password:params.password,
            role:'admin'
        }
        const remember = params.checkbox
        let response: Object = {};
        response = yield call(usersLoginApi,credentials)
        if(response.status >= 200 && response.status < 300 && response.data.status != "error"){
            let token_user = response.data.access_token
            store.remove()
            store.set(token_user)
            if (remember==true) {
                store.setUserEmail(params.email)
                // store.setUserPass(params.password)
            }
            let code_otp = response.data.code
           

            if (code_otp) {
                // store.setCodeOtp(credentials.email,code_otp)
                yield put({
                    type:'USER_AUTH_SAVE_EMAIL_SUCCESS',
                    email:credentials.email
                })
                yield put({
                    type:'USER_AUTH_SAVE_OTP_SUCCESS',
                    otp:code_otp
                })
            }
           
            yield put({
                type:'USER_AUTH_SUCCESS',
                logged_in:true
            })

            
            if (store.getCodeOtp(credentials.email)===null) {
                yield put({
                    type:'OTP_PAGE_OPEN_SUCCESS',
                    otp_page:true
                })
            }else{
                yield put(
                push('/admin')
                )

            }
            
            
            // yield put(
            //   push('/admin')
            // )
        }else{
            throw response
        }
    } catch (error) {
            yield put({
                type:'USER_AUTH_SUCCESS',
                logged_in:false
            })

            if (error.data.messages) {
                yield put({
                    type:'USER_AUTH_ADD_ERROR_MESSAGE',
                    error:error.data.messages
                })
            }

            store.removeUserCredentials()
    }
}


export function* userSubmitOtp({
    otp_user
}):Generator<*,*,*>{

    try {
        const user : Map<string, any> = yield select(getuser);
        let response: Object = {};
        
        response = yield call(usersCheckOtp,otp_user)
        if(response.status >= 200 && response.status < 300 && response.data.status != "error"){
            let token_user = response.data.access_token
            store.remove()
            store.set(token_user)
            store.setCodeOtp(user.get('email'),user.get('otp'))
             yield put({
                type:'OTP_PAGE_OPEN_SUCCESS',
                otp_page:false
            })

            yield put(
              push('/admin')
            )

            
        }else{
            throw response
        }
    } catch (error) {
        if (error.data.message) {
            yield put({
                type:'OTP_ERROR_SUCCESS',
                errorOtp:error.data.message
            })
        }
    }
   

   
}


export function* userRegistration({
    params
}:Object):Generator<*,*,*>{
    try {
        const data = {
            email:params.email,
            name:params.email.substring(0, params.email.lastIndexOf("@")),
            password:params.pass,
            password_confirmation:params.pass_confirm,
        }
        let response: Object = {};
        response = yield call(usersRegisterApi,data)
        if(response.status >= 200 && response.status < 300 && response.data.status != "error"){
            yield put({
                type:'REGISTER_MODAL_SUCCESS',
                register_modal:false
            })
            // yield put({
            //     type:'LOGIN_MODAL_SUCCESS',
            //     login_modal:true
            // })

            yield put(
                Notifications.success({
					title:'Success',
					message:'Registration success',
                    position:'bl'
				})
            )


        }else{
            throw response
        }
    } catch (error) {
       if (typeof error.data == 'undefined') {
            yield put(
                Notifications.error({
                title: 'Error',
                message: 'Check your connection and try again . . .',
                position: 'bl'
                })
            )
       }else{
            yield put({
                type:'REGISTER_ERROR_CHECK_SUCCESS',
                error:error.data.messages
            })
            yield put(
                Notifications.error({
                title: 'Error',
                message: 'Please check your form . . .',
                position: 'bl'
                })
            )
       }
           
    }
}