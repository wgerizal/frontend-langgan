
const initializers = {
    version: "2.0.0",
    api_host: process.env.REACT_APP_API_HOST || "https://stage-api.langgan.id"
  };
  
  export default initializers;
  