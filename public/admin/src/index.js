import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux'
import { Switch,Router } from 'react-router'
import { Route } from 'react-router-dom'
import history from './dependencies/history'
import configureStore from './store/configureStore'
import App from './App';
import PublicIndex from './components/root/PublicIndex'
import RequireAuth from './pages/RequireAuth'
import ModalLogin from './pages/partials/ModalLogin'
import ModalRegister from './pages/partials/ModalRegister'
import LoginPage from './pages/LoginPage';
import AdminHome from './pages/AdminHome';
import Dashboard from './pages/Dashboard';
import MemberArea from './pages/MemberArea';
import MemberPaket from './pages/MemberPaket';
import TemaPage from './pages/TemaPage';

import * as serviceWorker from './serviceWorker';
// import bootstrap from 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import './styles/scss/style.scss';

const store = configureStore()

const publicIndexTarget = document.getElementById("root")

ReactDOM.render(
     <Provider store={store}>
            <Router history={history}>
            <PublicIndex>
                <ModalLogin/>
                <ModalRegister/>
                 <Switch>
                    <Route path='/admin' component={RequireAuth(AdminHome)}/>
                    <Route exact path='/auth/login' component={LoginPage}/>
                    <Route exact path='/admin/dashboard' component={Dashboard}/>
                </Switch>
            </PublicIndex>
            </Router>
    </Provider>
            
           ,
 publicIndexTarget);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
